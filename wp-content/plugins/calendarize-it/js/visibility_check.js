/* when calendar is rendered on a container that is not visible, it doesnt gets the correct height calculated.*/
function _rhc_check_visibility(){
	jQuery(document).ready(function($){
		var recheck = false;
		$('.fullCalendar').each(function(i,c){
			if( $(c).is(':visible') && $(c).find('.fc-content').height()<10 ){
				$(c).fullCalendar('render');
			}else if( $(c).find('.fc-content').height()<10 ){
				recheck = true;
			}		
		});
		if( recheck ){
			setTimeout('_rhc_check_visibility()',300);
		}
	});
}
jQuery(document).ready(function($){
	if( jQuery('.fullCalendar').length>0 )setTimeout('_rhc_check_visibility()',200);
});