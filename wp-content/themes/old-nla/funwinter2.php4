<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>


              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Functionals</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <td>
		      </td>
                    </TR>

              	    <TR><TD height=20></TD></TR>
                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          <TR>
                            <td align=center><font style="color: #000000;"><b>Dates for Winter II 2015 </b></font></td>
                          </tr>
<!--
                          <TR>
                            <td align=center><strong>All sessions are at <i>Netsports</i>.</strong></td>
                          </tr>
-->
                        </tbody></table>
                      </td>
                    </tr>

                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          
                          <TR>
                            <TD align=center><strong>Fridays</strong><br>6:00 pm - 7:15 pm</TD>
                          </TR>
                          <TR>
                            <TD align=center>January 30th</TD>
                          </TR>                          
                          <TR>
                            <TD align=center>February 6th</TD>
                          </TR>
                          <TR>
                            <TD align=center>February 13th</TD>
                          </TR>
                          <TR>
                            <TD align=center>February 20th</TD>
                          </TR>
                          <TR>
                            <TD align=center>February 27th</TD>
                          </TR>
                          <TR>
                            <TD align=center>March 6th</TD>
                          </TR>
                          <TR>
                            <TD align=center>March  13th</TD>
                          </TR>
                          <TR>
                            <TD align=center>March  20th</TD>
                          </TR>

                          <TR>
                            <TD>Please note: if makeup days are needed they will be announced when the closing occurs and will probably 
					occur in April or May. However we reserve the right to hold makeup days when best fit the programs schedule.
                            </TD>
                          </TR>

                        </TBODY></TABLE>
                      </TD>
                    </TR>
                    <tr><td height=5 align=center><img src=images/line.jpg width=400></td></tr>

                    <tr>
		      <td colspan = 5 align=center>Other session dates:</TD>
                    </TR>
		    <tr>
		      <td colspan = 5 align=center> 
				&nbsp; &nbsp; &nbsp; <a href=funsummer.php4>Summer 2014</a> 
                        &nbsp; &nbsp; &nbsp; <a href=funwinter1.php4>Winter I 2014</a>
                        &nbsp; &nbsp; &nbsp; <a href=funwinter2.php4>Winter II 2015</a>
                      </TD>
                    </TR>

                    <tr><td height=5 align=center><img src=images/line.jpg width=400></td></tr>
                    <TR>
                      <TD align= center><strong>On-Line Registration</strong> </td>
		    </tr>
		<tr><td>
<?
  $regopen [] = array();

  $prgFile = "data/Functionals/Regactive.txt";
  if ( file_exists($prgFile) )
  {
    $fh = fopen($prgFile, "r");
    for ( $i=0;  $i < 20; $i++ ) {
      $active = fgets($fh,25);
      $regopen[$i] = chop($active);
    }
    fclose($fh);
    
    $regstart = true;
    for ( $i=0; $i<20; $i++ ) {
      if ( $regopen[$i] == "open" ) {
        $i = 50;
        print("<p>Functionals space is limited so please register online to reserve your place in the ");
        print("Next Level Functionals.  The following table shows the age groups and playing level of ");
        print("the Functionals that still have open slots.  Click on the appropriate box if it says ");
        print("OPEN to register on-line.  If the box states CLOSED, unfortunately that age group and ");
        print("playing level are full.</p></td></tr>"); 
        print("<tr><td><TABLE cellSpacing=5 cellPadding=5 width=\"100%\" border=1><TBODY>"); 
        print("<tr><td width=25 rowspan=2><td colspan =2 align=center><strong>Boys</strong></td>");
        print("<td colspan =2 align=center><strong>Girls</strong></td></tr>");
        print("<tr><td align=center><strong>Rec/Challenge</strong></td><td align=center><strong>Classic</strong></td>");
        print("<td align=center><strong>Rec/Challenge</strong></td><td align=center><strong>Classic</strong></td></tr>");
        $newrow = true;
        $rowindex=0;
        for ( $j=0; $j < 20; $j++ ) {
          print("<tr><td>U".($rowindex + 11)."</td>");
          for ( $k=0; $k<4; $k++ )
          {
            if ( $regopen[($j + $k)] == "open" )
              print("<td align=center><a href=functionalsreg.php4?&prgtype=Functionals>open</a></td>");
            else 
              print("<td align=center>closed</td>");
          }
          $j = $j + 3;
          $rowindex++;
          print("</tr>");
        }
        print("</tbody></table>"); 
      }
    }
    if ( $i == 20 ) 
    {
        print("<p align=center>At this time registration for NLA Functionals is not open.</p></td></tr><tr><td height=10>&nbsp;</td></tr>");
    }
 
  }                
?>
		</td</tr>

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>

