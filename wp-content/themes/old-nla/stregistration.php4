<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                <TD align=middle background=images/darkgraybg.gif>
                  <strong><FONT color=#ffffff>Next Level Academy Specialty Training 
                  </FONT></strong>
                </TD>
                <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
              </TR>
            </tbody></table>
          </td>
        </TR>
        <tr bgcolor=#f0f0f0><td height=10>&nbsp;</td></tr>
        <tr bgcolor=#f0f0f0>
          <td><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 bgcolor=#f0f0f0><TBODY>
             <tr>
               <td width=10 rowspan=13>&nbsp; </td>
               <td colspan=2 align=center><b>Specialty Training is a MEMBERS ONLY PROGRAM!</b>
               <td width=10 rowspan=13>&nbsp; </td>
               </td>
             </tr>
             <tr>
               <td colspan=2>Any student <b>currently</b> enrolled in our NLA Graduate Program, 
                 Academy Teams (Morrisville or Raleigh), Centers, or Functionals program constitutes a member.<br>
               </td>
             </tr>
             <tr><td height=20>&nbsp; </td></tr>
             <tr>
               <td colspan=2> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>Registration</b></td>
             </tr>
             <tr>
               <td colspan=2><ul>
                 <li> All registration will be completed on-line and 24 hours in advance</li>
                 <li> Registration is filled on a first come first serve basis</li>
<!--
                 <li> Registration for all Specialty Training sessions will be grouped by skill level and/or focus area</li>
-->
                 <li> Registration for Finishing/Ball Striking will be grouped by skill level and age</li>
                 <li> Registration for Finishing/Ball Striking is separate</li>
                 <li> You must be registered for a Specialty Training session in order to be eligible to participate in the Finishing/Ball Striking session </li>
                 <li> All Cancellations must be confirmed 24 hours in advance</li>
                 </ul>
                 <p> To register, select a day you would like to attend from the calendar below and
                 click on the age group of your player.</p>
               </td>
             </tr>
             <tr><td height=10  width = 20%>&nbsp; </td></tr>
             <tr>
               <td colspan=2><TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 bgcolor=#f0f0f0><TBODY>
                 <tr><td height=10>&nbsp;</td></tr>

                 <tr><td align=center><TABLE cellSpacing=0 cellPadding=0 width="75%" border=0 bgcolor=#ffff99><TBODY>
                   <tr><td height=10>&nbsp;</td></tr>
                   <tr>
                     <td width=10 rowspan=8>&nbsp;
                     <td colspan=2 align=right> <b>Schedule</b></td>
                   </tr>
                   <tr><td height=10>&nbsp;</td></tr>
                   <tr>
                     <td width = 150><font color=#aa4444>Monday - Thursday </font></td>
                     <td width = 100><font color=#aa4444>3:50 - 4:50 </font></td>
                     <td><font color=#aa4444>Specialty Training Session</font></td>
                   </td>
                   <tr><td height=5>&nbsp;</td></tr>
                   <tr>
                     <td><font color=#aa4444>Friday</font> </td>
                     <td><font color=#aa4444>3:50 - 4:50</font></td>
                     <td><font color=#aa4444>Finishing/Ball Striking</font></td>
                   </tr>
                   <tr><td colspan=3><i>* schedule subject to change throughout the year
                   <tr>
                     <td colspan = 3><font color=#aa4444>&nbsp;<br>The finishing/ball striking
                     sessions will be divided by age group:<br>
                     &nbsp; &nbsp; &nbsp; Ages 7 thru 10 - 1st and 3rd Friday<br>
                     &nbsp; &nbsp; &nbsp; Ages 11 thru 14 - 2nd and 4th Friday</font>
                     </td>
                   </tr>
                   <tr><td height=10>&nbsp;</td></tr>
                   </tbody></table>
                   </td>
                 </tr>

                 <tr><td height=10>&nbsp;</td></tr>

               </tbody></table></td>
             </tr>
             <tr><td height=10  width = 20%>&nbsp; </td></tr>
             <tr>
               <td colspan=2> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>Pricing</b>
               </td>
             </tr>
             <tr>
               <td width = 20%>Specialty Training</td>
               <td>$35/player per session</td>
             </tr>
             <tr>
               <td >Finishing/Ball Striking</td>
               <td>$20/player per session</td>
             </tr>
             <tr><td height=20>&nbsp; </td></tr>
          </tbody></table></td>
        </tr>
        

<?
 $prgtype= $_GET['prgtype'];
 $monthCount = 1;


  // Get current Month in text format for display.  Get current month in numerical format so that actual calendar
  // can be dynamically built.  Current year for display.

  $currMonth = date("F");
  $monthNumber = date("n");
  $currYear = date("Y");
  $currDay = date("j");

  $totalDisplayDays = 0;    // will display 28 days (rounded up to comeplete the week

  // read in the default session information, will be indexed
  // by day (Monday, Tuesday, etc ...).  Data is in the form
  // day^type - e.g  Monday^Specialty    - Monday being first entry.

  $sindex = 0;
  $fh = fopen("data/stdata/days.txt", "r");
  while ( $dayInfo= fgets($fh, 512) ) {
    $parms = explode ("^", $dayInfo, 2);
    $sday[$sindex] = $parms[0];
    $stype[$sday[$sindex]] = chop($parms[1]);
    if ( $stype[$sday[$sindex]] == "Finishing" )
      $finDay = $sday[$sindex];
    $sindex++;
  }
  fclose($fh);


  $monthIndex = 0;
  while ( $monthIndex < $monthCount )  {
              
    switch ( $currMonth ) {
      case 'January': 
                 $nextMonth = "February";
                 break;
     case 'February': 
                 $nextMonth = "March";
                 break;
     case 'March': 
                 $nextMonth = "April";
                 break;
     case 'April': 
                 $nextMonth = "May";
                 break;
     case 'May': 
                 $nextMonth = "June";
                 break;
     case 'June': 
                 $nextMonth = "July";
                 break;
     case 'July': 
                 $nextMonth = "August";
                 break;
     case 'August': 
                 $nextMonth = "September";
                 break;
     case 'September': 
                 $nextMonth = "October";
                 break;
     case 'October': 
                 $nextMonth = "November";
                 break;
     case 'November': 
                 $nextMonth = "December";
                 break;
     case 'December': 
                 $nextMonth = "January";
                 break;
    }

    $index = 1;
    $dayIndex = 0;

    print("<tr>");
    print("<td align=\"center\">");
    print("<table border=\"1\" cellpading=\"1\" cellspacing=\"1\" width=\"790\" bgcolor=\"#f0f0f0\"><tbody>");
    print("<tr><td height=\"30\" colspan=\"7\" align=\"center\">");
    print("<font style=\"font-size: 20px; color: #000000;\">");
 
    print("&nbsp ".$currMonth."  ".$currYear."</font>");

    print("</td></tr>");
    print("<tr>");
    print("<td width=\"110\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Sunday</td>");
    print("<td width=\"110\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Monday</td>");
    print("<td width=\"110\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Tuesday</td>");
    print("<td width=\"110\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Wednesday</td>");
    print("<td width=\"110\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Thursday</td>");
    print("<td width=\"110\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Friday</td>");
    print("<td width=\"110\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Saturday</td>");
    print("</tr>");

   // Find out how many days in the current month.

    $index = 29;
    while ( $index < 33 ) {
      if ( !checkdate($monthNumber, $index, $currYear) ) {
        $loopMax = $index;
        $index = 32;
      }
      $index++;
    }

 // if monthcount = 1, it is the first time thru (and may be only time,
 // depending on current date.

    if ( $monthCount == 1 ) {
      $currjddate = gregoriantojd($monthNumber, $currDay, $currYear);
      $dayofWeek = jddayofweek($currjddate , 0); 
      $dayIndex = $currDay;

      $startDay = $dayofWeek % 7;
     // $startDay = bcmod($dayofWeek, 7);

      if ( ($currDay + 28) > $loopMax ) 
        $monthCount = 2;
    }
    else {
      $currjddate = gregoriantojd($monthNumber, 1, $currYear);
      $dayofWeek = jddayofweek($currjddate , 0); 
      $startDay = $dayofWeek;
      $dayIndex = 1;
    }

// Determine when 1st and 3rd finishing days are
// i.e. 1st and 3rd Friday for younger, 2 and 4th for older ...
    $finindex = 1;
    while ( $finindex <= 7 ) {
      $findate = gregoriantojd($monthNumber, $finindex, $currYear);
      $finday = jddayofweek($findate , 1); 
      if ( $finday == $finDay ) {
        $finishDays[0] = $findate;
        $finindex = $finindex + 7;
      }
      $finindex++;
    }
    $finishDays[1] = $finishDays[0] + 7;
    $finishDays[2] = $finishDays[0] + 14;
    $finishDays[3] = $finishDays[0] + 21;

    $colIndex = 0;
 $skipdate = "true";
    print("<tr height=\"100\">");
    while ( $colIndex < 7 ) {        // 7 days in a week, displaying one week per row on the calendar

      if ( $colIndex >= $startDay ) {   // we have gotten to the current date, satrting printing session
        $totalDisplayDays++;            // information as well as date in lower right corner
        print("<td  width=\"100\" align=\"right\" valign=\"bottom\">");
        print("<font style=\"font-size: 12px; color: #000000;\">"); 
        $jdDate = gregoriantojd($monthNumber, $dayIndex, $currYear);
        $sdayofweek = jddayofweek($jdDate, 1);
  
 if ( $skipdate == "false" ) {
        switch ( $stype[$sdayofweek] ) {
          case 'Specialty':
            $stfile = "data/stdata/rsp".$jdDate.".txt";
            if ( !file_exists($stfile) ) {
              print("<a href=strgform.php4?&prgtype=U7&prgday=".$jdDate."><font color=#000000>U7-U10</font></a> &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; <br>");
              print("<a href=strgform.php4?&prgtype=U11&prgday=".$jdDate."><font color=#000000><br>U11-U14</font></a> &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  <br>");
/*
              if ( $dayIndex > 9 )
                print("<a href=strgform.php4?&prgtype=Gk&prgday=".$jdDate."><font color=#000000><br>Goalkeeper</font></a> &nbsp;&nbsp;");
              else
                print("<a href=strgform.php4?&prgtype=Gk&prgday=".$jdDate."><font color=#000000><br>Goalkeeper</font></a> &nbsp; &nbsp; ");
*/
            }

            print("</font><font style=\"font-size: 10px; color: #ff0000;\"><strong>".$dayIndex++."</strong></td>");
          break;

          case 'Finishing':
            $stfile = "data/stdata/rfin".$jdDate.".txt";
            if ( !file_exists($stfile) ) {
              if ( ($jdDate == $finishDays[0]) || ($jdDate == $finishDays[2]) )  { 
                print("<a href=strgform.php4?&prgtype=FB&prgday=".$jdDate."&age=younger><font color=#000000>Finish 7-10</font></a>&nbsp;&nbsp;&nbsp;");
              }
              else {
                if ( ($jdDate == $finishDays[1]) || ($jdDate == $finishDays[3]) )  { 
                  print("<a href=strgform.php4?&prgtype=FB&prgday=".$jdDate."&age=older><font color=#000000>Finish 11-14</font></a>&nbsp;&nbsp;");
                }
              }
            }

            print("</font><font style=\"font-size: 10px; color: #ff0000;\"><strong>".$dayIndex++."</strong></td>");
          break;

          default:
            $stfile = "data/stdata/sp".$jdDate.".txt";
            if ( file_exists($stfile) ) {
              print("<a href=strgform.php4?&prgtype=U7&prgday=".$jdDate."><font color=#000000>U7-U10</font></a>&nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; <br>");
              print("<a href=strgform.php4?&prgtype=U11&prgday=".$jdDate."><font color=#000000><br>U11-U14</font></a> &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  <br>");
/*
              if ( $dayIndex > 9 )
                print("<a href=strgform.php4?&prgtype=Gk&prgday=".$jdDate."><font color=#000000><br>Goalkeeper</font></a> &nbsp;&nbsp;");
              else
                print("<a href=strgform.php4?&prgtype=Gk&prgday=".$jdDate."><font color=#000000><br>Goalkeeper</font></a> &nbsp; &nbsp; ");
*/

            }
            else {
              $stfile = "data/stdata/fin".$jdDate.".txt";
              if ( file_exists($stfile) ) 
                print("<a href=strgform.php4?&prgtype=FB&prgday=".$jdDate."&age=All><font color=#000000>Finish All</font></a>&nbsp&nbsp;");
            }

            print("</font><font style=\"font-size: 10px; color: #ff0000;\"><strong>".$dayIndex++."</strong></td>");
          break;

        }
 }
 else {
   $skipdate = "false";
   print("</font><font style=\"font-size: 10px; color: #ff0000;\"><strong>".$dayIndex++."</strong></td>");
 }

      }
      else   //haven't gotten to current date yet, print spaces until then
        print("<td align=\"right\" valign=\"bottom\"><font style=\"font-size: 12px; color: #000000;\">&nbsp;</td>");

      $colIndex++;

      if ( ($dayIndex == $loopMax) && ($colIndex < 7) ) {
        while ( $colIndex++ < 7 )
          print("<td align=\"right\" valign=\"bottom\"><font style=\"font-size: 12px;\">&nbsp;</td>");
      }

    }
    print("</tr>");


  // now loop through and finish the rest of the rows of the calendar.  May need to pad end of calendar
  // to keep a nice square look.   

    while ( ($dayIndex < $loopMax) && ($totalDisplayDays < 27) ) {  
      $colIndex = 0; 
      print("<tr height=\"100\">");
      while ( ($colIndex < 7 ) && ($dayIndex < $loopMax) )  {
        $totalDisplayDays++;
        print("<td  width=\"100\" align=\"right\" valign=\"bottom\">");
        print("<font style=\"font-size: 12px; color: #000000;\">"); 
        $jdDate = gregoriantojd($monthNumber, $dayIndex, $currYear);
        $sdayofweek = jddayofweek($jdDate, 1);
        switch ( $stype[$sdayofweek] ) {
          case 'Specialty':
            $stfile = "data/stdata/rsp".$jdDate.".txt";
            if ( !file_exists($stfile) ) {
              print("<a href=strgform.php4?&prgtype=U7&prgday=".$jdDate."><font color=#000000>U7-U10</font></a> &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;<br>");
              print("<a href=strgform.php4?&prgtype=U11&prgday=".$jdDate."><font color=#000000><br>U11-U14</font></a> &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;<br>");
/*
              if ( $dayIndex > 9 )
                print("<a href=strgform.php4?&prgtype=Gk&prgday=".$jdDate."><font color=#000000><br>Goalkeeper</font></a> &nbsp;&nbsp;");
              else
                print("<a href=strgform.php4?&prgtype=Gk&prgday=".$jdDate."><font color=#000000><br>Goalkeeper</font></a> &nbsp; &nbsp; ");
*/
            }

            print("</font><font style=\"font-size: 10px; color: #ff0000;\"><strong>".$dayIndex++."</strong></td>");
          break;

          case 'Finishing':
            $stfile = "data/stdata/rfin".$jdDate.".txt";
            if ( !file_exists($stfile) ) 
              if ( ($jdDate == $finishDays[0]) || ($jdDate == $finishDays[2]) )  { 
                print("<a href=strgform.php4?&prgtype=FB&prgday=".$jdDate."&age=younger><font color=#000000>Finish 7-10</font></a>&nbsp;&nbsp;&nbsp;");
              }
              else {
                if ( ($jdDate == $finishDays[1]) || ($jdDate == $finishDays[3]) )  { 
                  print("<a href=strgform.php4?&prgtype=FB&prgday=".$jdDate."&age=older><font color=#000000>Finish 11-14</font></a>&nbsp;&nbsp;");
                }
              }

            print("</font><font style=\"font-size: 10px; color: #ff0000;\"><strong>".$dayIndex++."</strong></td>");
          break;

          default:
            $stfile = "data/stdata/sp".$jdDate.".txt";
            if ( file_exists($stfile) ) {
              print("<a href=strgform.php4?&prgtype=U7&prgday=".$jdDate."><font color=#000000>U7-U10</font></a> &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; <br>");
              print("<a href=strgform.php4?&prgtype=U11&prgday=".$jdDate."><font color=#000000><br>U11-U14</font></a> &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;   <br>");
/*
              if ( $dayIndex > 9 )
                print("<a href=strgform.php4?&prgtype=Gk&prgday=".$jdDate."><font color=#000000><br>Goalkeeper</font></a> &nbsp;&nbsp;");
              else
                print("<a href=strgform.php4?&prgtype=Gk&prgday=".$jdDate."><font color=#000000><br>Goalkeeper</font></a> &nbsp; &nbsp; ");
*/
            }
            else {
              $stfile = "data/stdata/fin".$jdDate.".txt";
              if ( file_exists($stfile) ) 
                print("<a href=strgform.php4?&prgtype=FB&prgday=".$jdDate."&age=all><font color=#000000>Finish All</font></a>&nbsp;&nbsp;");
            }

            print("</font><font style=\"font-size: 10px; color: #ff0000;\"><strong>".$dayIndex++."</strong></td>");

          break;

        }
        $colIndex++;
      }


      if ( ($dayIndex == $loopMax) && ($colIndex < 7) ) {
        while ( $colIndex++ < 7 )
          print("<td align=\"right\" valign=\"bottom\"><font style=\"font-size: 12px;\">&nbsp;</td>");
      }
    }
    print("</tr></tbody></table></td></tr>");
    print("<tr><td height=\"25\"><font style=\"font-size: 12px;\">&nbsp;</td></tr>");
      
    if ( $monthNumber == 12 ) {
      $monthNumber = 1;
      $currYear++;
    }
    else
      $monthNumber++;

    $currMonth = $nextMonth;
    $monthIndex++;
  }

  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>
