<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>

              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Raleigh/Wake Forest Academy Tryout Information</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top" colspan=2>
                        <p>All <strong>Raleigh/Wake Forest Academy</strong> tryouts are held at the The Factory in Wake Forest. 
			The times for the tryouts by age group and gender are listed below. Please have 
			your player at The Factory at least 30 minutes before tryouts to have sufficient 
			time to check in, pin your player's number on the front of their shirt and get 
			them ready to go onto the field including having their water bottle and soccer ball
			with their name on both.</p>
<p><strong>The site you select to tryout at is the 
site you are selecting to participate at (i.e. if you tryout out at Raleigh, then your 
player will be evaluated for the program at Raleigh).</strong></p>

                        <p align=center> <b>Parents will NOT be permitted on the field or in the player benches.</b></p>
                        <p>There is a $20 tryout fee for players who register by the deadline 
                         and a $30 tryout fee for walk-ups. <b>NLA only accepts cash or check as forms of payment for tryouts.</b></p>

                      </TD>
                    </TR>
                    <tr><td height=2 colspan=2></td></tr>
                    <tr>
                      <td width=350>Rising U7 
<?
  $mytime = getdate(date('U'));
  $myyear = $mytime['year'];
  $agegroups = array (7, 8, 9, 10, 11 );
  $myyear = $myyear + 1;
  print("(players born 8-1-".($myyear - $agegroups[0] - 1)." thru 7-31-". ($myyear - $agegroups[0]));
?>
                      </td>
                      <td width=230 rowspan=3 valign=middle>
                        For a detailed tryout schedule and on-line registration for the U7, U8, and U9, age
                        groups please <a href="ralyoungtryoutinfo.php4"><font style="color:#0000ff;">Click here</font></a>.
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Rising U8 
<?
  print("(players born 8-1-".($myyear - $agegroups[1] - 1)." thru 7-31-". ($myyear - $agegroups[1]));
?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        Rising U9
<?
  print("(players born 8-1-".($myyear - $agegroups[2] - 1)." thru 7-31-". ($myyear - $agegroups[2]));
?>
                      </td>
                    </tr>
                    <tr bgcolor=#ffffd0><td height=2 colspan=2></td></tr>


                    <tr>
                      <td width=350>Rising U10 
<?
  print("(players born 8-1-".($myyear - $agegroups[3] - 1)." thru 7-31-". ($myyear - $agegroups[3]));
?>
                      </td>
                      <td width=230 rowspan=2 valign=middle>
                        For a detailed tryout schedule and on-line registration for the U10 and U11 age
                        groups please <a href="ralyoungtryoutinfo.php4"><font style="color:#0000ff;">Click here</font></a>.
                      </td>
                    </tr>

                    <tr>
                      <td>Rising U11
<?
  print("(players born 8-1-".($myyear - $agegroups[4] - 1)." thru 7-31-". ($myyear - $agegroups[4]));
?>
                      </td>
                    </tr>

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>