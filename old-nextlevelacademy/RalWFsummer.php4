<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>


              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Raleigh/WF Development Centers</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>Younger Development Centers - U7, U8, and U9 Boys and Girls</FONT></strong></P>
                      </TD>
                    </TR>
                    <TR>
                      <TD valign="top">
                        <P>The younger Development Centers are for players in the following age groups: <b>U7, U8,</b> and <b>U9.</b></P>
                      </TD>
                    </TR>
		    <tr><td>   
<?
    $prgFile = "data/Centers/RalWFRegactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<p><strong>Registration for the younger Development Centers is available</strong>. &nbsp; &nbsp;");
        print("Space is limited so please <a href=regshtml.php4?&prgtype=RalWFCenters><font style=\"color: #0000ff;\">register online</font></a> to reserve your place in the younger Development Centers. ");
      }
      else 
        print("<p align=center>Online registration for the younger Development Centers is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the younger Development Centers is not open.</p>");
                       
?>
		</td></tr>
              	    <TR><TD height=20></TD></TR>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>Older Development Centers  - U10, U11, and U12 Boys and Girls</FONT></strong></P>
                      </TD>
                    </TR>
                    <TR>
                      <TD valign="top">
                        <P>The older Development Centers  are for players in the following age groups: <b>U10, U11,</b> and <b>U12.</b></P>
                      </TD>
                    </TR>

		    <tr>
		      <td>
<?
    $prgFile = "data/Academy/RalWFRegactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<p><strong>Registration for the older Development Centers  is available</strong>. &nbsp; &nbsp;");
        print("Space is limited so please <a href=regshtml.php4?&prgtype=RalWFAcademy><font style=\"color: #0000ff;\">register online</font></a> to reserve your place in the older Development Centers .</p> ");

      }
      else 
        print("<p align=center>Online registration for the older Development Centers  is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the older Development Centers  is not open.</p>");
                       
?>
		  </td>
		</tr>
              	    <TR><TD height=20></TD></TR>
                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          <TR>
                            <td align=center><font style="color: #000000;"><b>Dates for Summer 2014</b></font></td>
                          </tr>
                          <TR>
                            <td align=center>Tuesday is the only day of the week the session is offered.</td>
                          </tr>
                        </tbody></table>
                      </td>
                    </tr>

                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          
                          <TR>
                            <td width="20%" rowspan=8>&nbsp; </td>
                            <td width="20%" rowspan=8 valign=top>Tuesdays</td>
                            <TD >July 8th 7:00 - 8:00 pm </TD>
                          </TR>
                          <TR>
                            <TD >July 15th 7:00 - 8:00 pm</TD>
                          </TR>
                          <TR>
                            <TD >July 22nd 7:00 - 8:00 pm</TD>
                          </TR>
                          <TR>
                            <TD >July 29th 7:00 - 8:00 pm</TD>
                          </TR>
                          <TR>
                            <TD >Aug. 5th 7:00 - 8:00 pm</TD>
                          </TR>
                          <TR>
                            <TD >Aug. 12th 7:00 - 8:00 pm</TD>
                          </TR>
                          <TR>
                            <TD >Aug. 19th 7:00 - 8:00 pm</TD>
                          </TR>
                          <TR>
                            <TD >Aug. 26th 7:00 - 8:00 pm</TD>
                          </TR>
                          <tr><td height=4>&nbsp; </td></tr>
                        </TBODY></TABLE>
                      </TD>
                    </TR>
                    <tr><td height=5 align=center><img src=images/line.jpg width=400></td></tr>

                    <tr>
		      <td colspan = 5 align=center>Other session dates:</TD>
                    </TR>
		    <tr>
		      <td colspan = 5 align=center>
				   <p align=center>
                        &nbsp; &nbsp;<a href=RalWFSpring2014.php4>Spring 2014</a>
                        &nbsp; &nbsp;<a href=RalWFfall.php4>Fall 2014</a>
                        &nbsp; &nbsp;<a href=RalWFwinter1.php4>Winter I 2014</a>
                        &nbsp; &nbsp;<a href=RalWFwinter2.php4>Winter II 2015</a> 
                        &nbsp; &nbsp;<a href=RalWFSpring2015.php4>Spring 2015</a> 
				   </p>

                      </TD>
                    </TR>

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>

