<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>
              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>On-Line Registration Results</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
<?


  $age =chop( $_POST["age"]) ; 
  $age = substr($age,0,3);
  $gender =chop( $_POST["gender"]) ;  
  $firstname =chop( $_POST["firstname"]) ;
  $lastname =chop( $_POST["lastname"]) ; 
  $parentname =chop( $_POST["parentname"]) ; 
  $street =chop( $_POST["street"]);
  $city =chop( $_POST["city"]);
  $homephone =chop( $_POST["homephone"]);
  $workphone =chop( $_POST["workphone"]);
  $email  =chop( $_POST["email"]);
  $month =chop( $_POST["month"]);
  $day =chop( $_POST["day"]);
  $year =chop( $_POST["year"]);

  $firstname = str_replace("\\", "", $firstname);
  $lastname = str_replace("\\", "", $lastname);
  $parentname = str_replace("\\", "", $parentname);

  $foot =chop( $_POST["foot"]);

  $error = "<font color=#ff0000>Error message: </font><br>";
 
  $signupFile = "data/Tryouts/";

  if ( $age ==  "Age" ) 
    $error = $error."Age group not entered. <br>";

  if ( $gender == "Select One") 
    $error = $error."Gender not entered. <br>";

  if ( ($firstname == "") || ($lastname == "" ) ) 
    $error = $error."Player first and last name must be entered. <br>";

  if ( $parentname ==  "") 
    $error = $error."Parents name must be entered. <br>";

  if ( $email == "")
    $error = $error."An e-mail address must be entered. <br>";

  if ( $street == "")
    $error = $error."A street address must be entered. <br>";

  if ( $city == "")
    $error = $error."A city must be entered. <br>";

  if ( $homephone == "")
    $error = $error."A home phone number must be entered. <br>";

  if ( $month == "0") 
    $error = $error."Players birth month must be entered. <br>";

  if ( $day == "0") 
    $error = $error."Players birth day must be entered. <br>";

  if ( $year == "0") 
    $error = $error."Players birth year must be entered. <br>";

  $mytime = getdate(date('U'));
  $myyear = $mytime["year"];
  $ageInput = array ('U7 ', 'U8 ', 'U9 ', 'U10', "U11" );
  $agegroups = array (7, 8, 9, 10, 11 );

  for ($i=0; $i < count($agegroups); $i++ )
  {
    if ( $age == $ageInput[$i] )
    {
      $myyear = $myyear - $agegroups[$i];  
      $i =  count($agegroups);  
    }
  }

  if ( $month < '8' ) 
  {
    if ( $year != ($myyear + 1) )
      $error = $error."Player birthday doesn't fall into age group range.<br>";
  }
  else {
    if ( $year != $myyear )
      $error = $error."Players birthday doesn't fall into age group range.<br>";
  }

  if ( $foot == "Select One") 
    $error = $error."Left or Right footed must be selected. <br>";

  if ( $error != "<font color=#ff0000>Error message: </font><br>") { 
    Print("<p>Required data was not entered.  Please review the following error");
    Print(" message and enter the required data.");
    Print(" <p>");
    Print ($error);
  }
  else {
    $subject = "Tryout Registration - Raleigh";
    $emailstring = "                       Next Level Academy Registration - Raleigh \n\n";
    $emailstring = $emailstring."Check in at the registation table prior to the tryouts.\n";
    $emailstring = $emailstring."The cost for tryouts is $20.00 due at check-in.\n\n";
    $emailstring = $emailstring."Your player should wear a plain white shirt, black shorts, and black socks";
    $emailstring = $emailstring."Your player should have a water bottle with their name on it and a size 4 ball with their name on it.\n\n";
    $emailstring = $emailstring."Parents will not be allowed to sit on the team benches, this is an opportunity ";
    $emailstring = $emailstring."for your player to concentrate on the Next Level Coachs instructions.\n\n";
    $emailstring = $emailstring."______________________________________________________________________________________ \n";
    $emailstring = $emailstring."Player Name: ". $firstname." ".$lastname."   Gender: ".$gender."  Age Group: ".$age."\n\n";            
    $emailstring = $emailstring."Birthday: ".$month." ".$day." ".$year."\n\n";
    $emailstring = $emailstring.$firstname." is ".$foot." footed.\n\n";
    $emailstring = $emailstring."Parents Name: ".$parentname."\n\n";
    $emailstring = $emailstring."Address: ". $street."  ".$city."\n\n";
    $emailstring = $emailstring."Home Phone: ".$homephone."  Work Phone: ". $workphone."\n\n"; 
    $emailstring = $emailstring."Email Address: ".$email."\n\n";
    $emailstring = $emailstring."I, as parent/guardian, hereby give permission for ".$firstname." ".$lastname." to participate in the ";
    $emailstring = $emailstring."Next Level tryouts.  I hereby release and forever discharge Next Level, its coaches, ";
    $emailstring = $emailstring."agents and the owners of fields used from liability for any personal injury or illness, ";
    $emailstring = $emailstring."damage, or loss incurred while participating in this tryout. In the event I cannot be reached ";  
    $emailstring = $emailstring."in an emergency, I hereby grant permission to the Next Level Staff to secure treatment for the ";
    $emailstring = $emailstring."above named person.\n\nParent/Guardian Signature:__________________________________  Date:_________________";
    $mailsuccess = mail("staff@nextlevelacademy.com", $subject, $emailstring, "From: staff@nextlevelacademy.com");
 //print($emailstring);

    $subject = "Next Level Tryout Registration Confirmation";  
    $emailstring = "This e-mail is to confirm the registration of ".$firstname." ".$lastname." for Team Next Level Tryouts.\n";
    $emailstring = $emailstring."You registered for the Raleigh/Wake Forest tryouts.\n\n";    
    $emailstring = $emailstring."Check in at the registation table prior to the tryouts.\n";
    $emailstring = $emailstring."The cost for tryouts is $20.00 due at check-in.\n\n";
    $emailstring = $emailstring."Your player should wear a plain white shirt, black shorts, and black socks. ";
    $emailstring = $emailstring."Your player should have a water bottle with their name on it and a size 4 ball with their name on it.\n\n";
    $emailstring = $emailstring."Parents will not be allowed to sit on the team benches, this is an opportunity ";
    $emailstring = $emailstring."for your player to concentrate on the Next Level Coachs instructions.";

   $mailsuccess = mail($email, $subject, $emailstring, "From: staff@nextlevelacademy.com");
//$mailsuccess = 1;

   if ( $mailsuccess ) {
      $signupFile = $signupFile.$gender."Ral.txt";
      $playerString = $firstname."^".$lastname."^".$age."^".$gender."^".$month." ".$day." ".$year."^".$parentname."^";
      $playerString = $playerString.$street." ".$city."^".$homephone."^".$workphone."^".$email."^".$foot."\n";
      $fh = fopen($signupFile, "a+");
      fwrite($fh, $playerString);
      fclose($fh);
      print("Your registration has been completed.  You should receive an e-mail confirming your registration.");
      print("<p> Thank you for using the on-line system to register for the Next Level Tryouts.");
   }
   else {
     print("There was an error processing your registration.  Please go back and re-attempt the on-line registration.");
     print(" If problems persist please send and e-mail to:  staff@nextlevelacademy.com and describe your problem. ");
     print("<p>We apologize for the inconvenience.");
   }
  }
?>
                 
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>