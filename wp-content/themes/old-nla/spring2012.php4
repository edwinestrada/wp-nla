<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>


              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Development Centers</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>Younger Development Centers - U7, U8, and U9 Boys and Girls</FONT></strong></P>
                      </TD>
                    </TR>
                    <TR>
                      <TD valign="top">
                        <P>The younger Development Centers are for players in the following age groups: <b>U7, U8,</b> and <b>U9.</b></P>
                      </TD>
                    </TR>


		    <tr><td>
<?
    $prgFile = "data/Centers/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<p><strong>Registration for the younger Development Centers is available</strong>. &nbsp; &nbsp;");
        print("Space is limited so please <a href=regshtml.php4?&prgtype=Centers><font style=\"color: #0000ff;\">register online</font></a> to reserve your place in the younger Development Centers. ");
      }
      else 
        print("<p align=center>Online registration for the younger Development Centers is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the younger Development Centers is not open.</p>");
                       
?>
		</td></tr>
              	    <TR><TD height=20></TD></TR>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>Older Development Centers - U10, U11, and U12 Boys and Girls</FONT></strong></P>
                      </TD>
                    </TR>
                    <TR>
                      <TD valign="top">
                        <P>The older Development Centers are for players in the following age groups: <b>U10, U11,</b> and <b>U12.</b></P>
                      </TD>
                    </TR>

		    <tr>
		      <td>
<?
    $prgFile = "data/Academy/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<p><strong>Registration for the older Development Centers is available</strong>. &nbsp; &nbsp;");
        print("Space is limited so please <a href=regshtml.php4?&prgtype=Academy><font style=\"color: #0000ff;\">register online</font></a> to reserve your place in the older Development Centers.</p> ");
      }
      else 
        print("<p align=center>Online registration for the older Development Centers is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the older Development Centers is not open.</p>");
                       
?>
		  </td>
		</tr>

              	    <TR><TD height=20></TD></TR>
                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          <TR>
                            <td align=center><font style="color: #000000;"><b>Dates for Spring 2012 </b></font></td>
                          </tr>
                          <TR>
                            <td>This session will be 
						6 weeks instead of 8.  Since this session is shorter the cost is $115.
                            </td>
                          </tr>
                        </tbody></table>
                      </td>
                    </tr>

                    <TR>
                      <TD>

<TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          
                          <TR>
			            <td width="20%" rowspan=8>&nbsp; </td>
                            <TD>Fridays</TD>
                            <TD>March 30th 5:00 - 6:00pm</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>April 6th &nbsp; &nbsp; No Training</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>April 13th 5:00 - 6:00pm</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>April 20th 5:00 - 6:00pm </TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>April 27th &nbsp; &nbsp; No Training</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>May 4th 5:00 - 6:00pm </TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>May 11th 5:00 - 6:00pm </TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>May 18th 5:00 - 6:00pm </TD>
                          </TR>
                          <tr><td height=7>&nbsp; </td></tr>

<!--
                           <TR>
                            <TD colspan = 5>Please note if we need to cancel a session, the makeup 
                            day will be May 18th. If this occurs, we'll notify participants via e-mail and we'll 
                            update the online schedule and calendar.</TD>
                          </TR>
-->

                        </TBODY></TABLE>
<!--

                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>

                          <TR>
                            <TD>Tuesdays</TD>
                            <TD>April 26th 5:00 - 6:00pm *</TD>
                            <TD width="40">&nbsp; </TD>
                            <TD>Fridays</TD>
                            <TD>April 29th 5:00 - 6:00pm *</TD>
                          </TR>

                          <TR>
                            <TD></TD>
                            <TD>May 3rd 5:00 - 6:00pm </TD>
                            <TD>&nbsp; </TD>
                            <TD></TD>
                            <TD>May 6th 5:00 - 6:00pm  </TD>
                          </TR>

                          <TR>
                            <TD></TD>
                            <TD>May 10th 5:00 - 6:00pm  </TD>
                            <TD>&nbsp; </TD>
                            <TD></TD>
                            <TD>May 13th 5:00 - 6:00pm </TD>
                          </TR>

                          <TR>
                            <TD></TD>
                            <TD>May 17th 5:00 - 6:00pm </TD>
                            <TD>&nbsp; </TD>
                            <TD></TD>
                            <TD>May 20th 5:00 - 6:00pm </TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>May 24th 5:00 - 6:00pm </TD>
                            <TD>&nbsp; </TD>
                            <TD></TD>
                            <TD>May 27th 5:00 - 6:00pm </TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>May 31st 5:00 - 6:00pm </TD>
                            <TD>&nbsp; </TD>
                            <TD></TD>
                            <TD>June 3rd 5:00 - 6:00pm </TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>June 7th 5:00 - 6:00pm </TD>
                            <TD>&nbsp; </TD>
                            <TD></TD>
                            <TD>June 10th 5:00 - 6:00pm </TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>June 14th 5:00 - 6:00pm</TD>
                            <TD>&nbsp; </TD>
                            <TD></TD>
                            <TD>June 17th 5:00 - 6:00pm </TD>
                          </TR>

                          <tr><td height=7>&nbsp; </td></tr>
                           <TR>
                            <TD colspan = 5 align=center>* <i>There will be a brief parent 
			meeting after the first training session with the NLA coaches</i></TD>
                          </TR>
                        </TBODY></TABLE>
-->
                      </TD>
                    </TR>
                    <tr><td height=5 align=center><img src=images/line.jpg width=400></td></tr>
                   <tr>
		      <td colspan = 5 align=center>Other session dates:</TD>
                    </TR>
		    <tr>
		      <td colspan = 5 align=center> 
                         <p align=center>
                        &nbsp; &nbsp;<a href=summer.php4>Summer 2012</a> 
                        &nbsp; &nbsp;<a href=fall.php4>Fall 2012</a> 
                        &nbsp; &nbsp;<a href=winter1.php4>Winter I 2012</a>
                        &nbsp; &nbsp;<a href=winter2.php4>Winter II 2013</a> 
                        &nbsp; &nbsp;<a href=spring2013.php4>Spring 2013</a>
                        </p>
                      </TD>
                    </TR>

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>


