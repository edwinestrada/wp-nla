<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>

              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Next Level Summer Camps 2010</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top" colspan=2>
                        Next Level Academy is excited to announce our camps for this summer! Join us 
                                                for our 6th year of fantastic, fun-filled, soccer training camps!
                        <p>This is a great opportunity for your player to meet and work with our coaches 
                                                while furthering their technical, playing, and overall soccer skills.</p>
                        <p>Our camps will be full day (9:00am - 4:00pm), full 
                                                 week (Monday - Friday) and will be held inside at our Morrisville location 
                                                 (Netsports). We will also offer early and late drop options if you need that.</p>
                                             <p>Below are details about the camp.
                     </TD>
                    </TR>

                    <tr><td height=5 align=center colspan=2><img src=images/line.jpg width=500></td></tr>
                    <tr><td height=5></td></tr>
                    <TR>
                      <TD valign="top"  align=center colspan=2>
                        <p class=F18Black><u>Camp Info</u>
                     </TD>
                    </TR>
                    <TR>
                      <TD valign=top><p class=F14Blue>Dates: </p></TD>
                      <td>June 14 - 18 <br>
                      </td>
                    </TR>
<!--
                    <TR>
                      <TD width=100 valign=top><p class=F14Blue>Ages: </p></TD>
                      <td>U7 - U13 Boys and Girls.<br>
                        Space is limited to 12 campers per gender per age group.</td>
                    </TR>
-->
                    <TR>
                      <TD width=100 valign=top><p class=F14Blue>Location: </p></TD>
                      <td>Netsports in Morrisville</td>
                    </TR>

                    <TR>
                      <TD valign=top><p class=F14Blue>Cost: </p></TD>
                      <td>$275 for full day: 9am - 4pm (includes Camp T-shirt)
                      </td>
                    </TR>
                    <TR>
                      <TD colspan=2>Early drop off ( 8-9 am ) and/or late pick-up (4-5 pm)
                        are available: $25 per week.</TD>
                    </TR>

                    <TR>
                      <TD width=100 valign=top><p class=F14Blue>Registration Deadline: </p></TD>
                      <td>June 8 </td>
                    </TR>
                    <TR>
                      <TD colspan=2>Lunch and snacks are NOT provided. Players should bring their lunch and snacks 
                       each day as this is not provided.
                      </TD>
                    </TR>

                    <TR>
                      <TD colspan=2><b>Sample Daily Schedule</b>
                      </TD>
                    </TR>
                    <TR>
                      <TD colspan=2>9:00 - 5 v 2<br>
				9:30 - Speed, Agility, Quickness<br>
				10:30 - break<br>
				11:00 - NLA session which may include dribbling, passing, 1st touch, shooting drills<br>
				12:00 - Lunch<br>
				1:30 - soccer related games which may include games such as: world cup, power&fitness, knock-out, top gun,
                        or capture the flag<br>
				2:30 - break<br>
				3:00 - soccer games<br>
				4:00 - pick up<br>
                      </TD>
                    </TR>

<!--
                    <TR>
                      <TD colspan=2>T-Shirts - each camper will receive one t-shirt. This is included in the price of 
                              camp and size should be selected when completing the online registration form on our web site.
                      </TD>
                    </TR>
                    <TR>
                      <TD colspan=2>Payment Should be mailed to:<br>
                                               Next Level Academy<br>
                                              3717 Davis Drive<br>
                                              Morrisville, NC 27560<br>
                                              Attn: Summer Camps<br>
                      </td>
                    </tr>
                    <TR>
                      <TD colspan=2>
                                              Waiver Form: Parents will be required to sign a waiver form on the first day 
                                              that their player attends camp. All parents should plan on coming inside when 
                                             dropping off their child on the first day so that you can sign the waiver form.
                      </td>
                    </tr>

                    <TR>
                      <TD colspan=2>
                                              Sign up today so you don't miss out! See you at camp!
                      </td>
                    </tr>
-->

<?
/*
  $countFile[0] = "data/Camps/MaleCounts.txt";
  $countFile[1] = "data/Camps/FemaleCounts.txt";
  $gender[0] = "Male";
  $gender[1] = "Female";
  $week[0] = "June 15 - 19";
  $week[1] = "July 13 - 17";
  $ageGroup[0] = "U7 ";
  $ageGroup[1] = "U8 ";
  $ageGroup[2] = "U9 ";
  $ageGroup[3] = "U10 ";
  $ageGroup[4] = "U11 ";
  $ageGroup[5] = "U12 ";
  $ageGroup[6] = "U13 ";
  
  $fullSession1 = "<tr><td colspan=2><b>The following sessions for the week of June 15 are full:</b> <br>";
  $fullSession2 = "<tr><td colspan=2><b>The following sessions for the week of July 13 are full:</b> <br>";

  for ( $loopcount = 0; $loopcount<2; $loopcount++ ) {
    $index = 0;
    if ( file_exists($countFile[$loopcount]) ) {
      $fh = fopen($countFile[$loopcount], "r");
      while ( $countInfo[$index] = fgets($fh, 40) )
        $index++;
      
      fclose($fh);
       
      $parms = array();
      for ( $outputindex = 0; $outputindex<$index; $outputindex++ ) {
        $parms = explode ("^", $countInfo[$outputindex], 3);
        if ( $parms[1] >= 12 )
          $fullSession1 = $fullSession1.$ageGroup[$outputindex].$gender[$loopcount]."<br>";

        if ( $parms[2] >= 12 )   
          $fullSession2 = $fullSession2.$ageGroup[$outputindex].$gender[$loopcount]."<br>";
 
      }
    }
  }

  if (  $fullSession1 != "<tr><td colspan=2><b>The following sessions for the week of June 15 are full:</b> <br>" )
    print($fullSession1."</td></tr>");

  if (  $fullSession2 != "<tr><td colspan=2><b>The following sessions for the week of July 13 are full:</b> <br>" )
    print($fullSession2."</td></tr>");

*/

?>

                    <tr>
                      <td colspan=2>
<!--
                        All registration of the Next Level Academy camps will be online by 
                        <a href=campform.php4> clicking here</a>.  
                        <p>If you have any questions please feel free to call us at 467-2299 or email us at 
                        staff@nextlevelacademy.com</p>
-->

<?
    $prgFile = "data/Campregactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("All registration of the Next Level Academy camps will be online by ");
        print("<a href=campform.php4> clicking here</a>.  ");
        print("Please mail payment ");
        print("to the address below. </p>");
        print("<p>Next Level Academy <br>Attn: Summer Camp<br>3717 Davis Drive<br> Morrisville, NC 27560</p>");

      }
      else 
        print("<b>Camp registration is currently not open.</b>");
    }
    else 
      print("<b>Camp registration is currently not open.</b>");                    
?>
                        <p>If you have any questions please feel free to call us at 467-2299 or email us at 
                        staff@nextlevelacademy.com</p>

                      </td>
                    </tr>


                    <tr><td height=5 align=center colspan=2><img src=images/line.jpg width=500></td></tr>
                    <tr><td height=5></td></tr>
                                    
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>