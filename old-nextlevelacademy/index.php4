<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>Next Level Academy</TITLE>
<script language="JavaScript">
<!--
<!--hide script from old browsers
BroW = parseInt(navigator.appVersion)
if (BroW >= 4) {Vs = true} else {Vs = false}
if (Vs == true) {
Wid = screen.availWidth - 30
Hei = screen.availHeight + 50
Left = Math.floor((Wid - 350) / 2) ; Top = Math.floor((Hei - 100) / 2)
if (document.layers) {
document.captureEvents(Event.MOUSEDOWN)
}
document.onmousedown = noClick
}
//
function noClick (e) {
flag = false
if (document.layers) {which = e.which ; if (which == 3) {flag = true}
} else {which = event.button ; if (which == 2) {flag = true}}
if (flag) {
alert('The contents on this site have been protected against right-clicking')
return false
}
}
//
//end hiding contents-->



function putdate()
{
ThisDate=new Date();
thismonth=ThisDate.getMonth()+1;
if (thismonth=="1") {monthname="January"}
if (thismonth=="2") {monthname="February"}
if (thismonth=="3") {monthname="March"}
if (thismonth=="4") {monthname="April"}
if (thismonth=="5") {monthname="May"}
if (thismonth=="6") {monthname="June"}
if (thismonth=="7") {monthname="July"}
if (thismonth=="8") {monthname="August"}
if (thismonth=="9") {monthname="September"}
if (thismonth=="10") {monthname="October"}
if (thismonth=="11") {monthname="November"}
if (thismonth=="12") {monthname="December"}

CurrentDate=monthname+' '+ThisDate.getDate();
document.write(CurrentDate);
}
 

//-->
</script> 
<META http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="description" CONTENT="Next Level Academy  - taking steps to reach your goals.">
<META NAME="Keywords" CONTENT="soccer, youth soccer,  indoor soccer, youth soccer training, soccer training, raleigh soccer, raleigh youth soccer, raleigh indoor soccer, raleigh soccer training">
<META NAME="Rating" CONTENT="General">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">

<LINK href="css/style.css" type=text/css rel=stylesheet>
<META content="MSHTML 6.00.2800.1106" name=GENERATOR></HEAD>
<BODY vLink=#150185 aLink=#ff0000 link=#150185 leftMargin=0 topMargin=0 marginheight="0" marginwidth="0">
<TABLE cellSpacing=0 cellPadding=0 width=875 align=center border=0><TBODY>
  <TR><TD height=10></TD></TR>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD width=20 background="images/topblackleft.gif" height=24>&nbsp;</TD>
          <TD align=center background="images/toplinkbg.gif" height=24>
            <STRONG><FONT color=#ffffff><A class=whitelink 
            href="aboutus.shtml">About NLA</A> | <A class=whitelink 
            href="programs.shtml">Programs</A> | <A class=whitelink 
            href="facilities.shtml">Facilities</A> | <A class=whitelink 
            href="contact.shtml">Contact</A></FONT>
            </STRONG>
            &nbsp; &nbsp; &nbsp; &nbsp &nbsp; &nbsp; &nbsp; &nbsp <FONT COLOR=#ffffff><strong>
            <script language="Javascript"><!--  
                 putdate(); 
            // --></script></strong></FONT>
          </TD>
          <TD width=20 background="images/topblackright.gif" height=24>&nbsp;</TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td align=center valign=middle width=35%><br><img src=images/tiny_nl_logo.jpg>
	      <p><EM><STRONG>&nbsp; "Taking Steps to Reach Your Goals" 
            </STRONG></EM></p>
          </td>

          <TD valign=top width=65%>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
              <TBODY>
                <tr >
                  <td height=30 colspan=3 align=middle valign=center><strong><FONT color=#ff0000>NLA Schedule for Today</font></strong></td>
                </tr>
                <tr>
                  <td align=middle width=33%>
                    <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <strong><FONT color=#ffffff>&nbsp; &nbsp;NLA Morrisville &nbsp; &nbsp;</FONT></strong>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                      <TD width=5 rowspan=3>&nbsp; </td>
                    </TR>
                    <tr>
                      <td height=150 colspan=3 background="images/graymidbottom.gif">

<?
  $currMonth = date("F");
  $currYear = date("Y");
  $currDay = date("j");
  $calFile = "data/calendars/".$currYear."/".$currMonth;

  if ( file_exists($calFile."/".$currDay.".txt") ) {
    $fh = fopen($calFile."/".$currDay.".txt","r");
    while (  $calInfo = fgets($fh, 80) ) {
      $parms = explode ("^", $calInfo, 6);
      if ( $parms[0] != "freeform" ) 
        print("&nbsp; ".$parms[0].' '.$parms[1].' '.$parms[2].' '.$parms[3].' '.$parms[4]."<br>");
      else
        print($parms[1]."<br>");
    }
    fclose($fh);
  }
  else
    print("&nbsp; Nothing scheduled today. ");
?>

 
                      </td>
                    </tr>
                    <TR>
                      <TD colspan=3>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                          <TR>
                            <TD vAlign=bottom width=18><IMG height=21 src="images/grayleftbottom.gif" width=18></TD>
                            <TD background=images/graymidbottom.gif>&nbsp;</TD>
                            <TD vAlign=bottom width=15><IMG height=21 src="images/grayrightbottom.gif" width=15></TD>
                          </TR>
                        </TBODY></TABLE>
                      </TD>
                    </TR>
                    </TBODY></TABLE>
                  </td>
                  <td align=middle width=33%>
                    <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <strong><FONT color=#ffffff> &nbsp; &nbsp; &nbsp; NLA Raleigh &nbsp; &nbsp; &nbsp;</FONT></strong>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                      <TD width=5 rowspan=3>&nbsp </td>
                    </TR>
                    <tr>
                      <td height=150 colspan=3 background="images/graymidbottom.gif" >
<?
  if ( file_exists($calFile."/".$currDay."nlar.txt") ) {
    $fh = fopen($calFile."/".$currDay."nlar.txt","r");
    while (  $calInfo = fgets($fh, 80) ) {
      $parms = explode ("^", $calInfo, 6);
      if ( $parms[0] != "freeform" ) 
        print("&nbsp; ".$parms[0].' '.$parms[1].' '.$parms[2].' '.$parms[3].' '.$parms[4]."<br>");
      else
        print($parms[1]."<br>");
    }
    fclose($fh);
  }
  else
    print("&nbsp; Nothing scheduled today.");
?>
                      </td>
                    </tr>
                    <TR>
                      <TD colspan=3>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                          <TR>
                            <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                            <TD background=images/graymidbottom.gif>&nbsp;</TD>
                            <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                          </TR>
                        </TBODY></TABLE>
                      </TD>
                    </TR>
                    </TBODY></TABLE>
                  </td>
                  <td align=middle width=34%>
                    <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <strong><FONT color=#ffffff>&nbsp; Grads / Functionals &nbsp;</FONT></strong>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                      <TD width=5 rowspan=3>&nbsp </td>
                    </TR>
                    <tr>
                      <td height=150 colspan=3 background="images/graymidbottom.gif" >
<?
  if ( file_exists($calFile."/".$currDay."func.txt") ) {
    $fh = fopen($calFile."/".$currDay."func.txt","r");
    while (  $calInfo = fgets($fh, 80) ) {
      $parms = explode ("^", $calInfo, 6);
      if ( $parms[0] != "freeform" ) 
        print("&nbsp; ".$parms[0].' '.$parms[1].' '.$parms[2].' '.$parms[3].' '.$parms[4]."<br>");
      else
        print($parms[1]."<br>");
    }
    fclose($fh);
  }
  else
    print("&nbsp; Nothing scheduled today.");
?>
                      </td>
                    </tr>
                    <TR>
                      <TD colspan=3>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                          <TR>
                            <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                            <TD background=images/graymidbottom.gif>&nbsp;</TD>
                            <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                          </TR>
                        </TBODY></TABLE>
                      </TD>
                    </TR>
                    </TBODY></TABLE>
                  </td>
                </tr>
              </tbody>
            </table>
          </TD>




        </TR>
	<TR><TD height=5>&nbsp;</TD></TR>
      </TBODY></TABLE>
    </TD>
  </TR>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=26 background="" height=21><IMG height=21 src="images/graybarleft.gif" width=26></TD>
          <TD background=images/topgraybg.gif height=21 align=center>
            <STRONG> 
              <A href="index.php4">Home</A>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;
	        <A href="calendarChoice.shtml">NLA Calendar</A>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;
              <A href="photogallery.shtml">Photo Gallery</A>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;
              <A href="faqs.shtml">FAQs</A>&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;
              <A href="mailto:staff@nextlevelacademy.com">Email</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--               <A href="clarets.shtml">Cary Clarets U23s</A>&nbsp;&nbsp;&nbsp; -&nbsp;&nbsp;&nbsp;&nbsp;
-->
             <A href="summerprog.php4">Summer Play!</A>
<!--
               <font color=#C40000>NLA Hot Line: 786-7650 </font>
-->

	    </STRONG>
	  </TD>
          <TD width=20 background="images/grayrighttop.gif" height=21>&nbsp;</TD>
        </TR>

        <TR bgcolor=#ffffff><TD height=2></TD></TR>

        <TR>
	    <TD VAlign=bottom width=26 background="images/topredbg.gif"><IMG height=94 src="images/redbarleft.gif" width=26></TD>
          <TD background="images/topredbg.gif"> 
            <font style="font-size: 18px; color: #ffffff;">
<?
  if ( file_exists("forindex.txt") ) {
    $fh = fopen("forindex.txt","r");
    $indexInfo = fread($fh, 1400);
    fclose($fh);
    print($indexInfo);
  }
  else
    print("Marquee file not found.");
?>
            </font>
          </TD>
          <TD vAlign=bottom width=20 background="images/topredbg.gif"><IMG height=94 src="images/redbarright.gif" width=20></TD>
	</TR>
	</TBODY>
      </TABLE>
    </TD>
  </TR>

  <TR><TD>&nbsp;</TD></TR>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>


              <TR><TD>&nbsp;</TD></TR>


              <TR><TD height=2></TD></TR>

                          <TR>
                            <TD width=150>
                              <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                                <TR>
                                  <TD VAlign=top width=19 background=images/topbluebg.gif>
                                    <IMG src="images/bluefooterleft.gif" width=19>
                                  </TD>
                                  <TD align=middle background=images/topbluebg.gif>
                                    <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                                  </TD>
                                  <TD VAlign=top width=19 background=images/topbluebg.gif>
                                    <IMG src="images/bluefooterright.gif" width=19>
                                  </TD>
                                </TR>
                              </TBODY></TABLE>
                            </TD>
                          </TR>
                          <TR>
                            <TD bgcolor=#e7e7e7>
                              <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                            </TD>
                          </TR>
                          <TR>
                            <TD>
                              <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                                <TR>
                                  <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                                  <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                                  <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                                </TR>
                              </TBODY></TABLE>
                            </TD>
                          </TR>

            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
<!--- start  

              <TR>
                <TD colspan=2>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>A Word From Damon</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD colspan=2 valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P>
                        �Today was an exciting day in the soccer world as a major youth soccer partnership was announced. 
				Three great soccer organizations, NLA, CASL, and the Carolina RailHawks are partnering with each other to 
				create an initiative that will impact soccer in the triangle for players, parents, coaches, and supporters. 
				Next Level Academy is excited about being a part of this initiative as it will allow us to create many opportunities 
				for players in our program. Our main goal has been and always will be in individual development for all players. 
				We want to continue to develop future talent at all levels and look forward to making a difference with our technical focus. 
				Next Level Academy is committed as an organization to changing the game one player at a time.� </p>
				<p>- Damon Nahas <br>CEO, Next Level Academy

			</P> 
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD colspan=2>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>
-->


              <TR>
                <TD colspan=2>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Welcome!</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD colspan=2 valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P><strong><FONT COLOR=#ff0000>Next Level Academy</FONT></strong> 
                        provides advanced technical soccer training to develop your player�s agility and skill 
    				level. Our programs include NLA Academies at Morrisville and Raleigh/Wake Forest, NLA Development 
				Centers, NLA Functionals, and Specialty Training. All Next Level programs are staffed and 
				operated by trained professionals with years of experience in youth skill development and coaching. </p>
				<p>Currently we have two locations, one in Morrisville and one in Raleigh/Wake Forest. Our Morrisville 
				location operates at the NetSports facility and offers our full range of programs. Our Raleigh/Wake Forest 
				location operates at The Factory and offers the Academy and Development Centers programs. To get more information
				 on our programs, please click on that program�s name listed to the left.

                        </p>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD colspan=2>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              <TR>
                <TD colspan=2>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                          <TR>
                            <TD>
                              <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                                <TR>
                                  <TD vAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                                  <TD align=middle background=images/darkgraybg.gif>
                                    <STRONG><FONT color=#ffffff>Registration Center</FONT></STRONG>
                                  </TD>
                                  <TD vAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                                </TR>
                              </TBODY></TABLE>
                            </TD>
                          </TR>

                          <TR><TD height=2></TD></TR>

                          <TR>
                            <TD VAlign=top background=images/graymidbottom.gif> 
                              <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                                <TR>
                                  <TD VAlign=top> 
                                    <P>Registrations for all programs and tryouts are handled exclusively
                                       by our on-line registration system.  Registration will be activated 
                                       at an appropriate time and deactivated when the program is full. 
                                    </P>
<?
  $count = 0;
  $regis = array();
  $regis[0] = "";
$regis[1] = "";
$regis[2] = "";
$regis[3] = "";
$regis[4] = "";

    $prgFile = "data/Academy/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        $regis[0]="<p>&nbsp; &nbsp; <a href=\"acadcenterinfo.php4\">Older Development Centers</a></p>";
        $count++;
      }
    }

    $prgFile = "data/Centers/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        $regis[1]="<p>&nbsp; &nbsp; <a href=\"acadcenterinfo.php4\">Younger Development Centers</a></p>";
        $count++;
      }
    }

    $prgFile = "data/Tryouts/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        $regis[2]="<p>&nbsp; &nbsp; <a href=\"tryoutinfo.php4\">Next Level Tryouts - Morrisville</a></p>";
        $count++;
      }
    }
    $prgFile = "data/Tryouts/RegactiveRal.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        $regis[3]="<p>&nbsp; &nbsp; <a href=\"raltryoutinfo.php4\">Next Level Tryouts - Raleigh</a></p>";
        $count++;
      }
    }

    $prgFile = "data/Functionals/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $continue = 0;
      $fh = fopen($prgFile, "r");
      for ( $i=0;  $i<12; $i++ ) {
        $active = chop(fgets($fh,25));

        if ( $active == "open" )
          $continue = 1;
      }
      fclose($fh);
      if ( $continue == 1 ) {
        $regis[4]="<p>&nbsp; &nbsp; <a href=\"functionals.php4\">Functionals</a></p>";
        $count++;
      }
    }

  if ( $count != 0 ) {
    print("<P>Registration for the following programs is currently active:</p>");
    for ( $i=0; $i<5; $i++ )
      print($regis[$i]);
  }
  else {
    print("<P>At this time registration for all Next Level Academy programs is closed.</p>");
  }                      
?>
<!--
                                   <p>&nbsp; &nbsp; <a href="campinfo.php4">Summer Camps</a></p>
-->

                                  </TD>
                                </TR>
                              </TBODY></TABLE>
                            </TD>
                          </TR>

                          <TR>
                            <TD>
                              <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                                <TR>
                                  <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                                  <TD background=images/graymidbottom.gif>&nbsp;</TD>
                                  <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                                </TR>
                              </TBODY></TABLE>
                            </TD>
                          </TR>

<!--				
                          <tr><td height=5> &nbsp; </td></tr>
                          <TR>
                            <TD>
                              <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                                <TR>
                                  <TD vAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                                  <TD align=middle background=images/darkgraybg.gif>
                                    <STRONG><FONT color=#ffffff>Employment Opportunities</FONT></STRONG>
                                  </TD>
                                  <TD vAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                                </TR>
                              </TBODY></TABLE>
                            </TD>
                          </TR>
				
                          <TR><TD height=2></TD></TR>

                          <TR>
                            <TD VAlign=top background=images/graymidbottom.gif> 
                              <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                                <TR>
                                  <TD VAlign=top> 
                                    <P>The NLA Program Assistant position has been filled.  
                                    </P>
                                    <P>Next Level Academy is continuing it's 
					support of the Raleigh Elite and is hiring a General Manager for this summer's 
					season. 
                                    Click <a href=job2.php4><font style="color: #0000ff;">here</font></a> for more information.  
                                    </P>

 
                                  </TD>
                                </TR>
                              </TBODY></TABLE>
                            </TD>
                          </TR>

                          <TR>
                            <TD>
                              <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                                <TR>
                                  <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                                  <TD background=images/graymidbottom.gif>&nbsp;</TD>
                                  <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                                </TR>
                              </TBODY></TABLE>
                            </TD>
                          </TR>


-->


                        </TBODY></TABLE>
                      </TD>
                      <TD width=15>&nbsp;</TD>
                      <TD VAlign=top>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Members</FONT></STRONG>
                      </TD>
                      <TD vAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD background=images/graymidbottom.gif> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <FORM method="post" action="nlalogin.php4">
                    <TR>
                      <TD vAlign=top>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                          <TR>
                            <TD align=right height=25>Username:</TD>
                          </TR>
                          <TR>
                            <TD align=right height=25>Password:</TD>
                          </TR>
                        </TBODY></TABLE>
                      </TD>
                      <TD>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                          <TR>
                            <TD align=middle> 
                              <INPUT id=username maxLength=30 size=10 name=username style="font-size: 14px;">
                            </TD>
                          </TR>
                          <TR>
                            <TD align=middle><INPUT id=password type=password maxLength=10 size=10 name=password style="font-size: 14px;">
                            </TD>
                          </TR>
                        </TBODY></TABLE>
                      </TD>
                    </TR>
                    <TR>
                      <TD align=middle colspan=2><INPUT type=submit value=Login name=Submit style="font-size: 14px;">
                      </TD>
                    </TR>
                    </FORM>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
                        </TBODY></TABLE>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

  		<tr><td height=5> &nbsp; </td></tr>
  		<tr>
    		  <TD>
      	  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        	    <TR>

          		<td align=center width=33%><a href=http:\\www.burnleyfc.com>
            	  <img src=images/BFCsmall.png border=0><br>
            	  Burnley Football Club</a>
          		</td>

          		<td align=center width=33%><a href=http:\\www.caslnc.com>
            	  <img src=images/CASLLogo.gif border=0><br>
            	  In association with the Capital Area Soccer League.</a>
          		</td>

          		<td align=center width=33%><a href=http:\\www.fccary.com>
            	  <img src=images/fccary.gif border=0><br>
            	  FC Cary</a>
          		</td>
        	    </tr>
      	  </TBODY></TABLE>
    		  </TD>
  		</tr>
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>
<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>
