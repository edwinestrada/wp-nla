<div class="social-icons pull-center">
  <div class="span12">
    <ul class="ch-grid">
      <li>
        <div class="ch-item">
          <div class="ch-info">
            <?php
              $social_icon_front_facebook = get_field('social_icon_front_facebook', 'option');
              $social_icon_back_facebook  = get_field('social_icon_back_facebook', 'option');
              $social_link_facebook       = get_field('social_link_facebook', 'option');

              if($social_icon_back_facebook): ?>
                <style type="text/css">
                  .ch-img-1                       {
                    background-image: url("<?php echo $social_icon_front_facebook['url']; ?>")
                  }
                </style>
              <?php endif; ?>
            <div class="ch-info-front ch-img-1"></div>
            <div class="ch-info-back">
              <a href="<?php echo $social_link_facebook; ?>" target="_blank">
                <img src="<?php echo $social_icon_back_facebook['url']; ?>">
              </a>
            </div>
          </div>
        </div>
      </li>
      <li>
        <div class="ch-item">
          <div class="ch-info">
            <?php
              $social_icon_front_twitter = get_field('social_icon_front_twitter', 'option');
              $social_icon_back_twitter  = get_field('social_icon_back_twitter', 'option');
              $social_link_twitter       = get_field('social_link_twitter', 'option');

              if($social_icon_back_twitter): ?>
                <style type="text/css">
                  .ch-img-2                       {
                    background-image: url("<?php echo $social_icon_front_twitter['url']; ?>")
                  }
                </style>
              <?php endif; ?>
            <div class="ch-info-front ch-img-2"></div>
            <div class="ch-info-back">
              <a href="<?php echo $social_link_twitter; ?>" target="_blank">
                <img src="<?php echo $social_icon_back_twitter['url']; ?>">
              </a>
            </div>
          </div>
        </div>
      </li>
<!--                <li>
        <div class="ch-item">
          <div class="ch-info">
            <div class="ch-info-front ch-img-3"></div>
            <div class="ch-info-back"> <img src="<?php //echo get_stylesheet_directory_uri(); ?>/image/social-icons/in_dark.png" alt="" /> </div>
          </div>
        </div>
      </li>
      <li>
        <div class="ch-item">
          <div class="ch-info">
            <div class="ch-info-front ch-img-4"></div>
            <div class="ch-info-back"> <img src="<?php //echo get_stylesheet_directory_uri(); ?>/image/social-icons/dribbble_dark.png" alt="" /> </div>
          </div>
        </div>
      </li>
-->              </ul>
  </div><!-- span12 -->
</div><!-- .social-icons -->