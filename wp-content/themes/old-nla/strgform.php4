<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                <TD align=middle background=images/darkgraybg.gif>
<?
  $prgtype= $_GET['prgtype'];
  $prgday = $_GET['prgday'];
  if ( $prgtype == "FB" )
    $prgage = $_GET['age'];

  $dayofWeek = jddayofweek($prgday, 1); 

  $maxITplayers = 4;
  $maxGTplayers = 4;
  $maxGKplayers = 6;
  $maxFBplayers = 40;

  switch ( $prgtype )
  {
    case "U7":
      print("<STRONG><FONT color=#ffffff>Specialty Training - U7 thru U10 registration</FONT></STRONG>");
      $signupfile = "data/stdata/Rec".$prgday.".txt";
      break;

    case "U11":
      print("<STRONG><FONT color=#ffffff>Specialty Training - U11 thru U14 registration</FONT></STRONG>");
      $signupfile = "data/stdata/Cla".$prgday.".txt";
      break;

    case "Gk":
      print("<STRONG><FONT color=#ffffff>Specialty Training - Goalkeeping registration</FONT></STRONG>");
      $signupfile = "data/stdata/GK".$prgday.".txt";
      break;

    case "FB":
      print("<STRONG><FONT color=#ffffff>Specialty Training - Finishing/Ball Striking registration</FONT></STRONG>");
      $signupfile = "data/stdata/FB".$prgday.".txt";
      break;


  }

?>
                </TD>
                <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
              </TR>
            </tbody></table>
          </td>
        </TR>


<?

// read in default coaches assignments.
// array is indexed by day and type ....  coach[Monday][U7-1] = name

  $fh = fopen("data/stdata/scoaches.txt", "r");
  while ( $coachInfo= fgets($fh, 512) ) {
    $parms = explode (",", $coachInfo, 2);
    switch ( chop($parms[0]) ) {
      case "Monday":
      case "Tuesday":
      case "Wednesday":
      case "Thursday":
      case "Friday":
      case "Saturday":
      case "Sunday":
        $dayindex = chop($parms[0]);
      break;

      case "end";
      break;

      default:
        $sessindex = chop($parms[0]);
        $cname = chop($parms[1]);
        $coach[$dayindex][$sessindex] = $cname;
      break;

    }
  }
  fclose($fh);

// check to see if default assignment is overridden
  $ncIndex = 0;
  $coachassignment = "data/stdata/c".$prgday.".txt";
  if ( file_exists($coachassignment) ) {
    $fh = fopen($coachassignment, "r");
    while ( $coachInfo= fgets($fh, 512) ) {
      $parms = explode (",", $coachInfo, 2);
      $cname = chop($parms[0]);
      $stype = chop ($parms[1]);
      if ( $stype == "None" )
        $noCoach[$ncIndex++] = $cname;
      else 
        $coach[$dayofWeek][$stype] = $cname;
    }
  }

  switch ( $prgtype )       
  {
   
    case "U7":
      $numberITplayers = $maxITplayers;
      $numberGTplayers = $maxGTplayers;
      if ( file_exists($signupfile) ) {
        $fh = fopen($signupfile, "r");
        while ( $playerInfo= fgets($fh, 512) ) {

          $parms = explode ("^", $playerInfo, 2);
          if ( chop($parms[1]) == "2" )
            $numberGTplayers--;
          else
            $numberITplayers--;
        }
      }
      if ( $numberGTplayers > 0 )
        $registerGT = "true";
      else
        $registerGT = "false";

      if ( $numberITplayers > 0 )
        $registerIT = "true";
      else
        $registerIT = "false";

      print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"75%\" border=0><TBODY>\n");
      print("<tr><td align=center valign=top><table cellSpacing=0 cellPadding=0 width=\"50%\" border=0><TBODY>\n");
      print("<tr><td height=10 valign=top>&nbsp;<form method=\"post\" action=\"stregister.php4?&prgtype=U7-1&prgday=".$prgday."\" </td></tr>\n");
      print("<tr><td align=center valign=top>U7-U10<br>Specialty Training</b></td></tr>");
      print("<tr><td align=center>1 - 4 Players</td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");

      for ($cp = 0; $cp < 2; $cp++ ) {
        if ( $cp == 0 ) 
          $cpindex = "U7-1";
        else
          $cpindex = "U7-2";
        $printcoach[$cp] = $coach[$dayofWeek][$cpindex];
        if ( $printcoach[$cp]  == "None" )
          $printcoach[$cp]  = "TBD";
        else {
          for ( $i=0; $i < $ncIndex; $i++ ) {
            if ( $printcoach[$cp]  == $noCoach[$i] )
              $printcoach[$cp]  = "TBD";
          }
        }
      }
     print("<tr><td  align=center>Coaches:<br>".$printcoach[0]." / ".$printcoach[1]." </td></tr>");


      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxITplayers ) {
        print("<tr><td align=center><select style= \"font-size: 12px;\" name=\"session\">\n");
        if ( $numberITplayers > 0 ) {
          print("<option selected>Open &nbsp;\n</select></td></tr>");
          $numberITplayers--;
        }
        else
          print("<option selected>Filled &nbsp;\n</select></td></tr>");

        print("<tr><td height=5>&nbsp;</td></tr>\n");
        $loopindex++;
      }
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      print("<tr><td><INPUT type=\"hidden\" name=\"coach\" value=\"".$printcoach[0]."^".$printcoach[1]."\">\n");

      if ( $registerIT == "true")
	  print("<TR><TD align=center><INPUT type=\"submit\" style=\"font-size: 12px;\" value=\"Register\"></TD></tr>\n");
      else
        print("<TR><TD align=center>Session is full</TD><tr>\n");

      print("</form></tbody></table></td>\n");


      print("<td align=center valign=top><table cellSpacing=0 cellPadding=0 width=\"50%\" border=0><TBODY>\n");
      print("<tr><td height=10>&nbsp;<form method=\"post\" action=\"stregister.php4?&prgtype=U7-2&prgday=".$prgday."\" </td></tr>\n");
      print("<tr><td align=center>U7-U10<br>Specialty Training</b></td></tr>");
      print("<tr><td align=center>1 - 4 Players</td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");

      print("<tr><td  align=center>Coaches:<br>".$printcoach[0]." / ".$printcoach[1]." </td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxGTplayers ) {
        print("<tr>");
        if ( $numberGTplayers > 0 ) {
          print("<td align=center><select style= \"font-size: 12px;\" name=\"session\">\n<option selected>Open &nbsp;\n</select></td></tr>");
          $numberGTplayers--;
        }
        else
          print("<td align=center><select style= \"font-size: 12px;\" name=\"session\">\n<option selected>Filled &nbsp;\n</select></td></tr>");

        print("<tr><td height=5>&nbsp;</td></tr>\n");
        $loopindex++;
      }

      print("<tr><td height=5>&nbsp;</td></tr>\n");
      if ( $registerGT == "true" )
	  print("<TD align=center><INPUT type=\"submit\" style=\"font-size: 12px;\" value=\"Register\"></TD></TR>\n");
      else
        print("<TR><TD align=center>Session is full</TD></tr>\n");

      print("<tr><td><INPUT type=\"hidden\" name=\"coach\" value=\"".$printcoach[0]."^".$printcoach[1]."\">\n");
      print("</form></tbody></table></td></tr>\n");
      
      print("<tr><td colspan=2 height=10>&nbsp;</form></td></tr>\n");
      print("</tbody></table></td></tr>\n");

    break;

    case "U11":
      $numberITplayers = $maxITplayers;
      $numberGTplayers = $maxGTplayers;
      if ( file_exists($signupfile) ) {
        $fh = fopen($signupfile, "r");
        while ( $playerInfo= fgets($fh, 512) ) {
          $parms = explode ("^", $playerInfo, 2);
          if ( chop($parms[1]) == "2" )
            $numberGTplayers--;
          else
            $numberITplayers--;
        }
      }
      if ( $numberGTplayers > 0 )
        $registerGT = "true";
      else
        $registerGT = "false";

      if ( $numberITplayers > 0 )
        $registerIT = "true";
      else
        $registerIT = "false";

      print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"75%\" border=0><TBODY>\n");
      print("<tr><td align=center valign=top><table cellSpacing=0 cellPadding=0 width=\"50%\" border=0><TBODY>\n");
      print("<tr><td height=10 valign=top>&nbsp;<form method=\"post\" action=\"stregister.php4?&prgtype=U11-1&prgday=".$prgday."\" </td></tr>\n");
      print("<tr><td align=center valign=top>U11-U14<br>Specialty Training</b></td></tr>");
      print("<tr><td align=center>1 - 4 Players</td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");

      for ($cp = 0; $cp < 2; $cp++ ) {
        if ( $cp == 0 ) 
          $cpindex = "U11-1";
        else
          $cpindex = "U11-2";
        $printcoach[$cp] = $coach[$dayofWeek][$cpindex];
        if ( $printcoach[$cp]  == "None" )
          $printcoach[$cp]  = "TBD";
        else {
          for ( $i=0; $i < $ncIndex; $i++ ) {
            if ( $printcoach[$cp]  == $noCoach[$i] )
              $printcoach[$cp]  = "TBD";
          }
        }
      }
     print("<tr><td  align=center>Coaches:<br>".$printcoach[0]." / ".$printcoach[1]." </td></tr>");

      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxITplayers ) {
        print("<tr><td align=center><select style= \"font-size: 12px;\" name=\"session\">\n");
        if ( $numberITplayers > 0 ) {
          print("<option selected>Open &nbsp;\n</select></td></tr>");
          $numberITplayers--;
        }
        else
          print("<option selected>Filled &nbsp;\n</select></td></tr>");

        print("<tr><td height=5>&nbsp;</td></tr>\n");
        $loopindex++;
      }
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      if ( $registerIT == "true")
	  print("<TR><TD align=center><INPUT type=\"submit\" style=\"font-size: 12px;\" value=\"Register\"></TD></tr>\n");
      else
        print("<TR><TD align=center>Session is full</TD></tr>\n");

      print("<tr><td><INPUT type=\"hidden\" name=\"coach\" value=\"".$printcoach[0]."^".$printcoach[1]."\">\n");
      print("</form></tbody></table></td>\n");

      print("<td align=center valign=top><table cellSpacing=0 cellPadding=0 width=\"50%\" border=0><TBODY>\n");
      print("<tr><td height=10>&nbsp;<form method=\"post\" action=\"stregister.php4?&prgtype=U11-2&prgday=".$prgday."\" </td></tr>\n");
      print("<tr><td align=center>U11-14<br>Spceialty Training</b></td></tr>");
      print("<tr><td align=center>1 - 4 Players</td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");

      print("<tr><td  align=center>Coaches:<br>".$printcoach[0]." / ".$printcoach[1]." </td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxGTplayers ) {
        print("<tr>");
        if ( $numberGTplayers > 0 ) {
          print("<td align=center><select style= \"font-size: 12px;\" name=\"session\">\n<option selected>Open &nbsp;\n</select></td></tr>");
          $numberGTplayers--;
        }
        else
          print("<td align=center><select style= \"font-size: 12px;\" name=\"session\">\n<option selected>Filled &nbsp;\n</select></td></tr>");

        print("<tr><td height=5>&nbsp;</td></tr>\n");
        $loopindex++;
      }
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      if ( $registerGT == "true" )
	  print("<TD align=center><INPUT type=\"submit\" style=\"font-size: 12px;\" value=\"Register\"></TD></TR>\n");
      else
        print("<TR><TD align=center>Session is full</TD></tr>\n");

      print("<tr><td><INPUT type=\"hidden\" name=\"coach\" value=\"".$printcoach[0]."^".$printcoach[1]."\">\n");
      print("</form></tbody></table></td></tr>\n");
      
      print("<tr><td colspan=2 height=10>&nbsp;</form></td></tr>\n");
      print("</tbody></table></td></tr>\n");

    break;

    case "Gk":
      $numberplayers = $maxGKplayers;
      if ( file_exists($signupfile) ) {
        $fh = fopen($signupfile, "r");
        while ( $playerInfo= fgets($fh, 512) ) {
          $numberplayers--;
        }
      }
      if ( $numberplayers > 0 )
        $register = "true";
      else
        $register = "false";

      print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"75%\" border=0><TBODY>\n");
      print("<tr><td height=10 valign=top>&nbsp;<form method=\"post\" action=\"stregister.php4?&prgtype=GK&prgday=".$prgday."\" </td></tr>\n");
      print("<tr><td align=center valign=top><b>Goalkeeper Training</b></td></tr>");
      print("<tr><td align=center>1 - 6 Players</td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $printcoach = $coach[$dayofWeek]['GK'];
      if ( $printcoach == "None" )
        $printcoach = "TBD";
      else {
        for ( $i=0; $i < $ncIndex; $i++ ) {
          if ( $printcoach == $noCoach[$i] )
            $printcoach = "TBD";
        }
      }
     print("<tr><td  align=center>Coach: ".$printcoach."</td></tr>");

      // print("<tr><td  align=center>Coach: ".$coach[$dayofWeek]['GK']."</td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxGKplayers ) {
        print("<tr><td align=center><select style= \"font-size: 12px;\" name=\"session\">\n");
        if ( $numberplayers > 0 ) {
          print("<option selected>Open &nbsp;\n</select></td></tr>");
          $numberplayers--;
        }
        else
          print("<option selected>Filled &nbsp;\n</select></td></tr>");

        print("<tr><td height=5>&nbsp;</td></tr>\n");
        $loopindex++;
      }
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      if ( $register == "true")
	  print("<TR><TD align=center><INPUT type=\"submit\" style=\"font-size: 12px;\" value=\"Register\"></TD></tr>\n");
      else
        print("<TR><TD align=center>Session is full</TD></tr>\n");

      print("<tr><td><INPUT type=\"hidden\" name=\"coach\" value=\"".$printcoach."\">\n");      
      print("<tr><td height=10>&nbsp;</form></td></tr>\n");
      print("</tbody></table></td></tr>\n");

    break;

    case "FB":
      $register = "false";
      $numberplayers = $maxFBplayers - 4;
      $numberGKplayers = 4;
      if ( file_exists($signupfile) ) {
        $fh = fopen($signupfile, "r");
        while ( $playerInfo= fgets($fh, 512) ) {
          $parms = explode ("^", $playerInfo, 2);
          if ( chop($parms[1]) == "GK" )
            $numberGKplayers--;
          else
            $numberplayers--;
        }
      }

/*
      $numberplayers = $maxFBplayers;
      if ( file_exists($signupfile) ) {
        $fh = fopen($signupfile, "r");
        while ( $playerInfo = fgets($fh, 512) )
          $numberplayers--;
      }
*/

      if ( $numberplayers > 0 )
        $register = "true";

      print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"75%\" border=0><TBODY>\n");
      print("<tr><td height=10 valign=top>&nbsp;<form method=\"post\" action=\"stregister.php4?&prgtype=FB&prgday=".$prgday."&age=".$prgage."\"</td></tr>\n");
      print("<tr><td colspan=4 align=center valign=top><b>Finishing / Ball Striking</b></td></tr>");
      print("<tr><td colspan=4 align=center>1 - 40 Players</td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");

      $printcoach = $coach[$dayofWeek]['Finish'];
      if ( $printcoach == "None" )
        $printcoach = "TBD";
      else {
        for ( $i=0; $i < $ncIndex; $i++ ) {
          if ( $printcoach == $noCoach[$i] )
            $printcoach = "TBD";
        }
      }
     print("<tr><td colspan=4 align=center>Coach: ".$printcoach."</td></tr>");

      //print("<tr><td colspan=4 align=center>Coach: ".$coach[$dayofWeek]['Finish']."</td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;

      $firstRow = "true";
      while ( $loopindex < $maxFBplayers ) {
        print("<tr>");
        if ( $firstRow == "true" ) {
          $firstRow = "false";
          $boxOpen = "GK Open &nbsp;";
          $boxFill = "GK Filled &nbsp;";
          $savenumberplayers = $numberplayers;
          $numberplayers = $numberGKplayers;
        } 
        else {
          if ( $loopindex == 4 ) {
            $boxOpen = "Open &nbsp;";
            $boxFill = "Filled &nbsp;";
            $numberplayers = $savenumberplayers;
          }
        }

        for ( $j=0; $j<4; $j++ ) {
          print("<td align=center><select style= \"font-size: 12px;\" name=\"session\">\n");
          if ( $numberplayers > 0 ) {
            print("<option selected>".$boxOpen."\n</select></td>");
            $numberplayers--;
          }
          else
            print("<option selected>".$boxFill."\n</select></td>");
          $loopindex++;
        }
        print("</tr><tr><td height=5>&nbsp;</td></tr>\n");
  
      }
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      if ( $register == "true")
	  print("<TR><TD colspan=4 align=center><INPUT type=\"submit\" style=\"font-size: 12px;\" value=\"Register\"></TD></tr>\n");
      else
        print("<TR><TD colspan=4 align=center>Session is full</TD></tr>\n");

      print("<tr><td><INPUT type=\"hidden\" name=\"coach\" value=\"".$printcoach."\">\n");    
      print("<tr><td height=10>&nbsp;</form></td></tr>\n");
      print("</tbody></table></td></tr>\n");

    break;

  }
  
?>
        <TR>
          <TD>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                <TD background=images/graymidbottom.gif>&nbsp;</TD>
                <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>       
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>