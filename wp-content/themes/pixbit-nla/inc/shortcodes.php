<?php
/**
 * @package WordPress
 * @subpackage wolftech
 */

////////////////
// Shortcodes //
////////////////

function section_header_shortcode($atts, $content = null) {
  extract(shortcode_atts(array(
    "title" => 'Section'
    ,"subtitle" => 'Section'
    ,"direction" => 'ltr'
  ), $atts));
  ?>
  <?php
    switch($direction){
      case 'ltr': ?>
        <div class="heading-from-left">
          <div class="from_left_top top_box_left"></div>
          <div class="bg-blue">
            <div class="container">
              <div class="icon">
                <h1><?php echo $title; ?></h1>
                <p><?php echo $subtitle; ?></p>
              </div>
            </div>
          </div>
          <div class="from_left_bot bot_box_left"></div>
        </div>
        <?php break;
      case 'rtl': ?>
        <div class="heading-from-right">
          <div class="from_right_top top_box_right"></div>
          <div class="bg-blue">
            <div class="container">
              <div class="icon">
                <h1 style="text-align:right"><?php echo $title; ?></h1>
                <p style="text-align:right"><?php echo $subtitle; ?></p>
              </div>
            </div>
          </div>
          <div class="from_right_bot bot_box_right"></div>
        </div>
        
        <?php break;
      default:
        ?>
        <div class="heading-from-left">
          <div class="from_left_top top_box_left"></div>
          <div class="bg-blue">
            <div class="container">
              <div class="icon">
                <h1><?php echo $title; ?></h1>
                <p><?php echo $subtitle; ?></p>
              </div>
            </div>
          </div>
          <div class="from_left_bot bot_box_left"></div>
        </div>
        <?php
    }
  ?>
  <?php
}
add_shortcode('section_header', 'section_header_shortcode');

function subsection_header_shortcode($atts, $content = null) {
  extract(shortcode_atts(array(
    "title" => 'Section'
  ), $atts));
  ?>
  <div class="span12 pull-center">
    <h3 class="subheading"><?php echo $title; ?></h3>
  </div>
  <?php
}
add_shortcode('subheading', 'subsection_header_shortcode');

// This is the custom shortcode for the slides Loop on the Main Page
function ACFnewsLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "max" => '5',
    "query" => ''
  ), $atts));
  ?>

  <div id="news">
  <h3>News:</h3>
  <ul>
    <?php
    $x = 0;
    while(has_sub_field('news', 'options') && ($x < $max)):
      $types = array(
          'project',
          'video',
          'people',
          'page',
          'post',
          'external'
        );

      foreach ($types as $type) {
        if(get_row_layout() == $type.'_news_entry'){ ?>
          <?php
            $date = DateTime::createFromFormat('Ymd', get_sub_field('date'));
            $dateOutput = $date->format('M d, Y');
          ?>
          <a href="<?php the_sub_field($type); ?>">
            <li>
              <strong>
                <?php echo $dateOutput; ?>
              </strong>
              <?php the_sub_field('excerpt'); ?><br>
            </li>
          </a>
        <?php }
      }
      $x++;
    endwhile;
    ?>
  </ul>
  </div><!-- #news -->
  <?php
}
add_shortcode("ACFnews", "ACFnewsLoop");

// This is the custom shortcode for the slides Loop on the Main Page
function ACFslidesLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "query" => '',
  ), $atts));

  ?>
    <div id="slider-container">
      <div class="slider-wrapper theme-default">
        <div id="slider" class="nivoSlider">
  <?php
  while(has_sub_field('slides', 'options')):
    $types = array(
        'project',
        'video',
        'people',
        'page',
        'post',
        'publication',
        'outreach'
      );

    foreach ($types as $type) {
      if(get_row_layout() == $type.'_slide'){ ?>
        <?php
          $link = get_sub_field('link_to');
        ?>
        <a href="<?php echo $link ?>">
          <?php
            $slide = get_sub_field('image');
            $url = $slide['url'];
            $thumb = wp_get_attachment_image( $slide['id'], 'slide-thumb' );
            echo $thumb;
          ?>
            <div class="nivo-html-caption">
              <?php echo get_sub_field('caption'); ?>
            </div>            
        </a>
      <?php }
    }
  endwhile;
  ?>
        </div><!-- #slider .nivoSlider -->
      </div><!-- .slider-wrapper -->
    </div><!-- #slider-container -->
  <?php
}
add_shortcode("ACFslides", "ACFslidesLoop");

// This is the custom shortcode for the slides Loop on the Main Page
function ACFprojectSlidesLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "query" => '',
  ), $atts));

  ?>
    <div id="slider-container">
      <div class="slider-wrapper theme-default">
        <div id="slider" class="nivoSlider">
  <?php
  while(the_flexible_field('project_slides')):
    $types = array(
        'image'
      );

    foreach ($types as $type) {
      if(get_row_layout() == $type.'_slide'): ?>
        <!-- Slideshow Presentation Slides -->
        <a href="<?php echo $link ?>">
          <?php
            $title = get_sub_field('title');
            $caption = get_sub_field('caption');
            $slide = get_sub_field('image');
            $url = $slide['url'];
            $thumb = wp_get_attachment_image( $slide['id'], 'slide-thumb' );
            echo $thumb;
          ?>
            <div class="nivo-html-caption">
              <?php echo $caption; ?>
            </div>            
        </a>
      <?php endif;
    }
  endwhile;
  ?>
        </div><!-- #slider .nivoSlider -->
      </div><!-- .slider-wrapper -->
    </div><!-- #slider-container -->
  <?php
}
add_shortcode("ACFprojectSlides", "ACFprojectSlidesLoop");

// This is the custom shortcode for the slides Loop on the Main Page
function ACFoutreachSlidesLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "query" => '',
  ), $atts));

  ?>
    <div id="slider-container">
      <div class="slider-wrapper theme-default">
        <div id="slider" class="nivoSlider">
  <?php
  while(the_flexible_field('outreach_slides')):
    $types = array(
        'image'
      );

    foreach ($types as $type) {
      if(get_row_layout() == $type.'_slide'): ?>
        <!-- Slideshow Presentation Slides -->
        <a href="<?php echo $link ?>">
          <?php
            $title = get_sub_field('title');
            $caption = get_sub_field('caption');
            $slide = get_sub_field('image');
            $url = $slide['url'];
            $thumb = wp_get_attachment_image( $slide['id'], 'outreach-thumbnail' );
            echo $thumb;
          ?>
            <div class="nivo-html-caption">
              <?php echo $caption; ?>
            </div>            
        </a>
      <?php endif;
    }
  endwhile;
  ?>
        </div><!-- #slider .nivoSlider -->
      </div><!-- .slider-wrapper -->
    </div><!-- #slider-container -->
  <?php
}
add_shortcode("ACFoutreachSlides", "ACFoutreachSlidesLoop");

// This is the custom shortcode for the slides Loop on the Main Page
function slidesLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "query" => '',
  ), $atts));
  global $wp_query,$paged,$post;
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  if(!empty($query)){
    $query .= $query;
  }
  $wp_query->query($query);
  ob_start();
  ?>
  <?php while (have_posts()) : the_post(); ?>
    <a href="<?php the_field('slide_link_to_project'); ?>">
    <?php the_post_thumbnail( 'slide-thumbnail' ); ?>
    </a>
  <?php endwhile;?>
  <?php $wp_query = null; $wp_query = $temp;
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}
add_shortcode("slides", "slidesLoop");

// This is the custom shortcode for the slides Loop on the Main Page
function mobileslidesLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "query" => '',
  ), $atts));
  global $wp_query,$paged,$post;
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  if(!empty($query)){
    $query .= $query;
  }
  $wp_query->query($query);
  ob_start();
  ?>
          <?php while (have_posts()) : the_post(); ?>
            <?php the_post_thumbnail( 'mobile-slide-thumbnail' ); ?>
        <?php endwhile;?>
  <?php $wp_query = null; $wp_query = $temp;
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}
add_shortcode("mobileslides", "mobileslidesLoop");

// This is the custom shortcode for the ads Loop on the Main Page
function adsLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "query" => '',
  ), $atts));
  global $wp_query,$paged,$post;
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  if(!empty($query)){
    $query .= $query;
  }
  $wp_query->query($query);
  ob_start();
  ?>
          <?php while (have_posts()) : the_post(); ?>
            <div id="ad" class="grid_4">
          <a href="<?php echo get_post_meta($post->ID, "_url", $single = true); ?>" target="_blank"><?php echo get_the_post_thumbnail($page->ID, 'ad-thumbail'); ?></a>
            <div class="ad-title"><a href="<?php echo get_post_meta($post->ID, "_adurl", $single = true); ?>" class="ad-title-link" target="_blank"><?php the_title() ?></a></div>
        </div>
        <?php endwhile;?>
  <?php $wp_query = null; $wp_query = $temp;
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}
add_shortcode("ads", "adsLoop");

// This is the custom shortcode for the Knowledge Base Loop on the Main Page
function kbLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "query" => '',
  ), $atts));
  global $wp_query,$paged,$post;
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  if(!empty($query)){
    $query .= $query;
  }
  $wp_query->query($query);
  ob_start();
  ?>
          <?php while (have_posts()) : the_post(); ?>
            <li class="news-item"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> <span class="kbase-date">- <?php the_time('F j, Y, \a\t g:ia') ?></span></li>
        <?php endwhile;?>
  <?php $wp_query = null; $wp_query = $temp;
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}
add_shortcode("kb", "kbLoop");

//This is the shortcode loop for staff
function peopleLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "pagination" => 'true',
    "query" => '',
  ), $atts));
  global $wp_query,$paged,$post;
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  if($pagination == 'true'){
    $query .= '&paged='.$paged;
  }
  if(!empty($query)){
    $query .= $query;
  }
  $wp_query->query($query);
  ob_start();
  ?>
    <?php while (have_posts()) : the_post();
        if( ++$count > 5) $count = 1;
        if ($count == 1) echo '<div class="staff-row">'; ?>
        <div class="col-post">
          <?php
            if(has_post_thumbnail()) { ?>
              <?php the_post_thumbnail('staff-thumbnail'); ?>
            <?php } else { ?>
        <img src="<?php bloginfo('template_directory'); ?>/images/no-photo.jpg" alt="<?php the_title() ?>" title="<?php the_title() ?>" />
          <?php } ?>
          <div class="staff-title"><?php the_title(); ?></div>
          <?php if ( get_post_meta($post->ID, '_position', true) ) { ?>
            <div class="staff-position"><?php echo get_post_meta($post->ID, "_position", $single = true); ?></div>
      <?php } ?>
        </div>
        <?php if ($count == 5) echo '</div><!-- End row --><div class="clear"></div>'; ?>
      <?php endwhile; ?>
      <?php if ($count < 5) echo '</div><!-- End row --><div class="clear"></div>'; ?>
  <?php if(pagination == 'true'){ ?>
  <div class="navigation">
    <div class="alignleft"><?php previous_posts_link('« Previous') ?></div>
    <div class="alignright"><?php next_posts_link('More »') ?></div>
  </div>
  <?php } ?>
  <?php $wp_query = null; $wp_query = $temp;
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}
add_shortcode("people", "peopleLoop");

//This is the shortcode loop for current full time staff
function peoplefullLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "pagination" => 'true',
    "query" => '',
  ), $atts));
  global $wp_query,$paged,$post;
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  if($pagination == 'true'){
    $query .= '&paged='.$paged;
  }
  if(!empty($query)){
    $query .= $query;
  }
  $wp_query->query($query);
  ob_start();
  ?>
    <?php while (have_posts()) : the_post();
        if( ++$count > 2) $count = 1;
        if ($count == 1) echo '<div class="full-staff-row">'; ?>
        <div class="full-col-post">
          <div class="staff-left">
            <?php
              if(has_post_thumbnail()) { ?>
                <?php the_post_thumbnail('staff-thumbnail'); ?>
              <?php } else { ?>
            <img src="<?php bloginfo('template_directory'); ?>/images/no-photo.jpg" alt="<?php the_title() ?>" title="<?php the_title() ?>" />
            <?php } ?>
          </div>
          <div class="staff-right">
            <div class="full-staff-title"><?php the_title(); ?></div>
            <?php if ( get_post_meta($post->ID, '_position', true) ) { ?>
              <div class="full-staff-position"><?php echo get_post_meta($post->ID, "_position", $single = true); ?></div>
        <?php } ?>
          </div>
        </div>
        <?php if ($count == 2) echo '</div><!-- End row --><div class="clear"></div>'; ?>
      <?php endwhile; ?>
      <?php if ($count < 2) echo '</div><!-- End row --><div class="clear"></div>'; ?>
  <?php if(pagination == 'true'){ ?>
  <div class="navigation">
    <div class="alignleft"><?php previous_posts_link('« Previous') ?></div>
    <div class="alignright"><?php next_posts_link('More »') ?></div>
  </div>
  <?php } ?>
  <?php $wp_query = null; $wp_query = $temp;
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}
add_shortcode("peoplefull", "peoplefullLoop");

// This is the custom shortcode that will show Google Calender
function wt_maps_shortcode( $atts ) {

        $wt_timezone = get_option( 'timezone_string' );

        extract(
            shortcode_atts(
                array(
                     'width'          => '800',
                     'height'         => '600',
                     'account'        => '',
                     'timezone'       => $wt_timezone,
                ), $atts
            )
        );
        $wt_encode_account  = urlencode( $account );
        $wt_encode_timezone = urlencode( $timezone );

        ob_start();
        ?>

    <div class="wtgoogleCal">
      <iframe src="https://www.google.com/calendar/embed?showTitle=1&amp;showCalendars=0&amp;mode=WEEK&amp;height=<?php echo $height; ?>&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=<?php echo $wt_encode_account; ?>&amp;color=%232952A3&amp;ctz=<?php echo $wt_encode_timezone; ?>" style=" border-width:0 " width="<?php echo $width; ?>" height="<?php echo $height; ?>" frameborder="0" scrolling="no"></iframe>
    </div>
    <div class="clear"></div>
    <?php
        $output_string = ob_get_contents();
        ob_end_clean();
        return $output_string;

    }

    add_shortcode( 'wtgooglecal', 'wt_maps_shortcode' );

// This is the custom shortcode that will list the labs on the Lab Page
function labLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "query" => '',
  ), $atts));
  global $wp_query,$paged,$post;
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  if(!empty($query)){
    $query .= $query;
  }
  $wp_query->query($query);
  ob_start();
  ?>
          <?php while (have_posts()) : the_post(); ?>
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php endwhile;?>
  <?php $wp_query = null; $wp_query = $temp;
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}
add_shortcode("lab", "labLoop");

//This is the shortcode loop that will give a grid view with featured image for all labs
function labgridLoop($atts, $content = null) {
  extract(shortcode_atts(array(
    "query" => 'post_type=labs&orderby=menu_order&order=ASC&posts_per_page=-1',
  ), $atts));
  global $wp_query,$paged,$post;
  $temp = $wp_query;
  $wp_query= null;
  $wp_query = new WP_Query();
  if(!empty($query)){
    $query .= $query;
  }
  $wp_query->query($query);
  ob_start();
  ?>    <div id="lab-grid">
          <?php while (have_posts()) : the_post(); ?>
            <div class="grid_3">
              <div id="lab-image">
                <?php if(has_post_thumbnail()) { ?>
                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('lab-thumb'); ?></a>
                <?php } else { ?>
                  <a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/no-lab-thumb.jpg"></a>
                <?php } ?>
                <div class="lab-title"><a href="<?php the_permalink(); ?>" class="lab-title-link"><?php the_title(); ?></a></div>
              </div>
            </div>
           <?php endwhile;?>
        </div>
  <?php $wp_query = null; $wp_query = $temp;
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}
add_shortcode("labgrid", "labgridLoop");

function labtest_shortcode()
{
    //The Query
    query_posts('post_type=labs&orderby=menu_order&order=ASC&posts_per_page=-1');
 
    //The Loop
    if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="grid_3">
        <?php if(has_post_thumbnail()) { ?>
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('lab-thumb'); ?></a>
        <?php } else { ?>
            <a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/no-lab-thumb.jpg"></a>
          <?php } ?>
          <div class="lab-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
      </div>
     <?php
    endwhile; else:
    endif;
 
        //Reset Query
    wp_reset_query();
}
add_shortcode('labtest', 'labtest_shortcode');


?>