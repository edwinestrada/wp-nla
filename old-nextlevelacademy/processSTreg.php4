<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>
              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>On-Line Specialty Registration Results</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
<?

  $maxITplayers = 4;
  $maxGTplayers = 4;
  $maxGKplayers = 6;
  $maxFBplayers = 40;


  $age = chop( $_POST["age"]) ; 
  $firstname = chop( $_POST["firstname"]) ;
  $lastname = chop( $_POST["lastname"]) ; 
  $phone = chop( $_POST["phone"]);                
  $email  = chop( $_POST["email"]);
  $prgdate = chop( $_POST["day"]);
  $stprg = chop( $_POST["program"]);
  $coach = $_POST["coach"];
  $focus = "null";
 
  $firstname = str_replace("\\", "", $firstname);
  $lastname = str_replace("\\", "", $lastname);
  $month = jdmonthname($prgdate, 1);
  $dayname = jddayofweek($prgdate, 1);
  list($month1, $day, $year) = explode("/", jdtogregorian($prgdate));
  $datestring = $dayname." ".$month." ".$day.", ".$year;
  $stdate = "";
  $checkNLAprg = "no";
  $nlaprogram = "";

  switch ( $stprg ) {
    case "U7-1":
      $signupFile = "data/stdata/Rec".$prgdate.".txt";
      $program = "U7-U10 Specialty Training";
      $playerString = $firstname." ".$lastname."^1\n";
  $nlaprogram = chop( $_POST["nlaprogram"]);
  $checkNLAprg = "yes";
      $maxplayers = $maxITplayers;
    break;
    case "U7-2":
      $signupFile = "data/stdata/Rec".$prgdate.".txt";
      $program = "U7-U10 Specialty Training";
      $playerString = $firstname." ".$lastname."^2\n";
  $nlaprogram = chop( $_POST["nlaprogram"]);
  $checkNLAprg = "yes";
      $maxplayers = $maxGTplayers;

    break;
    case "U11-1":
      $signupFile = "data/stdata/Cla".$prgdate.".txt";
      $program = "U11-U14 Specialty Training";
      $focus = chop( $_POST["focus"]);
      $playerString = $firstname." ".$lastname."^1\n";
  $nlaprogram = chop( $_POST["nlaprogram"]);
  $checkNLAprg = "yes";
      $maxplayers = $maxITplayers;

    break;
    case "U11-2":
      $signupFile = "data/stdata/Cla".$prgdate.".txt";
      $program = "U11-U14 Specialty Training";
      $playerString = $firstname." ".$lastname."^2\n";
  $nlaprogram = chop( $_POST["nlaprogram"]);
  $checkNLAprg = "yes";
      $maxplayers = $maxGTplayers;

    break;
    case "GK":
      $signupFile = "data/stdata/GK".$prgdate.".txt";
      $program = "Goalkeeper Training";
      $playerString = $firstname." ".$lastname."\n";
      $maxplayers = $maxGKplayers;
      $coach = $coach."^TBD";

    break;
    case "FB":
      $signupFile = "data/stdata/FB".$prgdate.".txt";
      $program = "Finishing/Ball Striking";
      $playerString = $firstname." ".$lastname;
      $maxplayers = $maxFBplayers;
      $stdate = chop( $_POST["stdate"]);
      $position = chop ($_POST["position"]);
      if ( $position == "GoalKeeper" )
        $playerString = $playerString."^GK\n";
      else {
        $nlaprogram = chop( $_POST["nlaprogram"]);
        $playerString = $playerString."^FP\n";
        $checkNLAprg = "yes";
      }

      $coach = $coach."^TBD";
    break;

  }

  $error = "<font color=#ff0000>Error message: </font><br>";

  $totalplayers = 0;
  $totalGKplayers = 0;
  if ( file_exists($signupFile) ) {
    $fh = fopen($signupFile, "r");
    while ( $playerInfo= fgets($fh, 80) ) {
       switch ( $stprg ) {
         case "U7-1":
         case "U11-1":
           $parms = explode("^",$playerInfo);
           $name = chop($parms[0]);
           if ( chop($parms[1]) == "1" )
             $totalplayers++;

         break;

         case "U7-2":
         case "U11-2":
           $parms = explode("^",$playerInfo);
           $name = chop($parms[0]);
           if ( chop($parms[1]) == "2" )
             $totalplayers++;

         break;

         case "GK":
           $name = chop($playerInfo);
           $totalplayers++;
         break;

         case "FB":
           $parms = explode("^",$playerInfo);
           $name = chop($parms[0]);
           if ( chop($parms[1]) == "GK" )
             $totalGKplayers++;

           $totalplayers++;

         break;

       }
       if ( $name == $firstname." ".$lastname ) 
         $error = $error."This player is already registered for a session.<br>";
    }

    fclose($fh);
  }

  if ( $totalplayers >= $maxplayers ) {
    $error = $error."We are sorry but the last open slot for this session has just been filled.<br>";
  }
  else {
    if ( ($totalGKplayers >= 4) && ($position == "GoalKeeper") ) {
      $error = $error."We are sorry but the last open GoalKeeper slot for this session has just been filled.<br>";
    }
    else {

    if ( $age ==  "Select" ) 
      $error = $error."Age not entered. <br>";

    if ( ($firstname == "") || ($lastname == "" ) ) 
      $error = $error."Player first and last name must be entered. <br>";

    if ( $email == "")
      $error = $error."An e-mail address must be entered. <br>";

    if ( $phone == "")
      $error = $error."A phone number must be entered. <br>";

    if ( $focus == "Select One" )
      $error = $error."A focus area must be entered. <br>";
    
    if ($checkNLAprg == "yes" ) {
      if ( $nlaprogram == "Select One" )
        $error = $error."Specialty training is limited to current NLA program members - you must enter your current NLA program. <br>";
    }

    if ( $stprg == "FB" ) {
      if ( $stdate == "" )
        $error = $error."You must enter the date of the ST Session you registered for.<br>";
      if ( $position == "Select One")
        $error = $error."You must select either Field Player or GoalKeeper.<br>";
    }
  }
  }

  if ( $error != "<font color=#ff0000>Error message: </font><br>") { 
    Print("<p>Required data was not entered.  Please review the following error");
    Print(" message and enter the required data.");
    Print(" <p>");
    Print ($error);
  }
  else {

    $coachNames = explode("^", $coach);
    $coachIndex = 0;
    $cfound[0] = "false";
    $cfound[1] = "false";
    $coachFile = "data/stdata/Coaches.txt";   
    if ( file_exists($coachFile) ) {
      $fh = fopen($coachFile, "r");
      while ( $coachInfo= fgets($fh, 512) ) {
        $parms = explode ("^", $coachInfo, 3);
        $cname = $parms[0];
        if ( ($cname == $coachNames[0]) || ($cname == $coachNames[1]) ) {
          $cemail[$coachIndex] = chop($parms[2]);
          $cfound[$coachIndex] = "true";
          $coachIndex++;
        } 
      }
      fclose($fh);
    }

  $assignedCoaches = $coachNames[0].", ".$coachNames[1];

  $emailto = "jonas@nextlevelacademy.com, damon@nextlevelacademy.com, staff@nextlevelacademy.com";
  $emailtocoaches = "";
  for ($ii = 0; $ii<2; $ii++ ) {
    if ( $cfound[$ii] == "true" ) {
      if ( $emailtocoaches == "" )
        $emailtocoaches = $cemail[$ii];
      else
        $emailtocoaches = $emailtocoaches.", ".$cemail[$ii];
    }
  }

  if ( $stprg == "FB" ) {
    if ( $emailtocoaches == "" )
      $emailtocoaches = $emailtocoaches."Buddha@nextlevelacademy.com, markg@nextlevelacademy.com, scotty@nextlevelacademy.com";
    else
      $emailtocoaches = $emailtocoaches.", Buddha@nextlevelacademy.com, markg@nextlevelacademy.com, scotty@nextlevelacademy.com";
  }

    $subject = "Specialty Training Registration Confirmation";
    $emailstring = "         Next Level Academy Specialty Training Registration\n\n";
    $emailstring = $emailstring."Assigned Coaches: ". $assignedCoaches."\n\n";
    $emailstring = $emailstring."Player Name: ". $firstname." ".$lastname."   Age: ".$age."\n\n";            
    $emailstring = $emailstring."Phone: ".$phone."  Email Address: ".$email."\n\n";
    $emailstring = $emailstring."Session: ".$program."\n";
    $emailstring = $emailstring."Date: ".$datestring."\n";
    if ( $nlaprogram != "" )
      $emailstring = $emailstring."Currently Enrolled in: ".$nlaprogram."\n";
    if ( $stprg == "FB" ) {
      $emailstring = $emailstring."ST Session Registered For: ".$stdate."\n";
      $emailstring = $emailstring."Position: ".$position;
    }
    
   $mailsuccess = mail($emailto, $subject, $emailstring, "From: staff@nextlevelacademy.com");
  
   if ( $emailtocoaches != "" ) {
     $subject = "ST Training - someone registered for your session.";
     $mailsuccess = mail($emailtocoaches, $subject, $emailstring, "From: staff@nextlevelacademy.com");
   }

 // $mailsuccess = mail("jambgm@bellsouth.net", $subject, $emailstring, "From: staff@nextlevelacademy.com");
 // print($emailto);
//  print("<br><br>".$emailstring);

    $subject = "Specialty Training Registration Confirmation";  
    $emailstring = "This e-mail is to confirm the registration of ".$firstname." ".$lastname." for a Specialty Training Session.\n";
    $emailstring = $emailstring."Please confirm the information below.  If it is incorrect please contact us.\n\n";
    $emailstring = $emailstring."Player Name: ". $firstname." ".$lastname."   Age: ".$age."\n\n";            
    $emailstring = $emailstring."Phone: ".$phone."  Email Address: ".$email."\n\n";
    $emailstring = $emailstring."Session: ".$program."\n";
    $emailstring = $emailstring."Date: ".$datestring."\n";
    if ( $nlaprogram != "" )
      $emailstring = $emailstring."Currently Enrolled in: ".$nlaprogram."\n";
    if ( $stprg == "FB" ) {
      $emailstring = $emailstring."ST Session Registered For: ".$stdate."\n";
      $emailstring = $emailstring."Position: ".$position;
    }

   $mailsuccess = mail($email, $subject, $emailstring, "From: staff@nextlevelacademy.com");

// $mailsuccess = 1;
// print("<br><br>".$emailstring);

   if ( $mailsuccess ) {
      $fh = fopen($signupFile, "a+");
      fwrite($fh, $playerString);
      fclose($fh);
      print("Your registration has been completed.  You should receive an e-mail confirming your registration.");
      print("<p> Thank you for using the on-line system to register for a Specialty Training session.");
   }
   else {
     print("There was an error processing your registration.  Please go back and re-attempt the on-line registration.");
     print(" If problems persist please send and e-mail to:  staff@nextlevelacademy.com and describe your problem. ");
     print("<p>We apologize for the inconvenience.");
   }
  }
?>
                 
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>