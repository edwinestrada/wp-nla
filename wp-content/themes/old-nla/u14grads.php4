<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>
<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>

              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>NLA U14 - U15 Graduate Functionals Program</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>NLA U14 - U15 Graduate Functionals Program</FONT></strong></P>

                        <P>Next Level Academy is excited to announce our newest program, Graduate Functionals Program which 
					expands on our U13 Graduate Program to allow players ages U14 - U15 to continue training with us.  
					As is with our U13 Graduate Program, players must have graduated from our Academy program in order 
					to register for our Graduate Functionals Program.  In Graduate Functionals, players continue 
					working on and developing their technical skills and play small sided games at the end of each 
					session incorporating the technical skills worked on during that session. </p>
				   <p>We realize that's it's difficult for players in the U14 - U15 age groups to commit to a year long 
					program due to other commitments with their club teams so to accomodate for that Graduate 
					Functionals is offered in 3 sessions during the year.  The sessions  are summer, winter 1, and 
					winter 2 which should be during times when players have some downtime from club team activities.  
					Each session is 8 weeks long and the cost is $185 per player per session.  In order to ensure that 
					your player and players in this program reap the most benefits from it, we would like for players 
					registering for this program to enroll in all 3 sessions.
				   </p> 
				   <p>Our Graduate Functionals Program is on Friday nights from 6:00 - 7:15 pm and are held at our 
					Morrisville location only (NetSports at 3717 Davis Drive).
				   </p>
				   <p>Players registering for this program must also complete a waiver form and send that in along with 
					payment to:
				   </p>
				   <p>&nbsp; &nbsp; &nbsp; Next Level Academy<br>
					&nbsp; &nbsp; &nbsp; 3717 Davis Drive<br>
					&nbsp; &nbsp; &nbsp; Morrisville, NC  27560<br>
				   </p>
                        <p>Below are the session dates</p>
                     </TD>
                    </TR>

				<TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          
                          <TR>
                            <TD width="25%"><u>Summer</u></TD>
                            <TD width="40%"><u>Winter 1</u></TD>
                            <TD><u>Winter 2</u></TD>
                          </TR>
                          <TR>
                            <TD>July 11</TD>
                            <TD>November 14</TD>
                            <TD>January 30</TD>
                          </TR>
                          <TR>
                            <TD>July 18</TD>
                            <TD>November 21</TD>
                            <TD>February 6</TD>
                          </TR>
                          <TR>
                            <TD>July 25</TD>
                            <TD>November 28 - NO TRAINING</TD>
                            <TD>February 13</TD>
                          </TR>
                          <TR>
                            <TD>Aug 1</TD>
                            <TD>December 5</TD>
                            <TD>February 20</TD>
                          </TR>
                          <TR>
                            <TD>Aug 8</TD>
                            <TD>December 12</TD>
                            <TD>February 27</TD>
                          </TR>
                          <TR>
                            <TD>Aug 15  No Training</TD>
                            <TD>December 19</TD>
                            <TD>March 6</TD>
                          </TR>
                          <TR>
                            <TD>Aug 22</TD>
                            <TD>December 26 - NO TRAINING</TD>
				       <TD>March 13</TD>
                          </TR>
                          <TR>
                            <TD>Aug 29</TD>
                            <TD>January 2, 2015 - NO TRAINING</TD>
				       <TD>March 20</TD>
                          </TR>
                          <TR>
                            <TD>Sept. 5</TD>
                            <TD>Jan 9, 2015 </TD>
				      <TD></TD>
                          </TR>

                          <TR>
                            <TD></TD>
                            <TD>Jan 16, 2015</TD>
				      <TD></TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>Jan 23, 2015</TD>
				      <TD></TD>
                          </TR>
                          <TR>
                          <TR>
                            <TD colspan=3>Please note: if makeup days are needed they will be announced when the closing occurs and will probably 
					occur in April or May. However we reserve the right to hold makeup days when best fit the programs schedule.
                            </TD>
                          </TR>
                          </TR>
                        </TBODY></TABLE>
                      </TD>
                    </TR>

                    <TR>
                      <TD> All players registering for a Graduate Program session will need to 
                        complete a <a href = "docs/NextLevelAcademyMedicalWaiver.doc">
                        waiver form</a> for the program year. Once a new waiver 
                        form has been completed, it's good through Spring. 
                      </td>
                    </tr> 
		    <tr>
		      <td>
<?
    $prgFile = "data/Grads/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<p><strong>Registration for the U14-U15 Graduate Program is available</strong>. &nbsp; &nbsp;");
        print("Space is limited so please register online to reserve your place in the U14-U15 Graduate Program. ");
        print("To register <a href=gradsreg.php4><font style=\"color: #0000ff;\">click here.</font></a> </p>");
      }
      else 
        print("<p align=center>Online registration for the U14-U15 Graduate Program is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the U14-U15 Graduate Program is not open.</p>");
                       
?>
		  </td>
		</tr>


                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>