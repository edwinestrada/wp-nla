<?php
class CPT{
	/**
	 * Class variable that determines position of tab among tabs on the left.
   * Float rather than int to prevent likelyhood of conflicts with other tab positions.
	 * @var float
	 */
	private static $mCPTPosition =29.27;
	/**
	 * Class variable that will be appended to ID's generated for each tab so that each is unique.
	 * @var integer
	 */
  private static $mCPTCounter = 0;
  private static $mCPTBaseID  = 'cpt_';

  public $getFile        = '';
  public $cptTitlePlural = '';
  public $cptTitleSingle = '';
  public $id             = '';

  protected $icon = '/inc/cpt-lib/icons/16x16_iconaut/settings_16.png';
  protected $CPTs = array();
  protected $taxonomies;
  protected $supports;
  protected $rewrite;

  /**
   * When instance of CPT is created, increments counter of all instances created and increments
   * position so that there will be no conflicting ID's or positions.
   * @param string $name Name that appears as a CPT in WP-admin
   */
  public function __construct($titlePlural = "Custom CPTs", $titleSingle = "Custom CPTs", $id){
  	/**
  	 * Set details of where CPT is located
  	 */
		global $pagenow;
		$this->getFile = $pagenow;

    /**
     * Initialize CPT parameters
     */
    self::$mCPTCounter++;
    self::$mCPTPosition += 0.5;
    $this->cptTitlePlural = $titlePlural;
    $this->cptTitleSingle = $titleSingle;

    if(empty($id)){
      $this->id = self::$mCPTBaseID.strval(self::$mCPTCounter);
    }else{
      $this->id = $id;
    }
  }

  /**
   * Sets the support types
   */
  public function setSupport($suppports){
    $this->suppports = $suppports;
  }

  /**
   * Sets the taxonomy types
   */
  public function setTaxonomies($taxonomies){
    $this->taxonomies = $taxonomies;
  }

  /**
   * Sets the rewrite slug
   */
  public function setRewrite($rewrite){
    $this->rewrite = array('slug' => $rewrite);
  }

  /**
   * Sets the icon
   */
  public function setIcon($icon = 'redPin'){
    switch($icon){
      case('parrot'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/appearance_16.png';
        break;
      case('talkBubble'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/comments_16.png';
        break;
      case('clock'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/dashboard_16.png';
        break;
      case('chain'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/link_16.png';
        break;
      case('ipod'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/media_16.png';
        break;
      case('pages'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/pages_16.png';
        break;
      case('redPin'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/posts_16.png';
        break;
      case('equalizer'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/settings_16.png';
        break;
      case('redPin'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/posts_16.png';
        break;
      case('bluePin'):
        $this->icon = '/inc/cpt-lib/icons/16x16_blue/posts.png';
        break;
      case('grayPin'):
        $this->icon = '/inc/cpt-lib/icons/16x16_gray/posts.png';
        break;
      case('wrench'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/tools_16.png';
        break;
      case('people'):
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/users_16.png';
        break;
      case('people2'):
        $this->icon = '/inc/cpt-lib/icons/people.png';
        break;
      case('slide'):
        $this->icon = '/inc/cpt-lib/icons/slides.png';
        break;
      case('youtube'):
        $this->icon = '/inc/cpt-lib/icons/youtube.png';
        break;
      case('gray_media'):
        $this->icon = '/inc/cpt-lib/icons/16x16_gray/media.png';
        break;
      default:
        $this->icon = '/inc/cpt-lib/icons/16x16_iconaut/settings_16.png';
    }
  }

  /**
   * Implements all the functions it takes to create the menu page.
   */
  public function load(){
    // Add action to register the post type, if the post type does not already exist  
    if(!post_type_exists($this->id)){  
      add_action('init', array(&$this,'add_cpt'));
    }  
  }

  /**
   * Adds Tab Page
   */
  public function add_cpt(){
    $labels = array(
      'name'               => _x( $this->cptTitlePlural, 'post type general name' ),
      'singular_name'      => _x( $this->cptTitleSingle, 'post type singular name' ),
      'add_new'            => _x( 'Add New', $this->id ),
      'add_new_item'       => __( 'Add New '.$this->cptTitleSingle ),
      'edit_item'          => __( 'Edit '.$this->cptTitleSingle ),
      'new_item'           => __( 'New '.$this->cptTitleSingle ),
      'all_items'          => __( 'All '.$this->cptTitlePlural ),
      'view_item'          => __( 'View '.$this->cptTitleSingle ),
      'search_items'       => __( 'Search '.$this->cptTitlePlural ),
      'not_found'          => __( 'No '.$this->cptTitlePlural.' found' ),
      'not_found_in_trash' => __( 'No '.$this->cptTitlePlural.' found in the Trash' ), 
      'parent_item_colon'  => '',
      'menu_name'          => $this->cptTitlePlural
    );
    $args = array(
      'labels'        => $labels,
      'description'   => 'Holds our '.$this->cptTitlePlural.' and '.$this->cptTitleSingle.' data',
      'public'        => true,
      'menu_position' => $this->mCPTPosition,                       // The position in the menu order
      'menu_icon'     => get_stylesheet_directory_uri().$this->icon, // The url to the icon to be used for this menu
      'supports'      => (!empty($this->supports)) ? $this->supports : array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
      'has_archive'   => true,
      'taxonomies'    => (!empty($this->taxonomies)) ? $this->taxonomies : array(''),
      'rewrite'       => (!empty($this->rewrite)) ? $this->rewrite : true
    );
    register_post_type( $this->id, $args ); 
  }

}//class CPT