<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>


              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>CENTERS</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>Centers - U7, U8, and U9 Boys and Girls</FONT></strong></P>

                        <P>The Centers are for players in the following age groups: <b>U7, U8,</b> and <b>U9.</b>  
                           Interested older players (U10, U11, U12) should register for the NLA Academies. 
                           <a href="academyinfo.php4"><font style="color: #0000ff;">Click here</font></a> for more information 
                           on the academies.</P>
                      </TD>
                    </TR>

                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          <TR>
                            <td width=50></td>
                            <td><font style="color: #000000;"><b>Spring 2006 Session</b> - begining April 4, 2006.</font></td>
                          </tr>
                          <TR>
                            <td width=50></td>
                            <td>
                              <font style="color: #000000;">Summer Session - stay tuned for more info.</font>
                            </td>
                          </tr>
                          <TR>
                            <td width=50></td>
                            <td>
                              <font style="color: #000000;">Fall Session - stay tuned for more info.</font>
                            </td>
                          </tr>
                          <TR>
                            <td width=50></td>
                            <td>
                              <font style="color: #000000;">
                              Winter Session - in progress</font>
                            </td>
                          </tr>
                        </tbody></table>
                      </td>
                    </tr>

                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          <TR>
                            <TD colspan = 2>
                              <font style="color: #ff0000;"><b>Program:</b></font> 
                              <br>8 training sessions 
                              <br>$120 with T-shirt 
                              <br>$115 if you already have a white, red, or blue NLA training tshirt. 
                            </TD>
                          </TR>
                          <TR><TD height=15></TD></TR>


                          <TR>
                            <TD colspan=2> <font style="color: #ff0000;"><b>What you need at each session:</b></font>
                              &nbsp Players should wear a white, red or blue NLA training t-shirt if you have 
	                      previously attended the NLA Academies or Centers. If they have 
                              not previously been to a Next Level Academies, then you are required to purchase an NLA 
	                      t-shirt with your online registration.   Players are required to wear indoor shoes 
	                      or tennis shoes and shin guards.  All players should bring a water bottle and 
	                      size 4 ball with their name on it.
                            </td>
                          </tr>

                          <TR>
                            <TD colspan=2> <font style="color: #ff0000;"><b>Registration:</b></font>
                              &nbsp After completing the on-line registration, please complete and have
                              notarized the Medical Waiver below (only need to have one Medical Waiver
                              for the year) and make your payment out to Next Level Academy for the 
                              amount for the session and t-shirt if you have not previously purchased one.
                              Please put your player's name, age group, and gender in the memo section.
                              Payment may be dropped off at the Next Level Academy office in the Netsports facility
                              (3717 Davis Drive, Morrisville, NC).  If we have not received 
                              your payment by one week prior to the first session you may loose your spot
                              in the program as we often have waiting lists to get into the program.
                              <p><a href = "docs/NextLevelAcademyMedicalWaiver.doc">
                              <font style="color: #0000ff;">Medical Waiver</font></a> (only need to have
                              completed and notarized once per year. If you attend from Summer 05 to 
                              Spring 06, you will not need another waiver until Summer 06).</p>
                            </td>
                          </tr>

                        
                          <TR><TD height=15></TD></TR>

                         <TR>
                            <TD colspan = 2 align=center><strong>Spring 2006 Session Training Dates</strong></TD>
                          </TR>
                          <TR>
                            <TD width = "50%">Tuesday, April 4th </TD>
                            <TD>4:30 - 5:30pm</TD>
                          </TR>
                          <TR>
                            <TD>Friday, April  7th</TD>
                            <TD>4:30 - 5:30pm</TD>
                          </TR>
                          <TR>
                            <TD>Tuesday, April  11th</TD>
                            <TD>4:30 - 5:30pm</TD>
                          </TR>                           
                          <TR>
                            <TD>Thursday, April  13th</TD>
                            <TD>4:30 - 5:30pm</TD>
                          </TR>
                          <TR>
                            <TD>Tuesday, April  18th</TD>
                            <TD>4:30 - 5:30pm</TD>
                          </TR>                           
                          <TR>
                            <TD>Friday, April  21st</TD>
                            <TD>4:30 - 5:30pm</TD>
                          </TR>
                          <TR>
                            <TD>Tuesday, April  25th</TD>
                            <TD>4:30 - 5:30pm</TD>
                          </TR>                           
                          <TR>
                            <TD>Friday, April  28th</TD>
                            <TD>4:30 - 5:30pm</TD>
                          </TR>
                           <TR>
                            <TD colspan = 2 align=center>All trainings are indoor at the Netsports facility</TD>
                          </TR>
                        </TBODY></TABLE>
                      </TD>
                    </TR>
                    <tr><td height=5 align=center><img src=images/line.jpg width=400></td></tr>
                    <tr><td>


<?
    $prgFile = "data/Centers/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        $fh = fopen("data/Centers/Regseason.txt", "r");
        $season = fgets($fh,25);
        fclose($fh);
        print("<p><strong>Registration is available</strong> only for the <strong>".$season."</strong> season. &nbsp; &nbsp;");
        print("Centers space is limited so please <a href=regshtml.php4?&prgtype=Centers>");
        print("<font style=\"color: #0000ff;\">register online</font></a> to reserve your place in the Next Level Centers.</p>");
      }
      else 
        print("<p align=center>Online registration for the Centers is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the Centers is not open.</p>");
                       
?>
                      </TD>
                    </TR>

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>

