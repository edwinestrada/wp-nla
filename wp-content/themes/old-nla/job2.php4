<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD width=750 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Raleigh Elite General Manager</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                         <P>Next Level Academy is continuing it's 
			support of the Raleigh Elite and is hiring a General Manager for this summer's 
			season.  The position is a great resume builder and is a 9 month position.  If 
			you are interested in this position please send an email to: 
			staff@nextlevelacademy.com.  Also send your resume for our review. We would 
			like to fill this position within the next 2 weeks.
		</p>
                     </TD>
                    </TR>
                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          <TR>
                            <TD align=left><strong>Title of Position:  Raleigh Elite General Manager</strong></TD>
                          </TR>
                          <tr>
                            <td><strong>Primary Responsibilities:</strong>
                              <ul>
                              <li>Commitment to 10 month assignment- 30-40+ hours/week</li>
                              <li>Liaise with team owners and coaches</li>
                              <li>Liaise with team registrar</li>
                              <li>Liaise with volunteer coordinator</li>
                              <li>Liaise with marketing coordinator</li>
                              <li>Liaise with financial manager</li>
                              <li>Reply to Raleigh Elite emails</li>
                              <li>Assist with season ticket creation and tracking</li>
                              <li>Coordinate game day activities</li>
                              <li>Coordinate team away game travel, lodging and food</li>
                              <li>Coordinate visiting team hotel, vans, and hospitality</li>
                              <li>Work with webmaster to update and maintain website</li>
                              <li>Contact and arrange local sponsorships and advertising</li>
                              <li>Forward emails for other NLA staff and ensure emails are replied to</li>
                              <li>Record game day sales and expenses</li>
                              <li>Coordinate t-shirt sales</li>
                              <li>Coordinate and oversee Kick-off Party and any other team functions</li>
                              <li>Communicate weekly to all Raleigh Elite staff upcoming events and status</li>
                              </ul>
                            </td>
                          </tr>
                          <TR><TD height = 10> &nbsp; </TD></TR>
                          <tr>
                            <td><strong>Skills Required for the Position:</strong>
                              <ul>
                              <li>Excellent oral and written communication skills</li>
                              <li>Some basic understanding of soccer</li>
                              <li>Positive attitude </li>
                              <li>Ability to provide consistent and accurate information </li>
                              <li>Ability to multi-task</li>
                              <li>Ability to maintain professionalism at all times</li>
                              <li>Proficient in: Microsoft WORD, Excel, PowerPoint; Internet searching, and basic website understanding</li>
                              </ul>
                            </td>
                          </tr>
                          <TR><TD height = 10> &nbsp; </TD></TR>
                          <tr>
                            <td><strong>Education:</strong>
                              <ul>
                              <li>4 year degree in any field</li>
                              <li>2 year degree in any field</li>
                              <li>High school diploma with 5 years of work experience</li>
                              </ul>
                            </td>
                          </tr>

                        </TBODY></TABLE>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>