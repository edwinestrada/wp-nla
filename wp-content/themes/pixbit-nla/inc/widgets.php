<?php

function unregister_default_SiteOrigin_Panels_widgets() {
  unregister_widget('SiteOrigin_Panels_Widgets_Gallery');
  unregister_widget('SiteOrigin_Panels_Widgets_PostContent');
  unregister_widget('SiteOrigin_Panels_Widgets_Image');
  unregister_widget('SiteOrigin_Panels_Widgets_PostLoop');
  unregister_widget('SiteOrigin_Panels_Widgets_EmbeddedVideo');
  unregister_widget('SiteOrigin_Panels_Widgets_Video');
  unregister_widget('SiteOrigin_Panels_Widget_Animated_Image');
  unregister_widget('SiteOrigin_Panels_Widget_Button');
  unregister_widget('SiteOrigin_Panels_Widget_Call_To_Action');
  unregister_widget('SiteOrigin_Panels_Widget_List');
  unregister_widget('SiteOrigin_Panels_Widget_Price_Box');
  unregister_widget('SiteOrigin_Panels_Widget_Testimonial');
}
add_action('widgets_init', 'unregister_default_SiteOrigin_Panels_widgets', 11);

function my_unregister_widgets() {
  unregister_widget( 'WP_Widget_Pages' );
  unregister_widget( 'WP_Widget_Calendar' );
  unregister_widget( 'WP_Widget_Archives' );
  unregister_widget( 'WP_Widget_Links' );
  unregister_widget( 'WP_Widget_Categories' );
  unregister_widget( 'WP_Widget_Recent_Posts' );
  unregister_widget( 'WP_Widget_Search' );
  unregister_widget( 'WP_Widget_Tag_Cloud' );
  unregister_widget( 'WP_Widget_Meta' );
  unregister_widget( 'WP_Widget_Text' );
  unregister_widget( 'WP_Widget_Recent_Comments' );
  unregister_widget( 'WP_Widget_RSS' );
}
add_action( 'widgets_init', 'my_unregister_widgets' );

/**
 * Register the widgets.
 */
function pixbit_panels_widgets_init(){
  register_widget('Pixbit_Panels_Widgets_Bars');
  register_widget('Pixbit_Panels_Widgets_Accordion');
  register_widget('Pixbit_Panels_Widgets_Image_and_Excerpt_Loop_E');
  register_widget('Pixbit_Panels_Widgets_Subpage_Header_LTR');
  register_widget('Pixbit_Panels_Widgets_Subpage_Header_RTL');
  register_widget('Pixbit_Panels_Widgets_Section_Title_LTR');
  register_widget('Pixbit_Panels_Widgets_Section_Title_RTL');
}
add_action('widgets_init', 'pixbit_panels_widgets_init');



class Pixbit_Panels_Widgets_Bars extends WP_Widget {
  function __construct() {
    parent::__construct(
      'pixbit-panels-bars', // Widget ID
      __( 'Bars', 'so-panels' ), // Widget Title
      array(
        'description' => __( 'Displays a multiple bars.', 'so-panels' ), // Widget Description
      )
    );
  }

  /**
   * @param array $args
   * @param array $instance
   */
  function widget( $args, $instance ) {
    echo $args['before_widget'];

    // Filter the heading
    // $instance['heading'] = apply_filters('widget_title', $instance['heading'], $instance, $this->id_base);
    ?>
      <!-- Accordion Starts -->
      <section id="about" class="dark-texture-4">
            <div class="container page-contents">
                  <div class="row">
                        <div class="span6">
                              <h1><?php echo $instance['heading']; ?></h1>
                              <div class="progress progress-striped active">
                                <div class="bar" style="width: 95%;"><span><?php echo $instance['bar-a']; ?></span></div>
                              </div>
                              <div class="progress progress-striped">
                                <div class="bar" style="width: 85%;"><span><?php echo $instance['bar-b']; ?></span></div>
                              </div>
                              <div class="progress progress-striped">
                                <div class="bar" style="width: 75%;"><span><?php echo $instance['bar-c']; ?></span></div>
                              </div>
                              <div class="progress progress-striped">
                                <div class="bar" style="width: 65%;"><span><?php echo $instance['bar-d']; ?></span></div>
                              </div>
                              <div class="progress progress-striped">
                                <div class="bar" style="width: 55%;"><span><?php echo $instance['bar-e']; ?></span></div>
                              </div>
                        </div><!-- .span6  -->
                  </div><!-- .row -->
            </div><!-- .container .page-contents -->
    </section><!-- #about.dark-texture-4  -->
    <!-- Accordion Ends -->
    <?php
    echo $args['after_widget'];

  }//widget
  function update($new, $old){
    return $new;
  }

  function form( $instance ) {
    $instance = wp_parse_args($instance, array(
      'heading' => ''
      ,'bar-a' => ''
      ,'bar-b' => ''
      ,'bar-c' => ''
      ,'bar-d' => ''
      ,'bar-e' => ''
    ));
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'heading' ) ?>"><?php _e( 'Heading', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'heading' ) ?>" id="<?php echo $this->get_field_id( 'heading' ) ?>" value="<?php echo esc_attr( $instance['heading'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'bar-a' ) ?>"><?php _e( 'bar-a', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'bar-a' ) ?>" id="<?php echo $this->get_field_id( 'bar-a' ) ?>" value="<?php echo esc_attr( $instance['bar-a'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'bar-b' ) ?>"><?php _e( 'bar-b', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'bar-b' ) ?>" id="<?php echo $this->get_field_id( 'bar-b' ) ?>" value="<?php echo esc_attr( $instance['bar-b'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'bar-c' ) ?>"><?php _e( 'bar-c', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'bar-c' ) ?>" id="<?php echo $this->get_field_id( 'bar-c' ) ?>" value="<?php echo esc_attr( $instance['bar-c'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'bar-d' ) ?>"><?php _e( 'bar-d', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'bar-d' ) ?>" id="<?php echo $this->get_field_id( 'bar-d' ) ?>" value="<?php echo esc_attr( $instance['bar-d'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'bar-e' ) ?>"><?php _e( 'bar-e', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'bar-e' ) ?>" id="<?php echo $this->get_field_id( 'bar-e' ) ?>" value="<?php echo esc_attr( $instance['bar-e'] ) ?>">
    </p>
  <?php
  }
}
class Pixbit_Panels_Widgets_Accordion extends WP_Widget {
  function __construct() {
    parent::__construct(
      'pixbit-panels-accordion', // Widget ID
      __( 'Accordion', 'so-panels' ), // Widget Title
      array(
        'description' => __( 'Displays a accordion paragraph boxes.', 'so-panels' ), // Widget Description
      )
    );
  }

  /**
   * @param array $args
   * @param array $instance
   */
  function widget( $args, $instance ) {
    echo $args['before_widget'];

    // Filter the heading
    // $instance['heading'] = apply_filters('widget_title', $instance['heading'], $instance, $this->id_base);
    ?>
      <!-- Accordion Starts -->
      <section id="about" class="dark-texture-4">
            <div class="container page-contents">
                  <div class="row">
                        <div class="span6">
                              <h1><?php echo $instance['heading']; ?></h1>
                              <div class="accordion" id="accordion2">

                                    <div class="accordion-group">
                                          <div class="accordion-heading">
                                                <a id="collapseOne" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"><?php echo $instance['subheading-a']; ?></a>
                                          </div><!-- .accordion-heading  -->
                                          <div class="accordion-body collapse in">
                                                <div class="accordion-inner"><?php echo $instance['text-a']; ?></div><!-- .accordion-inner  -->
                                          </div><!-- .accordion-body .collapse .in  -->
                                    </div><!-- .accordion-group  -->

                                    <div class="accordion-group">
                                          <div class="accordion-heading">
                                                <a id="collapseTwo" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"><?php echo $instance['subheading-b']; ?></a>
                                          </div><!-- .accordion-heading  -->
                                          <div class="accordion-body collapse">
                                                <div class="accordion-inner"><?php echo $instance['text-b']; ?></div><!-- .accordion-inner  -->
                                          </div><!-- .accordion-body .collapse .in  -->
                                    </div><!-- .accordion-group  -->

                                    <div class="accordion-group">
                                          <div class="accordion-heading">
                                                <a id="collapseThree" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree"><?php echo $instance['subheading-c']; ?></a>
                                          </div><!-- .accordion-heading  -->
                                          <div class="accordion-body collapse">
                                                <div class="accordion-inner"><?php echo $instance['text-c']; ?></div><!-- .accordion-inner  -->
                                          </div><!-- .accordion-body .collapse  -->
                                    </div><!-- .accordion-group  -->

                                    <div class="accordion-group">
                                          <div class="accordion-heading">
                                                <a id="collapseFour" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour"><?php echo $instance['subheading-d']; ?></a>
                                          </div><!-- .accordion-heading  -->
                                          <div class="accordion-body collapse">
                                                <div class="accordion-inner"><?php echo $instance['text-d']; ?></div><!-- .accordion-inner  -->
                                          </div><!-- .accordion-body .collapse  -->
                                    </div><!-- .accordion-group  -->

                              </div><!-- .accordion  -->
                        </div><!-- .span6  -->
                  </div><!-- .row -->
            </div><!-- .container .page-contents -->
    </section><!-- #about.dark-texture-4  -->
    <!-- Accordion Ends -->
    <?php
    echo $args['after_widget'];

  }//widget
  function update($new, $old){
    return $new;
  }

  function form( $instance ) {
    $instance = wp_parse_args($instance, array(
      'heading' => ''
      ,'subheading-a' => ''
      ,'subheading-b' => ''
      ,'subheading-c' => ''
      ,'subheading-d' => ''
      ,'text-a' => ''
      ,'text-b' => ''
      ,'text-c' => ''
      ,'text-d' => ''
    ));
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'heading' ) ?>"><?php _e( 'Heading', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'heading' ) ?>" id="<?php echo $this->get_field_id( 'heading' ) ?>" value="<?php echo esc_attr( $instance['heading'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'subheading-a' ) ?>"><?php _e( 'subheading-a', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'subheading-a' ) ?>" id="<?php echo $this->get_field_id( 'subheading-a' ) ?>" value="<?php echo esc_attr( $instance['subheading-a'] ) ?>">
      <label for="<?php echo $this->get_field_id( 'text-a' ) ?>"><?php _e( 'text-a', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'text-a' ) ?>" id="<?php echo $this->get_field_id( 'text-a' ) ?>" value="<?php echo esc_attr( $instance['text-a'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'subheading-b' ) ?>"><?php _e( 'subheading-b', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'subheading-b' ) ?>" id="<?php echo $this->get_field_id( 'subheading-b' ) ?>" value="<?php echo esc_attr( $instance['subheading-b'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'text-b' ) ?>"><?php _e( 'text-b', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'text-b' ) ?>" id="<?php echo $this->get_field_id( 'text-b' ) ?>" value="<?php echo esc_attr( $instance['text-b'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'subheading-c' ) ?>"><?php _e( 'subheading-c', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'subheading-c' ) ?>" id="<?php echo $this->get_field_id( 'subheading-c' ) ?>" value="<?php echo esc_attr( $instance['subheading-c'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'text-c' ) ?>"><?php _e( 'text-c', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'text-c' ) ?>" id="<?php echo $this->get_field_id( 'text-c' ) ?>" value="<?php echo esc_attr( $instance['text-c'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'subheading-d' ) ?>"><?php _e( 'subheading-d', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'subheading-d' ) ?>" id="<?php echo $this->get_field_id( 'subheading-d' ) ?>" value="<?php echo esc_attr( $instance['subheading-d'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'text-d' ) ?>"><?php _e( 'text-d', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'text-d' ) ?>" id="<?php echo $this->get_field_id( 'text-d' ) ?>" value="<?php echo esc_attr( $instance['text-d'] ) ?>">
    </p>
  <?php
  }
}
class Pixbit_Panels_Widgets_Image_and_Excerpt_Loop extends WP_Widget {
  function update($new, $old){
    return $new;
  }

  function form( $instance ) {
    $instance = wp_parse_args($instance, array(
      'title' => '',
      // Query args
      'post_type' => 'post',
      'posts_per_page' => '',

      'order' => 'DESC',
      'orderby' => 'date'
    ));

    // Get all the loop template files
    $post_types = get_post_types(array('public' => true));
    $post_types = array_values($post_types);
    $post_types = array_diff($post_types, array('attachment', 'revision', 'nav_menu_item'));

    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ) ?>"><?php _e( 'Title', 'so-panels' ) ?></label>
      <input type="text" class="widefat" name="<?php echo $this->get_field_name( 'title' ) ?>" id="<?php echo $this->get_field_id( 'title' ) ?>" value="<?php echo esc_attr( $instance['title'] ) ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id('post_type') ?>"><?php _e('Post Type', 'so-panels') ?></label>
      <select id="<?php echo $this->get_field_id( 'post_type' ) ?>" name="<?php echo $this->get_field_name( 'post_type' ) ?>" value="<?php echo esc_attr($instance['post_type']) ?>">
        <?php foreach($post_types as $type) : ?>
          <option value="<?php echo esc_attr($type) ?>" <?php selected($instance['post_type'], $type) ?>><?php echo esc_html($type) ?></option>
        <?php endforeach; ?>
      </select>
    </p>
    <p>
      <label <?php echo $this->get_field_id('orderby') ?>><?php _e('Order By', 'so-panels') ?></label>
      <select id="<?php echo $this->get_field_id( 'orderby' ) ?>" name="<?php echo $this->get_field_name( 'orderby' ) ?>" value="<?php echo esc_attr($instance['orderby']) ?>">
        <option value="none" <?php selected($instance['orderby'], 'none') ?>><?php esc_html_e('None', 'so-panels') ?></option>
        <option value="ID" <?php selected($instance['orderby'], 'ID') ?>><?php esc_html_e('Post ID', 'so-panels') ?></option>
        <option value="author" <?php selected($instance['orderby'], 'author') ?>><?php esc_html_e('Author', 'so-panels') ?></option>
        <option value="name" <?php selected($instance['orderby'], 'name') ?>><?php esc_html_e('Name', 'so-panels') ?></option>
        <option value="name" <?php selected($instance['orderby'], 'name') ?>><?php esc_html_e('Name', 'so-panels') ?></option>
        <option value="date" <?php selected($instance['orderby'], 'date') ?>><?php esc_html_e('Date', 'so-panels') ?></option>
        <option value="modified" <?php selected($instance['orderby'], 'modified') ?>><?php esc_html_e('Modified', 'so-panels') ?></option>
        <option value="parent" <?php selected($instance['orderby'], 'parent') ?>><?php esc_html_e('Parent', 'so-panels') ?></option>
        <option value="rand" <?php selected($instance['orderby'], 'rand') ?>><?php esc_html_e('Random', 'so-panels') ?></option>
        <option value="comment_count" <?php selected($instance['orderby'], 'comment_count') ?>><?php esc_html_e('Comment Count', 'so-panels') ?></option>
        <option value="menu_order" <?php selected($instance['orderby'], 'menu_order') ?>><?php esc_html_e('Menu Order', 'so-panels') ?></option>
        <option value="menu_order" <?php selected($instance['orderby'], 'menu_order') ?>><?php esc_html_e('Menu Order', 'so-panels') ?></option>
      </select>
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('order') ?>"><?php _e('Order', 'so-panels') ?></label>
      <select id="<?php echo $this->get_field_id( 'order' ) ?>" name="<?php echo $this->get_field_name( 'order' ) ?>" value="<?php echo esc_attr($instance['order']) ?>">
        <option value="DESC" <?php selected($instance['order'], 'DESC') ?>><?php esc_html_e('Descending', 'so-panels') ?></option>
        <option value="ASC" <?php selected($instance['order'], 'ASC') ?>><?php esc_html_e('Ascending', 'so-panels') ?></option>
      </select>
    </p>

  <?php
  }
}
class Pixbit_Panels_Widgets_Image_and_Excerpt_Loop_E extends Pixbit_Panels_Widgets_Image_and_Excerpt_Loop {
  function __construct() {
    parent::__construct(
      'pixbit-panels-image-and-excerpt-loop-e', // Widget ID
      __( 'Image + Excerpt (V)', 'so-panels' ), // Widget Title
      array(
        'description' => __( 'Displays a simple image with Article Title and Paragraph.', 'so-panels' ), // Widget Description
      )
    );
  }

  /**
   * @param array $args
   * @param array $instance
   */
  function widget( $args, $instance ) {
    $query_args = $instance;
    unset($query_args['title']);

    // Create the query
    echo $args['before_widget'];

    $loop = new WP_Query(
                array(
                  'post_type'      => $instance['post_type'],
                  'posts_per_page' => '-1',
                  'orderby'        => $instance["orderby"],
                  'order'          => $instance["order"]
                ));
    $i = 0;
    ?>

    <section id="cataloge" class="image-and-excerpt-loop-e black-bg">
    <div class="container page-contents">
      <div class="row">
        <div class="span12">
          <!-- Cataloge Carousel Starts -->
          <div id="myCarousel2" class="carousel slide">
            <div class="carousel-inner">
    <?php

    // Filter the title
    $instance['title'] = apply_filters('widget_title', $instance['title'], $instance, $this->id_base);
    if ( !empty( $instance['title'] ) ) {
      echo $args['before_title'] . do_shortcode('[subheading title="'.$instance['title'].'"]') . $args['after_title'];
    }

    while ($loop->have_posts()) :
        $loop->the_post();
        /* Post */
        $loop_post['postID'][$i]        = $post->ID;
        $loop_post['title'][$i]         = get_the_title($post->ID);
        $loop_post['link'][$i]          = get_permalink($post->ID);
        $loop_post['modified_time'][$i] = get_the_modified_time('F j, Y');
        $loop_post['excerpt'][$i]       = get_the_excerpt();
        /***********************************************************************************************************************/
        $i++;
        ?>
                <!-- Item <?php echo $i; ?> Starts -->
                <div class="item <?php if($i == 1) echo 'active'; ?>">
                  <div class="row">
                    <div class="featurette">
                      <div class="span5 offset1">
                        <?php the_post_thumbnail(); ?>
                        <div class="title"><?php echo get_the_title($post->ID); ?></div>
                      </div>
                      <div class="span5">
                        <h2 class="featurette-heading">Vestibulum<br><span class="muted">Mauris</span></h2>
                        <p class="lead"><?php echo get_the_excerpt(); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Item <?php echo $i; ?> Ends -->
    <?php endwhile; ?>
              </div><!-- .carousel-inner -->
              <a class="left carousel-control" href="#myCarousel2" data-slide="prev"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/slider/icons/back-2.png"></a> <a class="right carousel-control" href="#myCarousel2" data-slide="next"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/slider/icons/forward-2.png"></a>
            </div><!-- #myCarousel2.carousel.slide -->
            <!-- Cataloge Carousel Ends -->
        </div><!-- .span12  -->
      </div><!-- .row  -->
    </div><!-- .container .page-contents -->
    </section><!-- .image-and-excerpt-loop-c .black-bg -->
    <?php
    echo $args['after_widget'];

    // Reset everything
    wp_reset_query();
  }//widget
}
class Pixbit_Panels_Widgets_Section_Title extends WP_Widget {
  function update($new, $old){
    $new = wp_parse_args($new, array(
      'title' => '',
      'subtitle' => '',
    ));
    return $new;
  }

  function form( $instance ) {
    $instance = wp_parse_args($instance, array(
      'title' => '',
      'subtitle' => '',
    ));

    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ) ?>"><?php _e( 'Section Title', 'so-panels' ) ?></label>
      <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ) ?>" name="<?php echo $this->get_field_name( 'title' ) ?>" value="<?php echo esc_attr($instance['title']) ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'subtitle' ) ?>"><?php _e( 'Section Subtitle', 'so-panels' ) ?></label>
      <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'subtitle' ) ?>" name="<?php echo $this->get_field_name( 'subtitle' ) ?>" value="<?php echo esc_attr($instance['subtitle']) ?>" />
    </p>
  <?php
  }
}
class Pixbit_Panels_Widgets_Subpage_Header_LTR extends Pixbit_Panels_Widgets_Section_Title {
  function __construct() {
    parent::__construct(
      'pixbit-panels-subpage-header-ltr',
      __( 'Subpage Header (LTR)', 'so-panels' ),
      array(
        'description' => __( 'Displays a section title and subtitle.', 'so-panels' ),
      )
    );
  }

  /**
   * @param array $args
   * @param array $instance
   */
  function widget( $args, $instance ) {
    echo $args['before_widget'];
    if(!empty($instance['title'])): ?>
      <section class="header dark-texture-4 subpage-header">
        <?php echo do_shortcode('[section_header title="'.$instance['title'].'" subtitle="'.$instance['subtitle'].'" direction="ltr"]'); ?>
      </section>
    <?php endif;
    echo $args['after_widget'];
  }
}
class Pixbit_Panels_Widgets_Subpage_Header_RTL extends Pixbit_Panels_Widgets_Section_Title {
  function __construct() {
    parent::__construct(
      'pixbit-panels-subpage-header-rtl',
      __( 'Subpage Header (RTL)', 'so-panels' ),
      array(
        'description' => __( 'Displays a section title and subtitle.', 'so-panels' ),
      )
    );
  }

  /**
   * @param array $args
   * @param array $instance
   */
  function widget( $args, $instance ) {
    echo $args['before_widget'];
    if(!empty($instance['title'])): ?>
      <section class="header dark-texture-4 subpage-header">
        <?php echo do_shortcode('[section_header title="'.$instance['title'].'" subtitle="'.$instance['subtitle'].'" direction="rtl"]'); ?>
      </section>
    <?php endif;
    echo $args['after_widget'];
  }
}
class Pixbit_Panels_Widgets_Section_Title_LTR extends Pixbit_Panels_Widgets_Section_Title {
  function __construct() {
    parent::__construct(
      'pixbit-panels-section-title-ltr',
      __( 'Section Header (LTR)', 'so-panels' ),
      array(
        'description' => __( 'Displays a section title and subtitle.', 'so-panels' ),
      )
    );
  }

  /**
   * @param array $args
   * @param array $instance
   */
  function widget( $args, $instance ) {
    echo $args['before_widget'];
    if(!empty($instance['title'])): ?>
      <section class="header dark-texture-4">
        <?php echo do_shortcode('[section_header title="'.$instance['title'].'" subtitle="'.$instance['subtitle'].'" direction="ltr"]'); ?>
      </section>
    <?php endif;
    echo $args['after_widget'];
  }
}
class Pixbit_Panels_Widgets_Section_Title_RTL extends Pixbit_Panels_Widgets_Section_Title {
  function __construct() {
    parent::__construct(
      'pixbit-panels-section-title-rtl',
      __( 'Section Header (RTL)', 'so-panels' ),
      array(
        'description' => __( 'Displays a section title and subtitle.', 'so-panels' ),
      )
    );
  }

  /**
   * @param array $args
   * @param array $instance
   */
  function widget( $args, $instance ) {
    echo $args['before_widget'];
    if(!empty($instance['title'])): ?>
      <section class="header dark-texture-4">
        <?php echo do_shortcode('[section_header title="'.$instance['title'].'" subtitle="'.$instance['subtitle'].'" direction="rtl"]'); ?>
      </section>
    <?php endif;
    echo $args['after_widget'];
  }
}