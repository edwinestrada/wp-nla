<?php
/**
 * Scripts and stylesheets
 */
// Register style sheet.
add_action( 'wp_enqueue_scripts', 'register_theme_scripts' );

/**
 * Register style sheet.
 */
function register_theme_scripts() {
    /**
     * wp_register_style Use the wp_enqueue_scripts action to call this function.
     * @param (string) (required) Name of the stylesheet (which should be unique as it is used to identify the script in the whole system).
     * @param (string) (required) URL to the stylesheet. Example: 'http://example.com/css/mystyle.css'. You should never hardcode URLs to local styles, use Function Reference/plugins_url (for Plugins) and Function Reference/get_template_directory_uri (for Themes) to get a proper URL. Remote assets can be specified with a protocol-agnostic URL, i.e. '//otherdomain.com/css/theirstyle.css'.
     * @param (array) (optional) Array of handles of any stylesheets that this stylesheet depends on. Dependent stylesheets will be loaded before this stylesheet.
     * @param (string|boolean) (optional) String specifying the stylesheet version number, if it has one. This parameter is used to ensure that the correct version is sent to the client regardless of caching, and so should be included if a version number is available and makes sense for the stylesheet. The version is appended to the stylesheet URL as a query string, such as ?ver=3.5.1. By default, or if false, the WordPress version string is used. If null nothing is appended to the URL.
     * @param (string) (optional) String specifying the media for which this stylesheet has been defined. Examples: 'all', 'screen', 'handheld', 'print'. See this list for the full range of valid CSS-media-types.
     */

    //////////////////
    // Google Fonts //
    //////////////////
    wp_enqueue_style('googlefonts-Oswald', 'http://fonts.googleapis.com/css?family=Oswald:400,300,700');

    //////////////////////
    // Bootstrap Styles //
    //////////////////////
    wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . "/assets/css/bootstrap/bootstrap.min.css");
    wp_enqueue_style('bootstrap-responsive-min', get_stylesheet_directory_uri() . "/assets/css/bootstrap/bootstrap-responsive.min.css");
    wp_enqueue_style('bootstrap-lightbox', get_stylesheet_directory_uri() . "/assets/css/bootstrap/bootstrap-lightbox.min.css");
    wp_enqueue_style('isotope-style', get_stylesheet_directory_uri() . "/assets/css/portfolio/isotope-style.css");
    wp_enqueue_style('supersized', get_stylesheet_directory_uri() . "/assets/css/supersized/supersized.css");
    wp_enqueue_style('supersized-shutter', get_stylesheet_directory_uri() . "/assets/css/supersized/supersized.shutter.css");
    wp_enqueue_style('social-icons', get_stylesheet_directory_uri() . "/assets/css/social-icons-hover/social-icons-style.css");
    // wp_enqueue_style('livetweets', get_stylesheet_directory_uri() . "/assets/css/twitter/livetweets.css");

    //////////////////
    // Custom Style //
    //////////////////
    wp_enqueue_style('nla-style', get_stylesheet_directory_uri() . '/style.css', false, null);

    ////////////
    // JQuery //
    ////////////
    // Deregister the included library
    wp_deregister_script( 'jquery' );
    // Register the library again from Google's CDN
    wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), null, false );
    wp_enqueue_script('jquery');


    ///////////////////////
    // Twitter Bootstrap //
    ///////////////////////
    // AJAX Contact Form
    wp_register_script('bootstrap-contact-form', ''.get_stylesheet_directory_uri().'/assets/js/contact/contact-form.js', false, null, 'all');
    wp_enqueue_script('bootstrap-contact-form');
    // Transitions
    wp_register_script('bootstrap-transition', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-transition.js', false, null, 'all');
    wp_enqueue_script('bootstrap-transition');

    // Alerts
    // http://getbootstrap.com/2.3.2/components.html#alerts
    wp_register_script('bootstrap-alert', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-alert.js', false, null, 'all');
    wp_enqueue_script('bootstrap-alert');

    // Modal
    wp_register_script('bootstrap-modal', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-modal.js', false, null, 'all');
    wp_enqueue_script('bootstrap-modal');

    // Dropdowns
    // http://getbootstrap.com/2.3.2/components.html#dropdowns
    wp_register_script('bootstrap-dropdown', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-dropdown.js', false, null, 'all');
    wp_enqueue_script('bootstrap-dropdown');

    // Scrollspy
    // http://jsfiddle.net/mCxqY
    wp_register_script('bootstrap-scrollspy', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-scrollspy.js', false, null, 'all');
    wp_enqueue_script('bootstrap-scrollspy');

    // Tab
    // http://getbootstrap.com/2.3.2/components.html#navs
    wp_register_script('bootstrap-tab', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-tab.js', false, null, 'all');
    wp_enqueue_script('bootstrap-tab');

    // Tooltip
    wp_register_script('bootstrap-tooltip', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-tooltip.js', false, null, 'all');
    wp_enqueue_script('bootstrap-tooltip');

    // Popover
    wp_register_script('bootstrap-popover', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-popover.js', false, null, 'all');
    wp_enqueue_script('bootstrap-popover');

    // Button
    // http://getbootstrap.com/2.3.2/components.html#buttonGroups
    wp_register_script('bootstrap-button', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-button.js', false, null, 'all');
    wp_enqueue_script('bootstrap-button');

    // Collapse
    // http://www.w3resource.com/twitter-bootstrap/collapse.php
    // http://www.w3resource.com/twitter-bootstrap/example-collapsible-simplet.html
    wp_register_script('bootstrap-collapse', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-collapse.js', false, null, 'all');
    wp_enqueue_script('bootstrap-collapse');

    // Carousel
    wp_register_script('bootstrap-carousel', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-carousel.js', false, null, 'all');
    wp_enqueue_script('bootstrap-carousel');

    // Typeahead
    // http://www.w3resource.com/twitter-bootstrap/typehead.php
    // http://www.w3resource.com/twitter-bootstrap/example-typehead.html
    wp_register_script('bootstrap-typeahead', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-typeahead.js', false, null, 'all');
    wp_enqueue_script('bootstrap-typeahead');

    // Lightbox
    // http://jbutz.github.io/bootstrap-lightbox/#demo
    wp_register_script('bootstrap-lightbox', ''.get_stylesheet_directory_uri().'/assets/js/bootstrap/bootstrap-lightbox.min.js', false, null, 'all');
    wp_enqueue_script('bootstrap-lightbox');

    //////////////
    // Parallax //
    //////////////
    // wp_register_script('parallax', ''.get_stylesheet_directory_uri().'/assets/js/parallax/jquery.parallax-1.1.3.js', false, null, 'all');
    // wp_enqueue_script('parallax');
    // wp_register_script('parallax-ini', ''.get_stylesheet_directory_uri().'/assets/js/parallax/parallax-ini.js', false, null, 'all');
    // wp_enqueue_script('parallax-ini');


    ////////////////////
    // Retina Support //
    ////////////////////
    wp_register_script('retina', ''.get_stylesheet_directory_uri().'/assets/js/retina/retina.js', false, null, 'all');
    wp_enqueue_script('retina');

    ///////////////
    // Portfolio //
    ///////////////
    // wp_register_script('isotope', ''.get_stylesheet_directory_uri().'/assets/js/isotope/jquery.isotope.min.js', false, null, 'all');
    // wp_enqueue_script('isotope');
    // wp_register_script('isotope-filtering', ''.get_stylesheet_directory_uri().'/assets/js/isotope/Isotope-Filtering.js', false, null, 'all');
    // wp_enqueue_script('isotope-filtering');
    // assets/js/isotope/jquery.js

    //////////////////////
    // Custom Scrollbar //
    //////////////////////
    wp_register_script('scrollbar', ''.get_stylesheet_directory_uri().'/assets/js/nicescroll/jquery.nicescroll.min.js', false, null, 'all');
    wp_enqueue_script('scrollbar');

    ///////////////////////
    // Supersized Slider //
    ///////////////////////
    if(is_front_page()){
        wp_register_script('supersized-jquery-easing', ''.get_stylesheet_directory_uri().'/assets/js/supersized/jquery.easing.min.js', false, null, 'all');
        wp_enqueue_script('supersized-jquery-easing');


        wp_register_script('supersized', ''.get_stylesheet_directory_uri().'/assets/js/supersized/supersized.3.2.7.js', false, null, 'all');
        wp_enqueue_script('supersized');


        wp_register_script('supersized-shutter', ''.get_stylesheet_directory_uri().'/assets/js/supersized/supersized.shutter.min.js', false, null, 'all');
        wp_enqueue_script('supersized-shutter');
    }

    ///////////////
    // Scroll To //
    ///////////////
    // wp_register_script('jquery-scrollTo', ''.get_stylesheet_directory_uri().'/assets/js/nav/jquery.scrollTo.js', false, null, 'all');
    // wp_enqueue_script('jquery-scrollTo');

    // wp_register_script('jquery-nav', ''.get_stylesheet_directory_uri().'/assets/js/nav/jquery.nav.js', false, null, 'all');
    // wp_enqueue_script('jquery-nav');

    ////////////
    // Custom //
    ////////////
    wp_register_script('custom-js', ''.get_stylesheet_directory_uri().'/assets/js/custom.js', false, null, 'all');
    wp_enqueue_script('custom-js');

    // if(is_front_page()){
    //     wp_register_script('hide-logo-js', ''.get_stylesheet_directory_uri().'/assets/js/hide-logo.js', false, null, 'all');
    //     wp_enqueue_script('hide-logo-js');
    // }else{
    //     wp_register_script('hide-logo-not-front-page-js', ''.get_stylesheet_directory_uri().'/assets/js/hide-logo-not-front-page.js', false, null, 'all');
    //     wp_enqueue_script('hide-logo-not-front-page-js');
    // }

    /////////////
    // Twitter //
    /////////////
    wp_register_script('twitter', ''.get_stylesheet_directory_uri().'/assets/js/twitter/twitlive-min.js', false, null, 'all');
    wp_enqueue_script('twitter');

}
