<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>


  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Accept Decline</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
<?
   $ageList = array (
          'U7',
          'U8',
          'U9',
          'U10',
          'U11' );

  $ageListItems = count ( $ageList);

  $agegroup =chop( $_POST["agegroup"]) ; 
  $gender =chop( $_POST["gender"]) ; 
  $firstname =chop( $_POST["firstname"]) ; 
  $lastname =chop( $_POST["lastname"]) ; 
  $playernumber =chop( $_POST["playernumber"]) ; 
  $email  =chop( $_POST["email"]);
  $source =chop( $_POST["source"]);
  $decision =chop( $_POST["decision"]);

  $firstname = str_replace("\\", "", $firstname);
  $lastname = str_replace("\\", "", $lastname);

  $error = "<font color=#ff0000>Error message: </font><br>";

   $index = 0;
   while ( $index <  $ageListItems )
   {
      if ( $agegroup == $ageList[$index] ) {      // item found
        $ageYear = $yearList[$index];
        $index =  $ageListItems;
      }
       $index++;
    }  // while $index

  if ( $playernumber == "") 
    $error = $error."A player's tryout number must be entered.<br>";

  if ( ($firstname == "") || ($lastname == "")) 
    $error = $error."Player's first and last name must be entered.<br>";

  if ( $index ==  $ageListItems ) 
    $error = $error."Age group not entered.<br>";

  if ( $gender ==  "Select One" ) 
    $error = $error."Gender must be entered.<br>";

  if ( $decision == "") 
    $error = $error."A decision must be entered.<br>";


  if ( $error != "<font color=#ff0000>Error message: </font><br>") {  
    print("The data entered was not correct.<br>".$error);
    print("<p>Please go back and make the appropriate corrections.  Thank you.</p>");
  }
  else {
    $subject = $source." Player Decision";
    $emailstring = $emailstring."Player Name: ". $firstname." ".$lastname."   Gender: ".$gender;
    $emailstring = $emailstring."\nAge Group: ".$agegroup."  Player Number: ".$playernumber."\n\n";
    $emailstring = $emailstring."Email Address: ".$email."\n\n";
    $emailstring = $emailstring."Decision:  ".$decision;   
    $mailsuccess = mail("staff@nextlevelacademy.com", $subject, $emailstring, "From: staff@nextlevelacademy.com");
 
    if ( $email != "" ) {
      $subject = "Next Level Academy Decision Confirmation";  
      if ( $source == "Team" ) {
        $emailstring = "This e-mail is to confirm receipt of the decision for  ".$firstname." for the Next Level Academy Team Pool.\n";
      }
      else {
        $emailstring = "This e-mail is to confirm receipt of the decision for ".$firstname." for the Next Level Academies.\n";
      }
      $emailstring = $emailstring."\n\nInformation entered: Player Name: ".$firstname." ".$lastname."\n";
      $emailstring = $emailstring."\nAge Group: ".$agegroup.",   Player Number: ".$playernumber."\n";
      $emailstring = $emailstring."Decision:  ".$decision;   
   
      $mailsuccess = mail($email, $subject, $emailstring, "From: staff@nextlevelacademy.com");
    }

// $mailsuccess = 1;

    if ( $mailsuccess == TRUE ) {
     print("Your decision has been processed. You should receive an e-mail confirmation of your decision. Thank you.");
    }
    else{
     print("There was an error processing your decision.  Please go back and re-attempt.");
     print(" If problems persist please send and e-mail to:  staff@nextlevelacademy.com and describe your problem. ");
     print("<p>We apologize for the inconvenience.");
    }
  }

?>
                 
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>


<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>

