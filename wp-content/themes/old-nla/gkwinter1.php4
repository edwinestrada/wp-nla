<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>


              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Goalkeeper Program</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>Younger Goalkeeper Program - U9, U10, and U11 Boys and Girls
                        (NLA Academy Players ONLY)</FONT></strong></P>
                      </TD>
                    </TR>
                    <TR>
                      <TD valign="top">
                        <P>The younger Goalkeeper Program is not offered in the winter.</p>
                      </TD>
                    </TR>

		    <tr><td>
<?
/*
    $prgFile = "data/Goalies/YoungRegactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<p><strong>Registration for the younger Goalkeepers Program is available</strong>. &nbsp; &nbsp;");
        print("<a href=gkregshtml.php4?&prgtype=Young><font style=\"color: #0000ff;\">Register online</font></a> to reserve your place in the younger Goalkeeper Program. ");
      }
      else 
        print("<p align=center>Online registration for the younger Goalkeeper Program is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the younger Goalkeeper Program is not open.</p>");
*/                       
?>
		</td></tr>
              	    <TR><TD height=20></TD></TR>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>Older Goalkeeper Program  - U12, U13, and U14 Boys and Girls
                        (open to the public)</FONT></strong></P>
                      </TD>
                    </TR>
                    <TR>
                      <TD valign="top">
                        <P>The older Goalkeeper Program is for players in the following age groups: <b>U12, U13,</b> and <b>U14</b>
                        and is open to the public.
                        The Winter 1 session is a 6 week session and the cost is $120 if your player is currently 
                        enrolled in NLA's Graduate Program.  If your player is not enrolled in our Graduate Program, 
                        the cost is $150 per player.  The sessions are on Friday nights from 6-7pm. </p>
                      </TD>
                    </TR>

		    <tr>
		      <td>
<?
    $prgFile = "data/Goalies/OldRegactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<p><strong>Registration for the older Goalkeeper Program is available</strong>. &nbsp; &nbsp;");
        print("<a href=gkregshtml.php4?&prgtype=Old><font style=\"color: #0000ff;\">Register online</font></a> to reserve your place in the older Goalkeeper Program.</p> ");

      }
      else 
        print("<p align=center>Online registration for the older Goalkeeper Program is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the older Goalkeeper Program is not open.</p>");
                       
?>
		  </td>
		</tr>


              	    <TR><TD height=20></TD></TR>
                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          <tr>
                            <td>This session is offered on Friday nights and is only for  
                            U12 - U14 age groups. Training is from 6-7pm.
                            </td>
                          </tr>
                          <TR>
                            <td align=center><font style="color: #000000;"><b>Dates for Winter 1 2010 </b></font></td>
                          </tr>
                        </tbody></table>
                      </td>
                    </tr>

                    <TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          
                          <TR>
                            <TD align="right">Older &nbsp; &nbsp; &nbsp; </TD>
                            <TD>Nov. 5th</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>Nov. 12th</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>Nov. 19th</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>Dec. 3rd</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>Dec. 10th</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>Dec. 17th</TD>
                          </TR>   
                      
                          <tr><td height=7>&nbsp; </td></tr>
<!--
                           <TR>
                            <TD colspan = 5 align=center>* <i>There will be a brief parent 
			meeting after the first training session with the NLA coaches</i></TD>
                          </TR>
-->
                        </TBODY></TABLE>
                      </TD>
                    </TR>
                    <tr><td height=5 align=center><img src=images/line.jpg width=400></td></tr>

                    <tr>
		      <td colspan = 5 align=center>Other session dates:<br>
				Dates for the 2011 - 2012 year will be posted soon. </TD>
                    </TR>
		    <tr>
		      <td colspan = 5 align=center> 
                        &nbsp; &nbsp;<a href=gkfall.php4>Fall 2010</a>
                        &nbsp; &nbsp;<a href=gkwinter2.php4>Winter II 2011</a> 
                        &nbsp; &nbsp;<a href=gkspring.php4>Spring 2011</a>
                      </TD>
                    </TR>

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>

