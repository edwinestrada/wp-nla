<?php
/**
 * ACF Section - Tab Blocks Section
 *
 */
?>
<?php
  $texts = array();

  while(have_rows('tab_blocks')): the_row();
    array_push($texts, get_sub_field('text_block'));
  endwhile;
?>
<section id="tab-block-section-<?php echo $section_counter; ?>" class="tab-block dark-texture-4">
  <div class="container page-contents">
    <?php if(count($texts) > 0): ?>
      <div class='text-block-section row'>
      <div class="span12">
          <div id="tabs">
            <ul class="nav allcaps">
              <?php $tab_count = 0; ?>
              <?php while(have_rows('tab_blocks')): the_row(); ?>
                <li><a href="#tab-<?php echo $section_counter; ?>-<?php echo $tab_count; ?>"><?php the_sub_field('title'); ?></a></li>
              <?php $tab_count++; ?>
              <?php endwhile; ?>
            </ul>
            <?php $tab_count = 0; ?>
            <?php while(have_rows('tab_blocks')): the_row(); ?>
              <div id="tab-<?php echo $section_counter; ?>-<?php echo $tab_count; ?>">
                <!-- <p><strong><?php //the_sub_field('title'); ?></strong></p> -->
                <p><?php the_sub_field('caption'); ?></p>
              </div>
            <?php $tab_count++; ?>
            <?php endwhile; ?>
        </div><!-- #tabs -->
      </div><!-- .span12 -->
      </div><!-- .text-block-section .row-->
    <?php endif; ?>
  </div><!-- .container .page-contents -->
</section><!-- .dark-texture-4 -->
<script type="text/javascript">
  $(function() {
      $( "#tabs" ).tabs({
        collapsible: true
      });
    });
</script>
