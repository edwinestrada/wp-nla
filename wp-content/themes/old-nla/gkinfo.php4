<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>
<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>

              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>NLA Goalkeeper Program</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>NLA Goalkeeper Program</FONT></strong></P>

                        <P>Next Level Academy Goalkeeper Program will cover all aspects of the position.  Each season 
				throughout the year will offer a variety of training.  Players can sign up for an individual session(s) 
				or the full year.  Each session will provide the instruction and focus areas to get your goalkeeper ready.  
				All sessions of our Goalkeeper Program are held indoors at NetSports (our Morrisville location).</p>
 				<p>The Fall and Spring sessions which are offered for all age groups will be focused around the 
				in-season needs of each goalkeeper.  The Winter I session will offer goalkeepers a chance to refine 
				the skills built throughout the Fall, and clean them up as well as work on the physical aspect of the 
				position.  The Winter II session will serve as a pre-season to help focus on the details that they 
				need to work on before the outdoor season begins.</p>
 				<p>Throughout the year, sessions will cover basic and advanced skills the positions demands at every 
				level.  Session focus areas will be determined by your NLA Director/Coach. </p>
                     </TD>
                    </TR>
                    <TR>
                      <TD>
                        <font style="color: #ff0000;"><strong>For each session your player will need to:</strong></font> 
                              <br>&nbsp; &nbsp; &nbsp; 1. Wear a long sleeve goalie shirt
                              <br>&nbsp; &nbsp; &nbsp; 2. Bring a size 4 soccer ball (U9 - U11 players)
                               or size 5 soccer ball (U12 - U14 playres)
                              <br>&nbsp; &nbsp; &nbsp; 3. Bring a water bottle/ sports drink with their name on it 
                      </TD>
                    </TR>
                    <TR>
                      <TD>
                        <font style="color: #ff0000;"><strong>Goalkeeper Training Focus Areas:</strong></font> 
                              <br>&nbsp; &nbsp; &nbsp; How to warm up 
                              <br>&nbsp; &nbsp; &nbsp; Footwork and Handling
                              <br>&nbsp; &nbsp; &nbsp; Stance 
                              <br>&nbsp; &nbsp; &nbsp; Catching techniques
                              <br>&nbsp; &nbsp; &nbsp; High and low balls
                              <br>&nbsp; &nbsp; &nbsp; Positioning and angles
                              <br>&nbsp; &nbsp; &nbsp; Shot handling
                              <br>&nbsp; &nbsp; &nbsp; How to dive
                              <br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Low/High collapse
                              <br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Mid-level dives
                              <br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Parrying
                              <br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Reactions and reflexes
                              <br>&nbsp; &nbsp; &nbsp; Communication
                              <br>&nbsp; &nbsp; &nbsp; Confidence
                              <br>&nbsp; &nbsp; &nbsp; Core training
                              <br>&nbsp; &nbsp; &nbsp; Agility
                      </TD>
                    </TR>
                    <tr><td height=10></td></tr>
                    <TR>
                      <TD colspan=2> <font style="color: #ff0000;"><strong>Registration and Payment:</strong></font>
                        &nbsp All <strong>registration</strong> is done online at our 
                        website - <a href=www.nextlevelacademy.com> www.nextlevelacademy.com</a> under
			      the Goalkeeper Program link.  A waiver form is also required in order for your player to participate 
                      in this program.  Click <a href = "docs/NextLevelAcademyMedicalWaiver.doc">here</a> to download the 
                      waiver form.  Once you've completed it and had it notarized please mail it along with payment to:
                      <br>&nbsp; &nbsp; Next Level Academy
                      <br>&nbsp; &nbsp; 3717 Davis Drive
                      <br>&nbsp; &nbsp; Morrisville, Nc 27560
                      </td>
                    </tr>
                    <tr><td height=10></td></tr>

                    <tr><td height=5 align=center><img src=images/line.jpg width=400></td></tr>
                    <TR>
                      <TD align= center><strong>Dates and Registration</strong> <br>
				Dates for the 2011 - 2012 year will be posted soon. </td>
		    </tr>
		    <tr>
		      <td>
                        <p>
                        &nbsp; &nbsp;<a href=gkfall.php4>Fall 2010</a> 
                        &nbsp; &nbsp;<a href=gkwinter1.php4>Winter I 2010</a>
                        &nbsp; &nbsp;<a href=gkwinter2.php4>Winter II 2011</a> 
                        &nbsp; &nbsp;<a href=gkspring.php4>Spring 2011</a>
                      </TD>
                    </TR>
                    <tr><td height=10></td></tr>
<!--
		    <tr>
		      <td>
<?
/*
    $prgFile = "data/Goalies/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        $fh = fopen("data/Goalies/Regseason.txt", "r");
        $season = fgets($fh,25);
        fclose($fh);
        print("<p><strong>Registration for the Goalkeeper Program is available</strong>. &nbsp; &nbsp;");
        print("To register <a href=gkregshtml.php4><font style=\"color: #0000ff;\">click here.</font></a> </p>");
      }
      else 
        print("<p align=center>Online registration for the Goalkeeper Program is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the Goalkeeper Program is not open.</p>");
*/
                       
?>
		  </td>
		</tr>
-->
		    <tr>
		      <td>

		  </td>
		</tr>

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>