module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    project: {
      assets_css: 'assets/css',
      assets_scss: 'assets/scss',
      assets_js: 'assets/js',
      build_css: 'build/css',
      build_js: 'build/js'
    }
    ,sass: {
      dist: {
        options: {
          style: 'compact'
        },
        files: {
           'style.css' : '<%= project.assets_scss %>/style.scss'
          //  '<%= project.assets_css %>/<%= pkg.name %>-foundation-top-bar.css' : '<%= project.assets_scss %>/<%= pkg.name %>-foundation-top-bar.scss'
        }
      }
    }
    ,watch: {
      markup: {
        files: ['acf/section-tab_block.php'],
        tasks: ['sass'],
        options: { livereload: true }
      },
      styles: {
        files: ['<%= project.assets_scss %>/style.scss'],
        tasks: ['sass'],
        options: { livereload: true }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass'); // npm install grunt-contrib-sass --save-dev
  grunt.loadNpmTasks('grunt-contrib-watch'); // npm install grunt-contrib-watch --save-dev

 // npm install grunt-rsync

  grunt.registerTask('default', [
    'sass'
    ,'watch'
  ]);
}
