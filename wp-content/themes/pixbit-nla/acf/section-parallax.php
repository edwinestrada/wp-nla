<?php
/**
 * ACF Section - Parallax
 */
?>
<?php $parallax_image = get_sub_field('image'); ?>
<div id="parallax1">
  <div class="bg1" style="background-image:url(<?php echo $parallax_image['url']; ?>)"></div>
  <div class="pattern"></div>
  <div class="container">
    <div class="vertical-text">
      <p class="parallax-heading"><?php the_sub_field('top_heading'); ?></p>
      <p class="parallax-quote"><?php the_sub_field('middle_heading'); ?></p>
      <p class="parallax-author"><?php the_sub_field('lower_heading'); ?></p>
    </div>
  </div>
</div>