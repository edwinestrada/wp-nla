<?php
	/**
	 * @package Wordpress
	 * @subpackage pixbit-nla
	 */

	get_header(); ?>
		<!-- Home Starts
			================================================== -->
		<div id="homepage">
			<div class="container">
				<div class="row pull-center">

					<!-- Supersized Slider Starts -->
						<div class="slider-text ">
							<div class="span12">
								<div id="slidecaption"></div>
							</div>
						</div>

						<!-- Left, Right Arrow Buttons Starts -->
						<a id="prevslide" class="load-item"></a> <a id="nextslide" class="load-item"></a>
						<!-- Left, Right Arrow Buttons Starts -->
					<!-- Supersized Slider Ends -->

					<!-- Social Icons Starts -->
					<div class="social-icons pull-center">
						<div class="span12">
							<ul class="ch-grid">
								<li>
									<div class="ch-item">
										<div class="ch-info">
											<div class="ch-info-front ch-img-1"></div>
											<div class="ch-info-back"> <a href="http://www.facebook.com" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/facebook_dark.png" alt="" /></a> </div>
										</div>
									</div>
								</li>
								<li>
									<div class="ch-item">
										<div class="ch-info">
											<div class="ch-info-front ch-img-2"></div>
											<div class="ch-info-back"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/in_dark.png" alt="" /> </div>
										</div>
									</div>
								</li>
								<li>
									<div class="ch-item">
										<div class="ch-info">
											<div class="ch-info-front ch-img-3"></div>
											<div class="ch-info-back"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/twitter02_dark.png" alt="" /> </div>
										</div>
									</div>
								</li>
								<li>
									<div class="ch-item">
										<div class="ch-info">
											<div class="ch-info-front ch-img-4"></div>
											<div class="ch-info-back"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/dribbble_dark.png" alt="" /> </div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- Social Icons Ends -->
				</div>
			</div>
		</div>
		<div class="line-bottom"></div>
		<!-- Home Ends
			================================================== -->
		<!-- About Starts
			================================================== -->
		<section id="about" class="dark-texture-4">
			<div class="heading-from-left">
				<div class="from_left_top top_box_left"></div>
				<div class="bg-blue">
					<div class="container">
						<div class="icon">
							<h1>ABOUT US</h1>
							<p>Pellentesque habitant morbi tristique senectus</p>
						</div>
					</div>
				</div>
				<div class="from_left_bot bot_box_left"></div>
			</div>
			<div class="container page-contents">
				<!-- Row 1 Starts -->
				<div class="row">
					<div class="span5">
						<div class="picture"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/about/1.jpg"></div>
					</div>
					<div class="span7">
						<h1>RESPONSIVE & RETINA READY</h1>
						<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.</p>
					</div>
				</div>
				<!-- Row 2 Starts -->
				<div class="row">
					<div class="span5">
						<div class="picture"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/about/2.jpg"></div>
					</div>
					<div class="span7">
						<h1>FREE SUPPORT & UPDATES</h1>
						<p>Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est.</p>
					</div>
				</div>
				<!-- Row 2 Ends -->
				<div class="row" id="team">
					<div class="span12 pull-center">
						<h3 class="subheading">WHO COACHES ARE</h3>
					</div>
					<div class="span3">
						<div class="picture"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/team/1.jpg"></div>
						<div class="title">
							<div class="bg-blue2">
								<div class="name">JOHN DOE<br><span>U-18 COACH</span></div>
							</div>
							<div class="from_left_bot bot_box_left"></div>
						</div>
						<div class="team-social-icons pull-right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/facebook_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/in_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/skype_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/twitter02_active.png"></div>
						<div class="description">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</div>
					</div>
					<div class="span3">
						<div class="picture"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/team/2.jpg"></div>
						<div class="title">
							<div class="bg-blue2">
								<div class="name">JANE DOE<br><span>U-18 COACH</span></div>
							</div>
							<div class="from_left_bot bot_box_left"></div>
						</div>
						<div class="team-social-icons pull-right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/facebook_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/in_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/skype_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/twitter02_active.png"></div>
						<div class="description">Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.  Sed arcu. Cras consequat.</div>
					</div>
					<div class="span3">
						<div class="picture"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/team/3.jpg"></div>
						<div class="title">
							<div class="bg-blue2">
								<div class="name">JOHN DOE<br><span>U-18 COACH</span></div>
							</div>
							<div class="from_left_bot bot_box_left"></div>
						</div>
						<div class="team-social-icons pull-right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/facebook_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/in_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/skype_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/twitter02_active.png"></div>
						<div class="description">Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Aliquam tincidunt mauris eu risus.</div>
					</div>
					<div class="span3">
						<div class="picture"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/team/4.jpg"></div>
						<div class="title">
							<div class="bg-blue2">
								<div class="name">JANE DOE<br><span>U-18 COACH</span></div>
							</div>
							<div class="from_left_bot bot_box_left"></div>
						</div>
						<div class="team-social-icons pull-right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/facebook_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/in_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/skype_active.png"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/twitter02_active.png"></div>
						<div class="description">Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam.</div>
					</div>
				</div>
				<hr class="soften">
				<!-- Two Columns Starts -->
				<div class="row">
					<div class="span6">
						<h1>WHY CHOOSE US?</h1>
						<div class="accordion" id="accordion2">
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
									PELLENTESQUE HABITANT
									</a>
								</div>
								<div id="collapseOne" class="accordion-body collapse in">
									<div class="accordion-inner">
										Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
									PELLENTESQUE HABITANT
									</a>
								</div>
								<div id="collapseTwo" class="accordion-body collapse">
									<div class="accordion-inner">
										Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
									PELLENTESQUE HABITANT
									</a>
								</div>
								<div id="collapseThree" class="accordion-body collapse">
									<div class="accordion-inner">
										Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="span6">
						<h1>OUR CAPABILITIES</h1>
						<div class="progress progress-striped active">
							<div class="bar" style="width: 95%;"><span>HTML5 & CSS3</span></div>
						</div>
						<div class="progress progress-striped">
							<div class="bar" style="width: 85%;"><span>JQUERY</span></div>
						</div>
						<div class="progress progress-striped">
							<div class="bar" style="width: 75%;"><span>WORDPRESS</span></div>
						</div>
						<div class="progress progress-striped">
							<div class="bar" style="width: 65%;"><span>ECOMMERCE</span></div>
						</div>
						<div class="progress progress-striped">
							<div class="bar" style="width: 55%;"><span>CMS</span></div>
						</div>
					</div>
				</div>
				<!-- Two Columns Ends -->
			</div>
			<!-- /container -->
		</section>
		<!-- About Ends
			================================================== -->
		<!-- First Parallax Background (Client Says) Starts -->
		<div id="parallax1">
			<div class="bg1"></div>
			<div class="pattern"></div>
			<div class="container">
				<div class="vertical-text">
					<p class="parallax-heading">WHAT OUR PARENTS SAY</p>
					<p class="parallax-quote">Pellentesque habitant morbi tristique senectus et netus et malesuada.</p>
					<p class="parallax-author">John Doe</p>
				</div>
			</div>
		</div>
		<!-- First Parallax Background (Client Says) Ends -->
		<!-- Services Starts
			================================================== -->
		<section id="services" class="black-bg">
			<div class="heading-from-right">
				<div class="from_right_top top_box_right"></div>
				<div class="bg-blue">
					<div class="container">
						<div class="icon">
							<h1 style="text-align:right">VALUES</h1>
							<p style="text-align:right">Pellentesque habitant morbi tristique senectus</p>
						</div>
					</div>
				</div>
				<div class="from_right_bot bot_box_right"></div>
			</div>
			<div class="container page-contents">
				<div class="row">
					<!-- Three Columns Starts -->
					<div class="row">
						<div class="span4 pull-center">
							<div class="round-icons"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/services/1.png" alt=""/></div>
							<h2>PELLENTESQUE</h2>
							<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
						</div>
						<div class="span4 pull-center">
							<div class="round-icons"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/services/2.png" alt=""/></div>
							<h2>VESTIBULUM</h2>
							<p>Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit.</p>
						</div>
						<div class="span4 pull-center">
							<div class="round-icons"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/services/3.png" alt=""/></div>
							<h2>DONEC EU</h2>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aliquam tincidunt.</p>
						</div>
					</div>
					<!-- Three Columns Ends -->
					<!-- Three Columns Starts -->
					<div class="row">
						<div class="span4 pull-center">
							<div class="round-icons"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/services/4.png" alt=""/></div>
							<h2>PELLENTESQUE</h2>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aliquam tincidunt.</p>
						</div>
						<div class="span4 pull-center">
							<div class="round-icons"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/services/5.png" alt=""/></div>
							<h2>VESTIBULUM</h2>
							<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
						</div>
						<div class="span4 pull-center">
							<div class="round-icons"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/services/6.png" alt=""/></div>
							<h2>DONEC EU</h2>
							<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.</p>
						</div>
					</div>
					<!-- Three Columns Ends -->
					<!-- Three Columns Starts -->
					<div class="row">
						<!-- Column 1 Starts -->
						<div class="span4">
							<!-- Caption Starts -->
							<div class="bg-blue2">
								<div class="titlebar clearfix">
									<div class="pull-left title">Pellentesque</div>
									<div class="pull-right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/services/7.png"></div>
								</div>
							</div>
							<div class="from_right_bot bot_box_right"></div>
							<!-- Caption Ends -->
							<div class="discription">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</div>
						</div>
						<!-- Column 1 Starts -->
						<!-- Column 2 Starts -->
						<div class="span4">
							<!-- Caption Starts -->
							<div class="bg-blue2">
								<div class="titlebar clearfix">
									<div class="pull-left title">Pellentesque</div>
									<div class="pull-right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/services/8.png"></div>
								</div>
							</div>
							<div class="from_right_bot bot_box_right"></div>
							<!-- Caption Ends -->
							<div class="discription">Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis.</div>
						</div>
						<!-- Column 2 Starts -->
						<!-- Column 2 Starts -->
						<div class="span4">
							<!-- Caption Starts -->
							<div class="bg-blue2">
								<div class="titlebar clearfix">
									<div class="pull-left title">Pellentesque</div>
									<div class="pull-right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/services/9.png"></div>
								</div>
							</div>
							<div class="from_right_bot bot_box_right"></div>
							<!-- Caption Ends -->
							<div class="discription">Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</div>
						</div>
						<!-- Column 2 Starts -->
					</div>
					<!-- Three Columns Ends -->
				</div>
			</div>
		</section>
		<!-- Services Ends
			================================================== -->
		<!-- Second Parallax Background (Twitter) Starts -->
		<div id="parallax2">
			<div class="bg2"></div>
			<div class="pattern"></div>
			<div class="container">
				<div class="vertical-text">
					<div class="twitter-icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter/twitter-icon-1.png" alt=""></div>
					<div id="tweet_box">
						<div class="tweets"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Second Parallax Background (Twitter) Ends -->
		<!-- Catalog Starts
			================================================== -->
		<section id="cataloge" class="black-bg">
			<div class="heading-from-left">
				<div class="from_left_top top_box_left"></div>
				<div class="bg-blue">
					<div class="container">
						<div class="icon">
							<h1>PLAYERS</h1>
							<p>Pellentesque habitant morbi tristique senectus</p>
						</div>
					</div>
				</div>
				<div class="from_left_bot bot_box_left"></div>
			</div>
			<div class="container page-contents">
				<div class="row">
					<div class="span12">
						<!-- Cataloge Carousel Starts -->
						<div id="myCarousel2" class="carousel slide">
							<div class="carousel-inner">
								<!-- Item One Starts -->
								<div class="item active">
									<div class="row">
										<div class="featurette">
											<div class="span5 offset1">
												<img class="featurette-image" src="<?php echo get_stylesheet_directory_uri(); ?>/image/players/1.jpg" alt="">
												<div class="title">Aenean ultricies</div>
											</div>
											<div class="span5">
												<h2 class="featurette-heading">Vestibulum<br><span class="muted">Mauris</span></h2>
												<p class="lead">Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna.<br><br>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
											</div>
										</div>
									</div>
								</div>
								<!-- Item One Ends -->
								<!-- Item Two Starts -->
								<div class="item">
									<div class="row">
										<div class="featurette">
											<div class="span5 offset1">
												<h2 class="featurette-heading">Vestibulum<br><span class="muted">Mauris</span></h2>
												<p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur.<br><br>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
											</div>
											<div class="span5">
												<img class="featurette-image" src="<?php echo get_stylesheet_directory_uri(); ?>/image/players/2.jpg" alt="">
												<div class="title">Morbi in sem quis</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Item Two Ends -->
								<!-- Item Three Starts -->
								<div class="item">
									<div class="row">
										<div class="featurette">
											<div class="span5 offset1">
												<img class="featurette-image" src="<?php echo get_stylesheet_directory_uri(); ?>/image/players/3.jpg" alt="">
												<div class="title">Pellentesque habitant</div>
											</div>
											<div class="span5">
												<h2 class="featurette-heading">Vestibulum<br><span class="muted">Mauris</span></h2>
												<p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur.<br><br>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
											</div>
										</div>
									</div>
								</div>
								<!-- Item Three Ends -->
							</div>
							<a class="left carousel-control" href="#myCarousel2" data-slide="prev"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/slider/icons/back-2.png"></a> <a class="right carousel-control" href="#myCarousel2" data-slide="next"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/slider/icons/forward-2.png"></a>
						</div>
						<!-- Cataloge Carousel Ends -->
					</div>
				</div>
			</div>
		</section>
		<!-- Catalog Ends
			================================================== -->
		<!-- Third Parallax Background (Quote) Starts -->
		<div id="parallax4">
			<div class="bg4"></div>
			<div class="pattern"></div>
			<div class="container">
				<div class="vertical-text">
					<p class="parallax-quote">Pellentesque habitant morbi tristique senectus et netus et malesuada</p>
					<p class="parallax-author">John Doe</p>
				</div>
			</div>
		</div>
		<!-- Third Parallax Background (Quote) Ends -->
		<!-- Portfolio Starts
			================================================== -->
		<section id="portfolio" class="black-bg">
			<div class="heading-from-right">
				<div class="from_right_top top_box_right"></div>
				<div class="bg-blue">
					<div class="container">
						<div class="icon">
							<h1 style="text-align:right">EVENTS</h1>
							<p style="text-align:right">Pellentesque habitant morbi tristique senectus</p>
						</div>
					</div>
				</div>
				<div class="from_right_bot bot_box_right"></div>
			</div>
			<div class="container page-contents">
				<!-- Portfolio Nav Starts -->
				<div class="row">
					<div class="span12 pull-center">
						<div id="options">
							<ul id="filters" class="option-set" data-option-key="filter">
								<li><a href="#selected" data-option-value="*" class="selected">ALL</a></li>
								<li><a href="#web" data-option-value=".photos1">PHOTOS 1</a></li>
								<li><a href="#photos" data-option-value=".photos2">PHOTOS 2 </a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Portfolio Nav Ends -->
				<!-- Portfolio Items Starts -->
				<div class="row">
					<div class="span12">
						<ul class="thumbnails gallery" id="container-thumbs">
							<li class="span3 photos2">
								<a data-toggle="lightbox" href="#1" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/1.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span>
								</a><!-- Caption Starts -->
								<div class="bg-blue2">
									<div class="title">Pellentes<br><span>Vestibulum erat wisi</span></div>
								</div>
								<div class="from_left_bot bot_box_left"></div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos1">
								<a data-toggle="lightbox" href="#2" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/2.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div class="bg-blue2">
									<div class="title">Vestibulum<br><span>Pellentesque habitant</span></div>
								</div>
								<div class="from_left_bot bot_box_left"></div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos1">
								<a data-toggle="lightbox" href="#3" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/3.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Condimentum<br><span>morbi tristique</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos2">
								<a data-toggle="lightbox" href="#4" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/4.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Aliquam<br><span>feugiat vitae</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos1">
								<a data-toggle="lightbox" href="#5" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/5.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Mauris<br><span>ac turpis egestas</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos2">
								<a data-toggle="lightbox" href="#6" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/6.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Donec eu<br><span>consectetuer adipiscing</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos1">
								<a data-toggle="lightbox" href="#7" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/7.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Aliquam<br><span>Quisque sit amet</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos2">
								<a data-toggle="lightbox" href="#8" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/8.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Eleifend<br><span>Nam dui mi</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos1">
								<a data-toggle="lightbox" href="#9" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/9.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Malesuada<br><span>magna eros eu</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos2">
								<a data-toggle="lightbox" href="#10" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/10.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Consectetuer<br><span>condimentum sed</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos1">
								<a data-toggle="lightbox" href="#11" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/11.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Quisque<br><span>Donec eu libero</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
							<li class="span3 photos2">
								<a data-toggle="lightbox" href="#12" class="overlayzoom">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/12.jpg" class="image-shadow" alt="" ><span class="zoom"><span></span></span></a>
								<!-- Caption Starts -->
								<div id="subh">
									<div class="bg-blue2">
										<div class="title">Tincidunt<br><span>tempor sit amet</span></div>
									</div>
									<div class="from_left_bot bot_box_left"></div>
								</div>
								<!-- Caption Ends -->
							</li>
						</ul>
					</div>
					<!-- Portfolio Items Ends -->
					<!-- Portfolio Lightbox Items Starts -->
					<div id="1" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/1.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="2" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/2.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="3" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/3.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="4" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/4.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="5" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/5.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="6" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/6.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="7" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/7.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="8" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/8.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="9" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/9.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="10" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/10.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="11" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/11.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<div id="12" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
						<div class='lightbox-header'>
							<button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
						</div>
						<div class='lightbox-content'>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/portfolio/thumbs/12.jpg" alt="" >
							<div class="lightbox-caption">
								<p>Your caption here</p>
							</div>
						</div>
					</div>
					<!-- Portfolio Lightbox Items Ends -->
				</div>
			</div>
		</section>
		<!-- Portfolio Ends
			================================================== -->
		<!-- Third Parallax Background (Clients) Starts -->
		<div id="parallax3">
			<div class="bg3"></div>
			<div class="pattern"></div>
			<div class="container">
				<div class="vertical-text">
					<div class="heading">Our Partners</div>
					<ul class="clients">
						<li><a href="#"><img alt="" src="http://www.senramedia.com/wp-content/uploads/2012/11/nike-and-adidas-logo.png" ></a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Third Parallax Background (Clients) Ends -->
		<!-- Blog Starts
			================================================== -->
		<section id="blog" class="black-bg">
			<div class="heading-from-left">
				<div class="from_left_top top_box_left"></div>
				<div class="bg-blue">
					<div class="container">
						<div class="icon">
							<h1>PROJECTS</h1>
							<p>Pellentesque habitant morbi tristique senectus</p>
						</div>
					</div>
				</div>
				<div class="from_left_bot bot_box_left"></div>
			</div>
			<div class="container page-contents">
				<!-- Row 1 Starts-->
				<div class="row">
					<!-- Post 2 Starts-->
					<div class="span6">
						<div class="post">
							<a class="overlayreadmore" href="blog.html">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/blog/1.jpg" class="image-shadow" alt="" ><span class="readmore"><span></span></span></a>
							<!-- Caption Starts -->
							<div class="bg-blue2">
								<div class="title clearfix">
									<p class="pull-left">Pellentesque habitant</p>
									<a href="blog.html" class="pull-right read-more"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/blog/readmore.png"></a>
								</div>
							</div>
							<div class="from_left_bot bot_box_left"></div>
							<!-- Caption Ends -->
							<ul class="icons clearfix">
								<li class="cat">Uncategorized</li>
								<li class="date">April 07, 2013</li>
								<li class="user">Admin</li>
								<li class="comment">5 comments</li>
							</ul>
							<div class="description">Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci.</div>
						</div>
					</div>
					<!-- Post 2 Ends-->
					<!-- Post 2 Starts-->
					<div class="span6">
						<div class="post">
							<a class="overlayreadmore" href="blog.html">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/blog/2.jpg" class="image-shadow" alt="" ><span class="readmore"><span></span></span></a>
							<!-- Caption Starts -->
							<div class="bg-blue2">
								<div class="title clearfix">
									<p class="pull-left">Pellentesque habitant</p>
									<a href="blog.html" class="pull-right read-more"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/blog/readmore.png"></a>
								</div>
							</div>
							<div class="from_left_bot bot_box_left"></div>
							<!-- Caption Ends -->
							<ul class="icons clearfix">
								<li class="cat">Uncategorized</li>
								<li class="date">April 07, 2013</li>
								<li class="user">Admin</li>
								<li class="comment">5 comments</li>
							</ul>
							<div class="description">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est.</div>
						</div>
					</div>
					<!-- Post 2 Ends-->
				</div>
				<!-- Row 1 Ends-->
				<!-- Row 2 Starts-->
				<div class="row">
					<!-- Post 3 Starts-->
					<div class="span6">
						<div class="post">
							<a class="overlayreadmore" href="blog.html">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/blog/3.jpg" class="image-shadow" alt="" ><span class="readmore"><span></span></span></a>
							<!-- Caption Starts -->
							<div class="bg-blue2">
								<div class="title clearfix">
									<p class="pull-left">Pellentesque habitant</p>
									<a href="blog.html" class="pull-right read-more"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/blog/readmore.png"></a>
								</div>
							</div>
							<div class="from_left_bot bot_box_left"></div>
							<!-- Caption Ends -->
							<ul class="icons clearfix">
								<li class="cat">Uncategorized</li>
								<li class="date">April 07, 2013</li>
								<li class="user">Admin</li>
								<li class="comment">5 comments</li>
							</ul>
							<div class="description">Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis.</div>
						</div>
					</div>
					<!-- Post 3 Ends-->
					<!-- Post 4 Starts-->
					<div class="span6">
						<div class="post">
							<a class="overlayreadmore" href="blog.html">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/blog/4.jpg" class="image-shadow" alt="" ><span class="readmore"><span></span></span></a>
							<!-- Caption Starts -->
							<div class="bg-blue2">
								<div class="title clearfix">
									<p class="pull-left">Pellentesque habitant</p>
									<a href="blog.html" class="pull-right read-more"><img src="<?php echo get_stylesheet_directory_uri(); ?>/image/blog/readmore.png"></a>
								</div>
							</div>
							<div class="from_left_bot bot_box_left"></div>
							<!-- Caption Ends -->
							<ul class="icons clearfix">
								<li class="cat">Uncategorized</li>
								<li class="date">April 07, 2013</li>
								<li class="user">Admin</li>
								<li class="comment">5 comments</li>
							</ul>
							<div class="description">Tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</div>
						</div>
					</div>
					<!-- Post 4 Ends-->
				</div>
				<!-- Row 2 Ends-->
			</div>
		</section>
		<!-- Blog Ends
			================================================== -->
		<!-- Third Parallax Background (Partner) Starts -->
		<div id="parallax3">
			<div class="bg3"></div>
			<div class="pattern"></div>
			<div class="container">
				<div class="vertical-text">
					<div class="heading">Our Sponsors</div>
					<ul class="clients">
						<li><a href="#"><img alt="" src="http://www.senramedia.com/wp-content/uploads/2012/11/nike-and-adidas-logo.png" ></a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Third Parallax Background (Partner) Ends -->
		<!-- Videos Starts
			================================================== -->
		<section id="video" class="dark-texture-1">
			<div class="heading-from-right">
				<div class="from_right_top top_box_right"></div>
				<div class="bg-blue">
					<div class="container">
						<div class="icon">
							<h1 style="text-align:right">VIDEOS</h1>
							<p style="text-align:right">Pellentesque habitant morbi tristique senectus</p>
						</div>
					</div>
				</div>
				<div class="from_right_bot bot_box_right"></div>
			</div>
			<div class="container page-contents">
				<div class="row">
					<div class="span10 offset1">
						<div class="videoWrapper">
							<iframe src="http://player.vimeo.com/video/35514005?wmode=transparent" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen height="250" width="475"></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Videos Ends
			================================================== -->
		<!-- Six Parallax Background (Contact) Starts -->
		<div id="parallax6">
			<div class="bg6"></div>
			<div class="pattern"></div>
			<div class="container">
				<div class="vertical-text">
					<p class="parallax-line1">FEEL FREE TO CONTACT US</p>
					<p class="parallax-line2">GET HELP AND SUPPORT</p>
				</div>
			</div>
		</div>
		<!-- Six Parallax Background (Contact) Ends -->
		<!-- Contact Us Starts
			================================================== -->
		<section id="contact" class="dark-texture-2">
			<div class="heading-from-left">
				<div class="from_left_top top_box_left"></div>
				<div class="bg-blue">
					<div class="container">
						<div class="icon">
							<h1>CONTACT US</h1>
							<p>Pellentesque habitant morbi tristique senectus</p>
						</div>
					</div>
				</div>
				<div class="from_left_bot bot_box_left"></div>
			</div>
			<div class="container page-contents">
			<!-- Form Starts -->
			<div class="row">
				<!-- Form Starts -->
				<div class="form-element">
					<div class="span12 center">
						<h3 class="subheading">GET IN TOUCH WITH US</h3>
						<h4>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</h4>
						<div class="done">
							<h3>Thank you ! We have received your message.</h3>
						</div>
					</div>
					<div class="form">
						<form method="post" action="process.php">
							<div class="span4">
								<input type="text" name="name" class="text" placeholder="Name *" style="width:100%"/>
							</div>
							<div class="span4"><input type="text" name="email" class="text"  placeholder="Email *" style="width:100%"/></div>
							<div class="span4"><input type="text" name="website" class="text"  placeholder="Website" style="width:100%"/></div>
							<div class="span12 pull-center">
								<textarea name="comment" class="text textarea" placeholder="Comment *" style="width:100%; height:100px;"></textarea>
								<input type="submit" class="btn btn-info btn-large" value="SUBMIT" id="submit"/>
							</div>
						</form>
					</div>
				</div>
				<!-- Form Ends-->
			</div>
			<!-- Form Ends -->
			<!-- Address Starts -->
			<div class="address">
				<div class="row">
					<div class="span12 pull-center">
						<h3 class="subheading">GET HELP AND SUPPORT</h3>
					</div>
				</div>
				<div class="row">
					<div class="span3">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/contact/address.png" class="pull-left">
						<h3>POSTAL ADDRESS</h3>
						<p>Envato<br>
							PO Box 21177<br>
							Little Lonsdale St, Melbourne <br>
							Victoria 8011 Australia<br>
							ABN 11 119 159 741
						</p>
					</div>
					<div class="span3">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/contact/street.png" class="pull-left">
						<h3>STREET ADDRESS</h3>
						<p>Envato<br>
							Level 13, 2 Elizabeth St, <br>
							Melbourne <br>
							Victoria 3000 Australia
						</p>
					</div>
					<div class="span3">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/contact/phone.png" class="pull-left">
						<h3>TELEPHONE</h3>
						<p>Phone: +61 3 8376 6284<br>
							Fax: +61 3 8376 6284
						</p>
					</div>
					<div class="span3">
						<h3>MAP LOCATION</h3>
						<iframe width="100%" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=+&amp;q=envato&amp;ie=UTF8&amp;hq=envato&amp;hnear=&amp;t=m&amp;ll=-37.815174,144.966858&amp;spn=0.006295,0.006295&amp;output=embed"></iframe>
					</div>
					<!-- Address Ends -->
					<!-- Say Hello Starts -->
					<div class="row">
						<div class="span12 say-hello pull-center">
							<h3 class="subheading">SAY HELLO...</h3>
							<p class="lead">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. </p>
							<br class="clearfix">
							<!-- Social Icons Starts -->
							<ul class="ch-grid">
								<li>
									<div class="ch-item">
										<div class="ch-info">
											<div class="ch-info-front ch-img-1"></div>
											<div class="ch-info-back"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/facebook_dark.png" alt="" /> </div>
										</div>
									</div>
								</li>
								<li>
									<div class="ch-item">
										<div class="ch-info">
											<div class="ch-info-front ch-img-2"></div>
											<div class="ch-info-back"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/in_dark.png" alt="" /> </div>
										</div>
									</div>
								</li>
								<li>
									<div class="ch-item">
										<div class="ch-info">
											<div class="ch-info-front ch-img-3"></div>
											<div class="ch-info-back"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/twitter02_dark.png" alt="" /> </div>
										</div>
									</div>
								</li>
								<li>
									<div class="ch-item">
										<div class="ch-info">
											<div class="ch-info-front ch-img-4"></div>
											<div class="ch-info-back"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/dribbble_dark.png" alt="" /> </div>
										</div>
									</div>
								</li>
							</ul>
							<!-- Social Icons Ends -->
						</div>
					</div>
					<!-- Say Hello Ends -->
					<hr>
					<!-- Copyright Start -->
					<div class="row pull-center">
						<div class="span12 copyright"> © 2000-2013 Fortune. All Rights Reserved. </div>
					</div>
					<!-- Copyright Ends -->
				</div>
			</div>
		</section>
		<!-- Contact Us Ends
			================================================== -->
<?php get_footer(); ?>