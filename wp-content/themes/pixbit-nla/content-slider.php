<?php
	/**
	 * Template Name: Slider
	 */
get_header(); ?>
<div id="homepage">
	<div class="container">
		<div class="row pull-center">

			<!-- Supersized Slider Starts -->
				<div class="slider-text ">
					<div class="span12">
						<div id="slidecaption"></div>
					</div>
				</div>

				<!-- Left, Right Arrow Buttons Starts -->
				<!-- <a id="prevslide" class="load-item"></a><a id="nextslide" class="load-item"></a> -->
				<!-- Left, Right Arrow Buttons Starts -->
			<!-- Supersized Slider Ends -->

			<?php //get_template_part('templates/content', 'social-icons'); ?>
		</div>
	</div>
</div>
<div class="line-bottom"></div>

<section id="announcements">
	<div class="container page-contents">
		<div class="row">
			<div class="span3">
				<h3><?php the_field("far_left_title", "options") ?></h3>
				<?php the_field("far_left_block", "options"); ?>
			</div>
			<div class="span3">
				<h3><?php the_field("left_middle_title", "options") ?></h3>
				<?php the_field("left_middle_block", "options"); ?>
			</div>
			<div class="span3">
				<h3><?php the_field("right_middle_title", "options") ?></h3>
				<?php the_field("right_middle_block", "options"); ?>
			</div>
			<div class="span3">
				<h3><?php the_field("far_right_title", "options") ?></h3>
				<?php the_field("far_right_block", "options"); ?>
			</div>
			<hr>
		</div>
	</div>
</section>

<?php the_content(); ?>

<?php get_footer(); ?>
<script type="text/javascript">
  var header_slides = [];
  <?php while(has_sub_field('slides')): ?>
    <?php
     $image = get_sub_field('image');
     $title = get_sub_field('title');
     $caption = get_sub_field('caption');
     $link = get_sub_field('link');
    ?>
    // console.log("<?php echo $image['url']; ?>");
    // console.log("<?php echo $title; ?>");
    // console.log("<?php echo $caption; ?>");
    header_slides.push(
      <?php echo  "{
                image: '".$image['url']."',
                title: '".$title."<div class=\"slidedescription\">".$caption."</div>',
                thumb: '',
                url  : '\"".$link."\"'
            }";
      ?>
    );//header_slides.push()
  <?php endwhile; ?>

  jQuery(function($){
    $.supersized({
      // Functionality
      slideshow       :   1,      // Slideshow on/off
      autoplay        :   1,      // Slideshow starts playing automatically
      start_slide     :   1,      // Start slide (0 is random)
      stop_loop       :   0,      // Pauses slideshow on last slide
      random          :   0,      // Randomize slide order (Ignores start slide)
      slide_interval  :   4000,   // Length between transitions
      transition      :   1,      // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
      transition_speed: 1000,   // Speed of transition
      new_window      : 1,      // Image links open in new window/tab
      pause_hover     :   0,      // Pause slideshow on hover
      keyboard_nav    :   1,      // Keyboard navigation on/off
      performance     : 1,      // 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
      image_protect   : 1,      // Disables image dragging and right click with Javascript

      // Size & Position
      min_width        :   0,     // Min width allowed (in pixels)
      min_height       :   0,     // Min height allowed (in pixels)
      vertical_center  :   1,     // Vertically center background
      horizontal_center:   1,     // Horizontally center background
      fit_always       :   0,     // Image will never exceed browser width or height (Ignores min. dimensions)
      fit_portrait     :   1,     // Portrait images will not exceed browser height
      fit_landscape    :   0,     // Landscape images will not exceed browser width

      // Components
      slide_links         : 'blank',  // Individual links for each slide (Options: false, 'num', 'name', 'blank')
      thumb_links         : 1,      // Individual thumb links for each slide
      thumbnail_navigation:   0,      // Thumbnail navigation
      slides              : header_slides,     // Slideshow Images

      // Theme Options
      progress_bar      : 1,      // Timer for each slide
      mouse_scrub       : 0
    });//$.supersized
  });
</script>
