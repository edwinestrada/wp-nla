<?php
	/**
	 * Template Name: Form
	 * @package Wordpress
	 * @subpackage pixbit-nla
	 */
get_header(); ?>

<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="sections-wrapper">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		    <section class="content-block dark-texture-1">
		      <div class="container page-contents">
		          <div class='content-block-section row'>
		            <div class="span12">
									<?php the_content(); ?>
		            </div>
		          </div><!-- .content-block-section .row-->
		      </div><!-- .container .page-contents -->
		    </section><!-- .dark-texture-4 -->
		<?php endwhile; ?>
		<?php endif; ?>

<?php get_footer(); ?>
