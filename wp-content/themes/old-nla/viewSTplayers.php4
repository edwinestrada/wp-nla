<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                <TD align=middle background=images/darkgraybg.gif>
<?
  $prgtype= $_GET['prgtype'];
  $prgday = $_GET['prgday'];
  if ( $prgtype == "FB" )
    $prgage = $_GET['age'];

  $dayofWeek = jddayofweek($prgday, 1); 

  $maxITplayers = 4;
  $maxGTplayers = 4;
  $maxGKplayers = 6;
  $maxFBplayers = 40;

  switch ( $prgtype )
  {
    case "U7":
      print("<STRONG><FONT color=#ffffff>Specialty Training - U7 thru U10 players</FONT></STRONG>");
      $signupfile = "data/stdata/Rec".$prgday.".txt";
      break;

    case "U11":
      print("<STRONG><FONT color=#ffffff>Specialty Training - U11 thru U14 players</FONT></STRONG>");
      $signupfile = "data/stdata/Cla".$prgday.".txt";
      break;

    case "Gk":
      print("<STRONG><FONT color=#ffffff>Specialty Training - Goalkeeps</FONT></STRONG>");
      $signupfile = "data/stdata/GK".$prgday.".txt";
      break;

    case "FB":
      print("<STRONG><FONT color=#ffffff>Specialty Training - Finishing/Ball Striking players</FONT></STRONG>");
      $signupfile = "data/stdata/FB".$prgday.".txt";
      break;


  }

?>
                </TD>
                <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
              </TR>
            </tbody></table>
          </td>
        </TR>


<?

// read in default coaches assignments.
// array is indexed by day and type ....  coach[Monday][U7-1] = name

  $fh = fopen("data/stdata/scoaches.txt", "r");
  while ( $coachInfo= fgets($fh, 512) ) {
    $parms = explode (",", $coachInfo, 2);
    switch ( chop($parms[0]) ) {
      case "Monday":
      case "Tuesday":
      case "Wednesday":
      case "Thursday":
      case "Friday":
      case "Saturday":
      case "Sunday":
        $dayindex = chop($parms[0]);
      break;

      case "end";
      break;

      default:
        $sessindex = chop($parms[0]);
        $cname = chop($parms[1]);
        $coach[$dayindex][$sessindex] = $cname;
      break;

    }
  }
  fclose($fh);

// check to see if default assignment is overridden
  $ncIndex = 0;
  $coachassignment = "data/stdata/c".$prgday.".txt";
  if ( file_exists($coachassignment) ) {
    $fh = fopen($coachassignment, "r");
    while ( $coachInfo= fgets($fh, 512) ) {
      $parms = explode (",", $coachInfo, 2);
      $cname = chop($parms[0]);
      $stype = chop ($parms[1]);
      if ( $stype == "None" )
        $noCoach[$ncIndex++] = $cname;
      else 
        $coach[$dayofWeek][$stype] = $cname;
    }
  }



  switch ( $prgtype )       
  {
   
    case "U7":
      $gtPlayerIndex=0;
      $itPlayerIndex=0;
      $numberITplayers = $maxITplayers;
      $numberGTplayers = $maxGTplayers;
      if ( file_exists($signupfile) ) {
        $fh = fopen($signupfile, "r");
        while ( $playerInfo= fgets($fh, 512) ) {

          $parms = explode ("^", $playerInfo, 2);
          if ( chop($parms[1]) == "2" ) {
            $numberGTplayers--;
            $gtPlayer[$gtPlayerIndex++] = $parms[0];
          }
          else {
            $numberITplayers--;
            $itPlayer[$itPlayerIndex++] = $parms[0];
          }
        }
      }

      print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"75%\" border=0><TBODY>\n");
      print("<tr><td align=center valign=top><table cellSpacing=0 cellPadding=0 width=\"200\" border=0><TBODY>\n");
      print("<tr><td height=10 valign=top>&nbsp;<form method=\"post\" action=\"stregister.php4\"</td></tr>\n");
      print("<tr><td valign=top>U7-U10<br>Specialty Training</b></td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");

      for ($cp = 0; $cp < 2; $cp++ ) {
        if ( $cp == 0 ) 
          $cpindex = "U7-1";
        else
          $cpindex = "U7-2";
        $printcoach[$cp] = $coach[$dayofWeek][$cpindex];
        if ( $printcoach[$cp]  == "None" )
          $printcoach[$cp]  = "TBD";
        else {
          for ( $i=0; $i < $ncIndex; $i++ ) {
            if ( $printcoach[$cp]  == $noCoach[$i] )
              $printcoach[$cp]  = "TBD";
          }
        }
      }
     print("<tr><td>Coaches:<br>".$printcoach[0]." / ".$printcoach[1]." </td></tr>");
     $gtPlayerIndex=0;
     $itPlayerIndex=0;

      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxITplayers ) {
        if ( $numberITplayers > 0 ) {
          $numberITplayers--;
        }
        else
          print("<tr><td>".$itPlayer[$itPlayerIndex++]."</td></tr>");

        $loopindex++;
      }
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      print("<tr><td><INPUT type=\"hidden\" name=\"coach\" value=\"".$printcoach[0]."^".$printcoach[1]."\">\n");

      print("</form></tbody></table></td>\n");

      print("<td align=center valign=top><table cellSpacing=0 cellPadding=0 width=\"200\" border=0><TBODY>\n");
      print("<tr><td height=10 valign=top>&nbsp;<form method=\"post\" action=\"stregister.php4\"</td></tr>\n");
      print("<tr><td>U7-U10<br>Specialty Training</b></td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");

      print("<tr><td>Coaches:<br>".$printcoach[0]." / ".$printcoach[1]." </td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxGTplayers ) {
        if ( $numberGTplayers > 0 ) {
          $numberGTplayers--;
        }
        else
          print("<tr><td>".$gtPlayer[$gtPlayerIndex++]."</td></tr>");
        $loopindex++;
      }

      print("<tr><td height=5>&nbsp;</td></tr>\n");
      print("</form></tbody></table></td></tr>\n");
      
      print("<tr><td colspan=2 height=10>&nbsp;</form></td></tr>\n");
      print("</tbody></table></td></tr>\n");

    break;

    case "U11":
      $gtPlayerIndex=0;
      $itPlayerIndex=0;
      $numberITplayers = $maxITplayers;
      $numberGTplayers = $maxGTplayers;
      if ( file_exists($signupfile) ) {
        $fh = fopen($signupfile, "r");
        while ( $playerInfo= fgets($fh, 512) ) {
          $parms = explode ("^", $playerInfo, 2);
          if ( chop($parms[1]) == "2" ) {                 
            $numberGTplayers--;
            $gtPlayer[$gtPlayerIndex++] = $parms[0];
          }
          else {
            $numberITplayers--;
            $itPlayer[$itPlayerIndex++] = $parms[0];
          }
        }
      }

      print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"90%\" border=0><TBODY>\n");
      print("<tr><td align=center valign=top><table cellSpacing=0 cellPadding=0 width=\"200\" border=0><TBODY>\n");
      print("<tr><td height=10 valign=top>&nbsp;<form method=\"post\" action=\"stregister.php4\"</td></tr>\n");
      print("<tr><td valign=top>U11-U14<br>Specialty Training</b></td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");

      for ($cp = 0; $cp < 2; $cp++ ) {
        if ( $cp == 0 ) 
          $cpindex = "U11-1";
        else
          $cpindex = "U11-2";
        $printcoach[$cp] = $coach[$dayofWeek][$cpindex];
        if ( $printcoach[$cp]  == "None" )
          $printcoach[$cp]  = "TBD";
        else {
          for ( $i=0; $i < $ncIndex; $i++ ) {
            if ( $printcoach[$cp]  == $noCoach[$i] )
              $printcoach[$cp]  = "TBD";
          }
        }
      }
     print("<tr><td>Coaches:<br>".$printcoach[0]." / ".$printcoach[1]." </td></tr>");
     $gtPlayerIndex=0;
     $itPlayerIndex=0;

      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxITplayers ) {
        if ( $numberITplayers > 0 ) {
          $numberITplayers--;
        }
        else
          print("<tr><td>".$itPlayer[$itPlayerIndex++]."</td></tr>");

        $loopindex++;
      }
      print("</form></tbody></table></td>\n");

      print("<td align=center valign=top><table cellSpacing=0 cellPadding=0 width=\"200\" border=0><TBODY>\n");
      print("<tr><td height=10>&nbsp;<form method=\"post\" action=\"stregister.php4\"</td></tr>\n");
      print("<tr><td>U11-14<br>Spceialty Training</b></td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");

      print("<tr><td>Coaches:<br>".$printcoach[0]." / ".$printcoach[1]." </td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxGTplayers ) {
        if ( $numberGTplayers > 0 ) {
          $numberGTplayers--;
        }
        else
          print("<tr><td>".$gtPlayer[$gtPlayerIndex++]."</td></tr>");

        $loopindex++;
      }
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      print("</form></tbody></table></td></tr>\n");
      
      print("<tr><td colspan=2 height=10>&nbsp;</form></td></tr>\n");
      print("</tbody></table></td></tr>\n");

    break;

    case "Gk":
      $gkPlayerIndex = 0;
      $numberplayers = $maxGKplayers;
      if ( file_exists($signupfile) ) {
        $fh = fopen($signupfile, "r");
        while ( $playerInfo= fgets($fh, 512) ) {
          $numberplayers--;
          $gkPlayer[$gkPlayerIndex++] = $playerInfo;
        }
      }
      print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"75%\" border=0><TBODY>\n");
      print("<tr><td height=10 valign=top>&nbsp;<form method=\"post\" action=\"stregister.php4\"</td></tr>\n");
      print("<tr><td valign=top><b>Goalkeeper Training</b></td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $printcoach = $coach[$dayofWeek]['GK'];
      if ( $printcoach == "None" )
        $printcoach = "TBD";
      else {
        for ( $i=0; $i < $ncIndex; $i++ ) {
          if ( $printcoach == $noCoach[$i] )
            $printcoach = "TBD";
        }
      }
     print("<tr><td>Coach: ".$printcoach."</td></tr>");
     $gkPlayerIndex = 0;

      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      while ( $loopindex < $maxGKplayers ) {
        if ( $numberplayers > 0 ) {
          $numberplayers--;
        }
        else
          print("<tr><td>".$gkPlayer[$gkPlayerIndex++]."</td></tr>");

        $loopindex++;
      }
      print("<tr><td height=5>&nbsp;</td></tr>\n");  
      print("<tr><td height=10>&nbsp;</form></td></tr>\n");
      print("</tbody></table></td></tr>\n");

    break;

    case "FB":
      $fbPlayerIndex = 0;
      $numberplayers = $maxFBplayers;
      $numberGKplayers = 4;
      if ( file_exists($signupfile) ) {
        $fh = fopen($signupfile, "r");
        while ( $playerInfo= fgets($fh, 512) ) {
          $parms = explode ("^", $playerInfo, 2);
/*
          if ( chop($parms[1]) == "GK" ) 
            $numberGKplayers--;
          else
            $numberplayers--;
*/

          $fbPlayer[$fbPlayerIndex++] = $parms[0];
        }
      }

      print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"75%\" border=0><TBODY>\n");
      print("<tr><td height=10 valign=top>&nbsp;<form method=\"post\" action=\"stregister.php4\"</td></tr>\n");
      print("<tr><td align=center colspan=4 valign=top><b>Finishing / Ball Striking</b></td></tr>");
      print("<tr><td height=5>&nbsp;</td></tr>\n");


      $printcoach = $coach[$dayofWeek]['Finish'];
      if ( $printcoach == "None" )
        $printcoach = "TBD";
      else {
        for ( $i=0; $i < $ncIndex; $i++ ) {
          if ( $printcoach == $noCoach[$i] )
            $printcoach = "TBD";
        }
      }
     print("<tr><td colspan=4 align=center>Coach: ".$printcoach."</td></tr>");

      print("<tr><td height=5>&nbsp;</td></tr>\n");
      $loopindex=0;
      $printIndex = 0;
      while ( $loopindex < $fbPlayerIndex ) {
        print("<tr>");
        for ( $j=0; $j<4; $j++ ) {
          if ( $printIndex < $fbPlayerIndex ) 
            print("<td>".$fbPlayer[$printIndex++]."</td>");

          $loopindex++;
        }
        print("</tr>");
  
      }  
      print("<tr><td height=10>&nbsp;</form></td></tr>\n");
      print("</tbody></table></td></tr>\n");

    break;

  }
  
?>
        <TR>
          <TD>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                <TD background=images/graymidbottom.gif>&nbsp;</TD>
                <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>       
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>