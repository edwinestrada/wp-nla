<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>
        <?php if (is_home() || is_front_page()) {
        bloginfo('name');
    } else {
        wp_title('');
    }?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="js/bootstrap/html5shiv.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/image/ico/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/image/ico/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/image/ico/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/image/ico/favicon.png">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class( 'not-front-page' ); ?>>
    <div class="navbar navbar-inverse not-front-page dark-texture-2">
      <div class="navbar-inner">
        <div class="container">
          <div class="is-front-page row">
          <div class="pull-center not-front-page span2" id="logo">
            <a class="brand" href="#homepage">
              <?php $logo_main = get_field('subpage_logo', 'option'); ?>
              <img src="<?php echo $logo_main['url']; ?>">
            </a>
          </div><!-- #logo -->
          <div class="pull-center not-front-page span10" id="top-nav">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <div class="nav-collapse collapse pull-center">
              <!-- Your menu items are placed here -->
              <?php
              wp_nav_menu( array(
                'theme_location'  => 'main-menu',
                'container'       => false,
                'menu'            => '',
                'menu_class'      => 'nav allcaps',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'link_after'      => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth'           => 0,
                'walker'          => new Nla_walker_nav_menu
              ));

              ?>

            <!-- <div class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">Dropdown trigger</a>
              <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                ...
              </ul>
            </div> -->

<!-- ERASE LATER -->
<!--               <ul class="nav">
                <li class="active"><a href="#homepage" data-toggle="collapse" data-target=".nav-collapse">HOME</a></li>
                <li><a href="#about" data-toggle="collapse" data-target=".nav-collapse">ABOUT</a></li>
                <li><a href="#services" data-toggle="collapse" data-target=".nav-collapse">VALUES</a></li>
                <li><a href="#cataloge" data-toggle="collapse" data-target=".nav-collapse">PLAYERS</a></li>
                <li><a href="#portfolio" data-toggle="collapse" data-target=".nav-collapse">EVENTS</a></li>
                <li><a href="#blog" data-toggle="collapse" data-target=".nav-collapse">PARTNERS</a></li>
                <li><a href="#video" data-toggle="collapse" data-target=".nav-collapse">VIDEOS</a></li>
                <li><a href="#contact" data-toggle="collapse" data-target=".nav-collapse">CONTACT</a></li>
                <li><a href="#" class="external" target="_blank" data-toggle="collapse" data-target=".nav-collapse">PURCHASE</a></li>
              </ul>
 -->
            </div>
            <!--/.nav-collapse -->
          </div>
        </div><!-- .is-not-front-page .row -->
        </div><!-- .container -->
      </div>
    </div>
