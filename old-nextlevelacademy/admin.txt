<tr>
  <td background="images/graymidbottom.gif">
    <p align="center" font style="font-size: 14px;">
    Please make your selection from the following choices.
  </td>
</tr>
<tr><td height=5></td</tr>
<tr>
  <td>
    <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
      <TR>
        <td valign=middle width=49%>
          <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 background="images/graymidbottom.gif"><TBODY>
            <TR>
              <TD VAlign=top width=21 background=images/darkgraybg.gif>
                <IMG height=21 src="images/darkgrayleft.gif" width=21>
              </TD>
              <TD align=middle background=images/darkgraybg.gif>
                <STRONG><FONT color=#ffffff>Registration Items</FONT></STRONG>
              </TD>
              <TD VAlign=top width=19 background=images/darkgraybg.gif>
                <IMG height=21 src="images/darkgrayright.gif" width=19>
              </TD>
            </TR>
          </tbody></table>
        </td>
        <td width=2% rowspan=2></td>
        <td width=49% valign=top>
          <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 background="images/graymidbottom.gif"><TBODY>
            <TR>
              <TD VAlign=top width=21 background=images/darkgraybg.gif>
                <IMG height=21 src="images/darkgrayleft.gif" width=21>
              </TD>
              <TD align=middle background=images/darkgraybg.gif>
                <STRONG><FONT color=#ffffff>Results Items</FONT></STRONG>
              </TD>
              <TD VAlign=top width=19 background=images/darkgraybg.gif>
                <IMG height=21 src="images/darkgrayright.gif" width=19>
              </TD>
            </TR>
          </tbody></table>
        </td>
      </tr>
 
      <TR>
        <td valign=top width=50%>
          <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 background="images/graymidbottom.gif"><TBODY>
            <TR>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="regdisplay.php4?&prgtype=Academy">Morrisville Academy Registration Display</a>
                <br>&nbsp;  Display/download the reg info for the academies.
              </td>
            </tr>
            <tr><td height=10></td></tr>

            <TR>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="regdisplay.php4?&prgtype=RalWFAcademy">Ral/WF Academy Registration Display</a>
                <br>&nbsp;  Display/download the reg info for the academies.
              </td>
            </tr>
            <tr><td height=10></td></tr>

            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="regdisplay.php4?&prgtype=Center">Morrisville Centers Registration Display</a>
                <br>&nbsp;  Display/download the reg info for the centers.
              </td>
            </tr>
            <tr><td height=10></td></tr>

            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="regdisplay.php4?&prgtype=RalWFCenter">Ral/WF Centers Registration Display</a>
                <br>&nbsp;  Display/download the reg info for the centers.
              </td>
            </tr>
            <tr><td height=10></td></tr>

            <TR>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="regdisplay.php4?&prgtype=Tryout">Morrisville Tryouts Registration Display</a>
                <br>&nbsp;  Display/download the reg info for the tryouts.
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <TR>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="regdisplay.php4?&prgtype=TryoutRal">Raleigh Tryouts Registration Display</a>
                <br>&nbsp;  Display/download the reg info for the tryouts.
              </td>
            </tr>
            <tr><td height=10></td></tr>

            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="regdisplay.php4?&prgtype=Functional">Functionals Registration Display</a>
                <br>&nbsp;  Display/download the reg info for the Functionals.
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="campregdisplay.php4">Camps Registration Display</a>
                <br>&nbsp;  Display/download the reg info for the summer camp.
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="summerregdisplay.php4">Summer Play Registration Display</a>
                <br>&nbsp;  Display/download the reg info for Summer Play.
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="gkregdisplay.php4">Goalkeeper Program Display</a>
                <br>&nbsp;  Display/download the reg info for Goalkeeper Program.
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="gradsregdisplay.php4">Graduate Program Display</a>
                <br>&nbsp;  Display/download the reg info for Graduate Program.
              </td>
            </tr>

            <TR>
              <TD>
                <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                  <TR>
                    <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                    <TD background=images/graymidbottom.gif>&nbsp;</TD>
                    <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                  </TR>
                </tbody></table>
              </TD>
            </TR>
          </tbody></table>
        </td>
        <td valign=top width=50%>
          <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 background="images/graymidbottom.gif"><TBODY>
            <TR>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="createresults.php4?&prgtype=Team">Tryout Results(team)</a>
                <br>&nbsp;  Create results for the NLA Team
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="createresults.php4?&prgtype=Academy">Academy Results</a>
                <br>&nbsp;  Create results for the Academy (players that did not make NLA Team)
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <TR>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="createresults.php4?&prgtype=Suppteam">Supplemental Results(team)</a>
                <br>&nbsp;  Create results for the SUPPLEMENTAL Team tryouts.
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="createresults.php4?&prgtype=Suppacademy">Academy Supplemental Results</a>
                <br>&nbsp;  Create results for the SUPPLEMENTAL Academy (players that did not make NLA Team)
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="activateshtml.php4?&prgtype=Tryouts">Activate Results</a>
                <br>&nbsp;  Activate the tryout results so that they may be viewed.
              </td>
            </tr>
            <tr><td height=10></td></tr>
            <tr>
              <td>
                &nbsp; &nbsp &nbsp; &nbsp <a href="activateshtml.php4?&prgtype=Supps">Activate Supplemental Results</a>
                <br>&nbsp;  Activate the SUPPLEMENTAL results so that they may be viewed.
              </td>
            </tr>
            <TR>
              <TD>
                <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                  <TR>
                    <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                    <TD background=images/graymidbottom.gif>&nbsp;</TD>
                    <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                  </TR>
                </tbody></table>
              </TD>
            </TR>
          </tbody></table>
        </td>
      </TR>
    </tbody></table>
  </td>        
</TR>
<tr><td height=5></td</tr>
<tr>
  <td>
    <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
      <TR>
        <td width=49% valign=top>
          <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 background="images/graymidbottom.gif"><TBODY>
            <TR>
              <TD VAlign=top width=21 background=images/darkgraybg.gif>
                <IMG height=21 src="images/darkgrayleft.gif" width=21>
              </TD>
              <TD align=middle background=images/darkgraybg.gif>
                <STRONG><FONT color=#ffffff>Activate/Deactivate Registrations</FONT></STRONG>
              </TD>
              <TD VAlign=top width=19 background=images/darkgraybg.gif>
                <IMG height=21 src="images/darkgrayright.gif" width=19>
              </TD>
            </TR>
          </tbody></table>
        </td>
        <td width=2% rowspan=2></td>
        <td valign=middle width=49%>
          <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 background="images/graymidbottom.gif"><TBODY>
            <TR>
              <TD VAlign=top width=21 background=images/darkgraybg.gif>
                <IMG height=21 src="images/darkgrayleft.gif" width=21>
              </TD>
              <TD align=middle background=images/darkgraybg.gif>
                <STRONG><FONT color=#ffffff>Miscellaneous Items</FONT></STRONG>
              </TD>
              <TD VAlign=top width=19 background=images/darkgraybg.gif>
                <IMG height=21 src="images/darkgrayright.gif" width=19>
              </TD>
            </TR>
          </tbody></table>
        </td>
      </tr>
      <TR>
        <td valign=top width=50%>
          <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 background="images/graymidbottom.gif"><TBODY>
            <tr><td height=5>&nbsp;</td</tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=Academy&prgaction=on">Activate Morrisville Academy</a></td>
              <td width=180>Morrisville Academy Reg On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=Centers&prgaction=on">Activate Morrisville Centers</a></td>
              <td width=160>Morrisville Center Reg On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>

            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=RalWFAcademy&prgaction=on">Activate Ral/WF Academy</a></td>
              <td width=160>Ral/WF Academy Reg On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=RalWFCenters&prgaction=on">Activate Ral/WF Centers</a></td>
              <td width=160>Ral/WF Center Reg On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>


            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=Tryouts&prgaction=on">Activate Morrisville Tryout</a></td>
              <td width=160>Tryout Registration On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=TryoutsRal&prgaction=on">Activate Raleigh Tryout</a></td>
              <td width=160>Tryout Registration On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>

            <TR>
              <td width=200>&nbsp; <a href="activatefun.php4?&prgaction=on">Activate Functional</a></td>
              <td width=160>Function Registration On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>

            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=Camps&prgaction=on">Activate Camps</a></td>
              <td width=160>Camps Registration On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=Play&prgaction=on">Activate Summer Play</a></td>
              <td width=160>Summer Play Registration On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=YGK&prgaction=on">Activate Young Goalkeepers</a></td>
              <td width=160>Young Goalkeeper Registration On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=OGK&prgaction=on">Activate Older Goalkeepers</a></td>
              <td width=160>Older Goalkeeper Registration On</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="activategrads.php4?&prgaction=on">Activate Grad Program</a></td>
              <td width=160>Grad Registration On</td>
            </tr>

            <tr><td height=35 colspan=2 align=center valign=middle>&nbsp;<img src=images/line.jpg width=250></td></tr>
           <TR>
              <td width=210>&nbsp; <a href="regsactivate.php4?&prgtype=Academy&prgaction=off">Deactivate Morrisville Academy</a></td>
              <td width=180>Morrisville Academy Reg Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=Centers&prgaction=off">Deactivate Morrisville Centers</a></td>
              <td width=160>Morrisville Center Reg Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>

           <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=RalWFAcademy&prgaction=off">Deactivate Ral/WF Academy</a></td>
              <td width=160>Ral/WF Academy Reg Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=RalWFCenters&prgaction=off">Deactivate Ral/WF Centers</a></td>
              <td width=160>Ral/WF Center Reg Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>


            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=Tryouts&prgaction=off">Deactivate Morrisville Tryout</a></td>
              <td width=160>Tryout Registration Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=TryoutsRal&prgaction=off">Deactivate Raleigh Tryout</a></td>
              <td width=160>Tryout Registration Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>

            <TR>
              <td width=200>&nbsp; <a href="activatefun.php4?&prgaction=off">Deactivate Functional</a></td>
              <td width=160>Function Registration Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>

            <tr><td height=10>&nbsp;</td></tr>

            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=Camps&prgaction=off">Deactivate Camps</a></td>
              <td width=160>Camps Registration Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>

            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=Play&prgaction=off">Deactivate Summer Play</a></td>
              <td width=160>Summer Play Registration Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=YGK&prgaction=off">Deactivate Young Goalkeeper</a></td>
              <td width=160>Young Goalkeeper Registration Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="regsactivate.php4?&prgtype=OGK&prgaction=off">Deactivate Older Goalkeepers</a></td>
              <td width=160>Older Goalkeeper Registration Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td width=200>&nbsp; <a href="activategrads.php4?&prgaction=off">Deactivate Grad Program</a></td>
              <td width=160>Grad Registration Off</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>



            <TR>
              <TD colspan=2>
                <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                  <TR>
                    <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                    <TD background=images/graymidbottom.gif>&nbsp;</TD>
                    <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                  </TR>
                </tbody></table>
              </TD>
            </TR>
          </tbody></table>
        </td>

        <td valign=top width=50%>
          <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 background="images/graymidbottom.gif"><TBODY>
            <tr><td height=5>&nbsp;</td</tr>
            <TR>
              <td width=160>&nbsp; &nbsp &nbsp; &nbsp; <a href="editcoaches.php4">Edit Coach Bios</a></td>
              <td width=160>Edit/Delete coach bios</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td>&nbsp; &nbsp &nbsp; &nbsp; <a href="addcoaches.shtml">Add Coach Bios</a></td>
              <td>Enter a new coach bio</td>
            </tr>
            <tr><td height=10>&nbsp;</td</tr>
            <TR>
              <td>&nbsp; &nbsp; &nbsp; &nbsp; <a href="editnews.php4">Edit News Items</a></td>
              <td>Edit/Delete News Items</td>
            </tr>
            <tr><td height=10>&nbsp;</td</tr>
            <TR>
              <td>&nbsp; &nbsp; &nbsp; &nbsp; <a href="addnews.shtml">Add a News Item</a></td>
              <td>Create a new "News' Items</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td>&nbsp; &nbsp; &nbsp; &nbsp; <a href="editmarquee.php4">Edit Marquee</a></td>
              <td>Change the scrolling marquee</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td>&nbsp; &nbsp; &nbsp; &nbsp; <a href="calendarInput.php4">Input Calendar</a></td>
              <td>Input new calendar data</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td>&nbsp; &nbsp; &nbsp; &nbsp; <a href="editCalMenu.shtml">Edit Calendar</a></td>
              <td>Change calendar Data</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>
            <TR>
              <td>&nbsp; &nbsp; &nbsp; &nbsp; <a href="managest.shtml">Specialty Training</a></td>
              <td>Manage Specialty Training</td>
            </tr>
            <tr><td height=10>&nbsp;</td></tr>

            <TR>
              <TD colspan=2>
                <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                  <TR>
                    <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                    <TD background=images/graymidbottom.gif>&nbsp;</TD>
                    <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                  </TR>
                </tbody></table>
              </TD>
            </TR>
          </tbody></table>
        </td>

      </TR>
    </tbody></table>
  </td>        
</TR>
