<?php
	/**
	 * @package Wordpress
	 * @subpackage pixbit-nla
	 */
get_header(); ?>

<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="sections-wrapper">


		<?php if(get_field('sections')): ?>
			<?php $section_counter = 0; ?>
			<?php while(has_sub_field('sections')): ?>
				<?php include(locate_template( 'acf/section-'.get_row_layout().'.php' )); ?>
			<?php $section_counter++; ?>
			<?php endwhile; ?>
		<?php endif; ?>

    <section class="content-block dark-texture-4">
      <div class="container page-contents">
          <div class='content-block-section row'>
            <div class="span12">
							<?php while ( have_posts() ) : the_post(); ?>

								<?php //get_template_part( 'partials/content', 'page' ); ?>

									<?php the_content(); ?>

							<?php endwhile; ?>
            </div>
          </div><!-- .content-block-section .row-->
      </div><!-- .container .page-contents -->
    </section><!-- .dark-texture-4 -->

	</div><!-- .sections-wrapper -->
	<?php //get_template_part( 'parts/projects', 'carousel' ); ?>
</article>

<?php get_footer(); ?>
