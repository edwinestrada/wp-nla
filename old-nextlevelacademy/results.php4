<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                <TD align=middle background=images/darkgraybg.gif>

<?
  $prgtype =chop( $_POST["prgtype"]);
  if ( $prgtype == "Team" )
    print("<STRONG><FONT color=#ffffff>Create Team Tryout Results</FONT></STRONG>");
  else
    if ( $prgtype == "Suppteam" )
      print("<STRONG><FONT color=#ffffff>Create Team Supplemental Tryout Results</FONT></STRONG>");
    else 
      if ( $prgtype == "Academy" )
        print("<STRONG><FONT color=#ffffff>Create Academy Results</FONT></STRONG>");
      else 
        print("<STRONG><FONT color=#ffffff>Create Academy Supplemental Results</FONT></STRONG>");
?>
                </TD>
                <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
              </TR>
            </tbody></table>
          </td>
        </TR>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <tr><td height=5>&nbsp;</td></tr>

<?

  $ageGroupList = array ( 
      'Select One',
      'U7',
      'U8',
      'U9',
      'U10',
      'U11' );
  $ageGroupItems = count ( $ageGroupList);

  $ageGroup =chop( $_POST["agegroup"]);
  $gender =chop( $_POST["gender"]);

  $player=($HTTP_POST_VARS["player"]);
  extract($player);
  for ($i=0; $i<count($player); $i++)  { 
    $player_txt .=$player[$i]; 
    if ($i<(count($player)-1))  {
      $player_txt .= ', '; 
    } 
  } 
// now have a comma separated string with the player numbers in $player_txt

  $index = 0;
  while ( $index <  $ageGroupItems ) {
    if ( $ageGroup == $ageGroupList[$index] )  {    // item found
      $optionindex =  $index;
      $index = $ageGroupItems;
    }
    $index++;
  }  // while $index

  if ( $optionindex == 0 ) {
    print ( "<tr><td>&nbsp; </td>\n<td width=\"560\" align=\"left\" valign=\"top\">\n");
    print ("<blockquote>You need to select an age group.  Go back and try again</blockquote>");
    print ("</td></tr>");

  }
  else {

    if ( $index ==  $ageGroupItems )   {   
      print ( "<tr><td>&nbsp; </td>\n<td width=\"560\" align=\"left\" valign=\"top\">\n");
      print ("<blockquote>Software bug - try and remember exactly what you entered on the previous page and call Jack.</blockquote>");
      print ("</td></tr>");
    }  // if $index == $generalItems
    else {  
      if ( $prgtype == "Team" )
        $teamFile = "data/Tryouts/".$ageGroup.$gender."team.txt";
      else
        if ( $prgtype == "Suppteam" )
          $teamFile = "data/Tryouts/".$gender."supteam.txt";
        else 
          if ( $prgtype == "Academy" )
            $teamFile = "data/Tryouts/".$ageGroup.$gender."academy.txt";
          else 
            $teamFile = "data/Tryouts/".$gender."supacademy.txt";

      $fh = fopen($teamFile, "w");
      if ( fwrite($fh, ($player_txt."\n")) ) {
          print ( "<tr><td>&nbsp;</td><td width=\"560\" align=\"left\" valign=\"top\">\n");
          print("<blockquote>Tryout results successfully created.</blockquote>");
          print ("</td></tr>");
      }
      else {
        print ( "<tr><td>&nbsp </td>\n<td width=\"560\" align=\"left\" valign=\"top\">\n");
        print ("<blockquote>Error attempting to create file: ".$ageGroup.",".$gender."<br>Please go back and
                 try again. </blockquote>");
        print ("</td></tr>");
      }
      fclose($fh);


    }
  }
?>
              <tr><td height=5>&nbsp;</td></tr>
            </tbody></table>
          </td>
        </tr>

        <TR>
          <TD>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                <TD background=images/graymidbottom.gif>&nbsp;</TD>
                <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>       
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>