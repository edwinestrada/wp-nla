<?php if(is_front_page()): ?>
    <!-- Contact Us Starts
      ================================================== -->
    <section id="contact" class="dark-texture-2">
      <div class="container page-contents">
      <!-- Form Starts -->
      <div class="row">
        <!-- Form Starts -->
        <div class="form-element">
          <div class="span12 center">
            <h3 class="subheading">CONTACT NLA</h3>
<!--             <h4>
              Registration for tryouts for our Academy program is now open. For Morrisville Tryouts click on Morrisville Academy. For Raleigh/Wake Forest tryouts click on Raleigh/WF Academy.
            </h4>
            <h4>
              Registration for Summer Play is now open!
            </h4>
 -->            <div class="done">
              <h3>Thank you ! We have received your message.</h3>
            </div>
          </div>
          <div class="form">
            <form method="post" action="">
              <div class="span6">
                <input type="text" name="name" class="text" placeholder="Name *" style="width:100%"/>
              </div>
              <div class="span6"><input type="text" name="email" class="text"  placeholder="Email *" style="width:100%"/></div>
              <div class="span12 pull-center">
                <textarea name="comment" class="text textarea" placeholder="Message *" style="width:100%; height:100px;"></textarea>
                <input type="submit" class="btn btn-info btn-large" value="SUBMIT" id="submit"/>
              </div>
            </form>
          </div>
        </div>
        <!-- Form Ends-->
      </div>
      <!-- Form Ends -->
      <!-- Address Starts -->
      <div class="address">
        <div class="row">
          <div class="span3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/contact/street.png" class="pull-left">
            <h3>STREET ADDRESS</h3>
            <h4>Main Office</h4>
            <p>Next Level Academy<br>
              3717 Davis Drive<br>
              Morrisville, NC 27560
            </p>
            <br>
            <h4>Morrisville Site</h4>
            <p>1839 S Main St<br>
              Wake Forest, NC 27587
            </p>
          </div>
          <div class="span3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/contact/address.png" class="pull-left">
            <h3>EMAIL ADDRESS</h3>
            <p>staff@nextlevelacademy.com
            </p>
          </div>
          <div class="span3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/contact/phone.png" class="pull-left">
            <h3>TELEPHONE</h3>
            <p>Phone: (919) 467-2299
            </p>
          </div>
          <div class="span3">
            <h3>MAP LOCATION</h3>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3234.725175918448!2d-78.85427800000001!3d35.831218!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89acede265b71109%3A0xd53cfc03a9ad9854!2sNext+Level+Academy!5e0!3m2!1sen!2sus!4v1396953750103" width="250" height="250" frameborder="0" style="border:0"></iframe>
          </div>
          <!-- Address Ends -->
          <hr>
          <!-- Copyright Start -->
          <div class="row pull-center">
            <div class="span12 copyright"> © 2014 Next Level Academy. All Rights Reserved. </div>
          </div>
          <!-- Copyright Ends -->
        </div>
      </div>
    </section>
<?php endif; ?>
    <!-- Contact Us Ends
      ================================================== -->
    <!-- ToTop Starts
      ================================================== -->
    <a href="#" class="scrollup">Scroll</a>
    <!-- ToTop Ends
      ================================================== -->
    <?php wp_footer(); ?>
  </body>
</html>
