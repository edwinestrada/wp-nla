<?php
/**
 * Next Level parent theme functions
 */

/*Including other theme components*/
require_once locate_template('/inc/scripts.php'); // Scripts and stylesheets
require_once locate_template('/inc/shortcodes.php'); //This will create the shortcodes
require_once locate_template('/inc/widgets.php'); //This will create the shortcodes

////////////////////////
// TURN OFF ADMIN BAR //
////////////////////////
// add_filter('show_admin_bar', '__return_false');

//////////////
// 0. DEBUG //
//////////////

// add_action( 'wp_print_styles', 'print_queue_scripts' );
function print_queue_scripts(){
  global $wp_scripts;

  echo "<br/>";
  echo "SCRIPTS:";
  echo "<br/>";
  foreach( $wp_scripts->queue as $handle ) :
    echo $handle . ' | ';
  endforeach;
}

////////////////////
// 1. Admin Menus //
////////////////////

/**
 * Hides the text/visual content editor.
 * Can base it on Post ID, User roles, or template name.
 */
function hide_editor() {
  // Get the Post ID.
  // $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  // if( !isset( $post_id ) ) return;

  // Get the name of the Page Template file.
  // $template_file = get_post_meta($post_id, '_wp_page_template', true);
  // if($template_file == 'contact.php'){ // edit the template name
  //   remove_post_type_support('page', 'editor');
  // }

  remove_post_type_support('page', 'editor');
}
// add_action( 'admin_init', 'hide_editor' );


////////////////////////////
// Advanced Custom Fields //
////////////////////////////

/**
 * Sets up Custom Post Types to be used by ACF
 */
function my_acf_options_page_settings($settings){
  $settings['title'] = 'My Options';
  $settings['pages'] = array(
     'Section: Header'
    ,'Section: About'
    ,'Section: Footer'
    ,'Social Media'
    );

  if(!class_exists('CPT'))
  require_once('inc/cpt-lib/CPT.php');

  /**
   * @var CPT(PLURAL_NAME, SINGULAR_NAME, SLUG)
   */
  // $FAQs = new CPT('FAQs', 'FAQ', 'faq');
  // $FAQs->setIcon('redPin');
  // $FAQs->setTaxonomies(array('category'));
  // $FAQs->load();

  // $Facilities = new CPT('Facilities', 'Facility', 'facility');
  // $Facilities->setIcon('redPin');
  // $Facilities->setTaxonomies(array('category'));
  // $Facilities->setSupport(array( 'title', 'editor', 'thumbnail', 'page-attributes' ));
  // $Facilities->load();

  // $Coaches = new CPT('Coaches', 'Coach', 'coach');
  // $Coaches->setIcon('redPin');
  // $Coaches->setTaxonomies(array('category'));
  // $Coaches->setSupport(array( 'title', 'editor', 'thumbnail', 'page-attributes' ));
  // $Coaches->load();

  $Players = new CPT('Players', 'Player', 'player');
  $Players->setIcon('people');
  $Players->setTaxonomies(array('category'));
  $Players->setSupport(array( 'title', 'editor', 'thumbnail', 'page-attributes' ));
  $Players->load();

  // $Values = new CPT('Values', 'Value', 'value');
  // $Values->setIcon('redPin');
  // $Values->setTaxonomies(array('category'));
  // $Values->setSupport(array( 'title', 'editor', 'thumbnail', 'page-attributes' ));
  // $Values->load();

  // $Services = new CPT('Services', 'Service', 'service');
  // $Services->setIcon('redPin');
  // $Services->setTaxonomies(array('category'));
  // $Services->setSupport(array( 'title', 'editor', 'thumbnail', 'page-attributes' ));
  // $Services->load();

  add_theme_support('post-thumbnails');

  return $settings;
}
add_filter('acf/options_page/settings', 'my_acf_options_page_settings');

/////////////////////////////////
// 2. Register Nav Menu //
/////////////////////////////////

function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' => 'Main Menu'
      ,'footer-menu' => 'Footer Menu'
    )
  );
}
add_action( 'init', 'register_my_menus' );


/**
 * Class Name: wp_bootstrap_navwalker
 * GitHub URI: https://github.com/twittem/wp-bootstrap-navwalker
 * Description: A custom WordPress nav walker class to implement the Twitter Bootstrap 2.3.2 navigation style in a custom theme using the WordPress built in menu manager.
 * Version: 1.4.5
 * Author: Edward McIntyre - @twittem
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

class Nla_walker_nav_menu extends Walker_Nav_Menu {

	/**
	 * @see Walker::start_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\" dropdown-menu\">\n";
	}

	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		/**
		 * Dividers & Headers
	     * ==================
		 * Determine whether the item is a Divider, Header, or regular menu item.
		 * To prevent errors we use the strcasecmp() function to so a comparison
		 * that is not case sensitive. The strcasecmp() function returns a 0 if
		 * the strings are equal.
		 */
		if (strcasecmp($item->title, 'divider') == 0) {
			// Item is a Divider
			$output .= $indent . '<li class="divider">';
		} else if (strcasecmp($item->title, 'divider-vertical') == 0) {
			// Item is a Vertical Divider
			$output .= $indent . '<li class="divider-vertical">';
		} else if (strcasecmp($item->title, 'nav-header') == 0) {
			// Item is a Header
			$output .= $indent . '<li class="nav-header">' . esc_attr( $item->attr_title );
		} else {

			$class_names = $value = '';

			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;

			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

			//If item has_children add dropdown class to li
			if($args->has_children) {
				$class_names .= ' dropdown';
			}

			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

			$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
			$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

			$output .= $indent . '<li' . $id . $value . $class_names .'>';

			$atts = array();
			$atts['title']  = ! empty( $item->title ) 	   ? $item->title 	   : '';
			$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
			$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';

			//If item has_children add atts to a
			if($args->has_children) {
				// $atts['href']   		= '#';
        $atts['href'] = ! empty( $item->url ) ? $item->url : '';
				$atts['class']			= 'dropdown-toggle';
				$atts['data-toggle'] = 'dropdown';
				$atts['data-target'] = '#';
			} else {
				$atts['href'] = ! empty( $item->url ) ? $item->url : '';
			}

			$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

			$attributes = '';
			foreach ( $atts as $attr => $value ) {
				if ( ! empty( $value ) ) {
					$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
					$attributes .= ' ' . $attr . '="' . $value . '"';
				}
			}

			$item_output = $args->before;

			/*
			 * Glyphicons
			 * ===========
			 * Since the the menu item is NOT a Divider or Header we check the see
			 * if there is a value in the attr_title property. If the attr_title
			 * property is NOT null we apply it as the class name for the glyphicon.
			 */

			if(! empty( $item->attr_title )){
				$item_output .= '<a'. $attributes .'><i class="' . esc_attr( $item->attr_title ) . '"></i>&nbsp;';
			} else {
				$item_output .= '<a'. $attributes .'>';
			}

			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= ($args->has_children) ? ' <i class="fa fa-angle-down"></i></span></a>' : '</a>';
			$item_output .= $args->after;

			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	}

	/**
	 * Traverse elements to create list from elements.
	 *
	 * Display one element if the element doesn't have any children otherwise,
	 * display the element and its children. Will only traverse up to the max
	 * depth and no ignore elements under that depth.
	 *
	 * This method shouldn't be called directly, use the walk() method instead.
	 *
	 * @see Walker::start_el()
	 * @since 2.5.0
	 *
	 * @param object $element Data object
	 * @param array $children_elements List of elements to continue traversing.
	 * @param int $max_depth Max depth to traverse.
	 * @param int $depth Depth of current element.
	 * @param array $args
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return null Null on failure with no changes to parameters.
	 */

	function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
        if ( !$element ) {
            return;
        }

        $id_field = $this->db_fields['id'];

        //display this element
        if ( is_object( $args[0] ) ) {
           $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }

        parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }
}
