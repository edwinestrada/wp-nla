<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                <TD align=middle background=images/darkgraybg.gif>
<?
  $prgtype= $_GET['prgtype'];
  $prgday = $_GET['prgday'];
  $coach = $_POST['coach'];

  $maxITplayers = 4;
  $maxGTplayers = 4;
  $maxGKplayers = 6;
  $maxFBplayers = 40;

  switch ( $prgtype )
  {
    case "U7-1":
      print("<STRONG><FONT color=#ffffff>U7-U10 Specialty Training Registration</FONT></STRONG>");
      $signupFile = "data/stdata/Rec".$prgday.".txt";
      $maxplayers = $maxITplayers;
      break;

    case "U7-2":
      print("<STRONG><FONT color=#ffffff>U7-U10 Specialty Training Registration</FONT></STRONG>");
      $signupFile = "data/stdata/Rec".$prgday.".txt";
      $maxplayers = $maxGTplayers;
      break;

    case "U11-1":
      print("<STRONG><FONT color=#ffffff>U11-U14 Specialty Training Registration</FONT></STRONG>");
      $signupFile = "data/stdata/Cla".$prgday.".txt";
      $maxplayers = $maxITplayers;
      break;

    case "U11-2":
      print("<STRONG><FONT color=#ffffff>U11-U14 Specialty Training Registration</FONT></STRONG>");
      $signupFile = "data/stdata/Cla".$prgday.".txt";
      $maxplayers = $maxGTplayers;
      break;

    case "GK":
      print("<STRONG><FONT color=#ffffff>Goalkeeper Training Registration</FONT></STRONG>");
      $signupFile = "data/stdata/GK".$prgday.".txt";
      $maxplayers = $maxGKplayers;
      break;

    case "FB":
      print("<STRONG><FONT color=#ffffff>Finishing/Ball Striking Registration</FONT></STRONG>");
      $signupFile = "data/stdata/FB".$prgday.".txt";
      $prgage = $_GET['age'];
      $maxplayers = $maxFBplayers;
      break;

  }

?>
                </TD>
                <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
              </TR>
            </tbody></table>
          </td>
        </TR>


<?
  $totalplayers = 0;
  $totalGKplayers = 0;

  if ( file_exists($signupFile) ) {
    $fh = fopen($signupFile, "r");
    while ( $playerInfo= fgets($fh, 512) ) {
       switch ( $prgtype ) {
         case "U7-1":
         case "U11-1":
           $parms = explode("^",$playerInfo);
           if ( chop($parms[1]) == "1" )
             $totalplayers++;

         break;

         case "U7-2":
         case "U11-2":
           $parms = explode("^",$playerInfo);
           if ( chop($parms[1]) == "2" )
             $totalplayers++;

         break;

         case "GK":
           $totalplayers++;
         break;

         case "FB":
           $totalplayers++;
           $parms = explode("^",$playerInfo);
           if ( chop($parms[1]) == "GK" )
             $totalGKplayers++;

         break;

       }
    }

    fclose($fh);
  }

  if ( $totalplayers >= $maxplayers ) {
    print("<tr><td> We are sorry but the last open slot for this session has just been filled.<br>");
    print("Please go back and try another session or a different day.</td></tr>");
  }
  else {
    print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"75%\" border=0><TBODY>\n");
    print("<tr><td width=125 height=10>&nbsp;<form method=\"post\" action=\"processSTreg.php4\" </td></tr>\n");

    print("<TR><TD width=\"30%\" align=left>Player First Name</TD>\n");
    print("<TD width=\"70%\" align=left><INPUT type=\"text\" style=\"font-size: 14px;\" name=\"firstname\" size=\"15\" maxlength=\"20\"></TD></tr>\n");
    print("<tr><TD align=left>Player Last Name</TD>\n");
    print("<TD align=left><INPUT type=\"text\" style=\"font-size: 14px;\" name=\"lastname\" size=\"15\" maxlength=\"30\"></TD>\n");
    print("</TR>\n");
    print("<tr><td height=5>&nbsp;</td></tr>\n");
    print("<tr><td>Age</td>\n");
    print("<td><select style= \"font-size: 12px;\" name=\"age\">\n");
    print("<option selected>Select\n");
    if ( $prgtype == "FB" ) {
      if ( $prgage == "younger" ) {
        print("<option>7\n");
        print("<option>8\n");
        print("<option>9\n");
        print("<option>10\n");
      }
      else {
        print("<option>11\n");
        print("<option>12\n");
        print("<option>13\n");
        print("<option>14\n");
      }
    }
    else {
      switch ( $prgtype ) {      
        case "GK":
          print("<option>7\n");
          print("<option>8\n");
          print("<option>9\n");
          print("<option>10\n");

        case "U11-1":
        case "U11-2":
          print("<option>11\n");
          print("<option>12\n");
          print("<option>13\n");
          print("<option>14\n");
          break;
        case "U7-1":
        case "U7-2":
          print("<option>7\n");
          print("<option>8\n");
          print("<option>9\n");
          print("<option>10\n");
          break;
      }
      
    }
    print("</select></td></tr>\n");
    print("<tr><td height=5>&nbsp;</td></tr>\n");

    if ( ($prgtype != "GK") && ($prgtype != "FB")  ) {
      print("<tr><td>Focus Area</td>\n");
      print("<td><select style= \"font-size: 12px;\" name=\"focus\">\n");
      print("<option selected>Select One\n");
      print("<option>1st Touch\n");
      print("<option>Dribbling/Moves\n");
      print("<option>Coerver/Fast-Footwork\n");
      print("<option>Passing\n");
      print("<option>Ball Striking\n");
      print("<option>Shielding\n");
      print("<option>Heading\n");
      print("<option>Agility\n");
      print("<option>Juggling/Tricks\n");
      print("</select></td></tr>\n");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
    }
    
    if ( $prgtype == "FB" ) {
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      print("<TR><TD align=left>Field Player or GoalKeeper</TD>\n");
      print("<td><select style= \"font-size: 12px;\" name=\"position\">\n");
      print("<option selected>Select One\n");
      print("<option>Field Player\n");
      print("<option>GoalKeeper\n");
      print("</select></td></tr>\n");

      print("<tr><td height=5>&nbsp;</td></tr>\n");
      print("<TR><TD align=left>ST Session Registered For (Month-Day)</TD>\n");
      print("<TD align=left><INPUT type=\"text\" style=\"font-size: 14px;\" name=\"stdate\" size=\"15\"></TD>\n");
      print("</TR>\n");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
    }

    print("<TR><TD align=left>Phone</TD>\n");
    print("<TD align=left><INPUT type=\"text\" style=\"font-size: 14px;\" name=\"phone\" size=\"10\" maxlength=\"20\"></TD>\n");
    print("</TR>\n");
    print("<tr><td height=5>&nbsp;</td></tr>\n");
    print("<TR><TD align=left>Current E-Mail Address</TD>\n");
    print("<TD align=left><INPUT type=\"text\" style=\"font-size: 14px;\" name=\"email\" size=\"35\" maxlength=\"50\"></TD>\n");
    print("</TR>\n");

    if ( $prgtype != "GK" ) {
      print("<tr><td height=5>&nbsp;</td></tr>\n");
      print("<tr><td>NLA progam currently enrolled in</td>\n");
      print("<td><select style= \"font-size: 12px;\" name=\"nlaprogram\">\n");
      print("<option selected>Select One\n");
      print("<option>Morrisville Academy\n");
      print("<option>Raleigh / WF Academy\n");
      print("<option>Development Centers\n");
      print("<option>Functionals\n");
      print("<option>Graduate Program\n");
      print("</select></td></tr>\n");
    }
    print("<tr><td height=5>&nbsp;</td></tr>\n");

    print("<tr><td><INPUT type=\"hidden\" name=\"program\" value=\"".$prgtype."\"></td></tr>\n");
    print("<tr><td><INPUT type=\"hidden\" name=\"day\" value=\"".$prgday."\"></td></tr>\n");
    print("<tr><td><INPUT type=\"hidden\" name=\"coach\" value=\"".$coach."\">\n");
    print("<TR><TD align=right><INPUT type=\"submit\" style=\"font-size: 12px;\" value=\"submit\"></TD>\n");
    print("<TD align=center><INPUT type=\"reset\" style=\"font-size: 12px;\" value=\"clear\"></TD>\n</TR>");
    print("<tr><td height=10>&nbsp;</form></td></tr>\n");
    print("</tbody></table></td></tr>\n");

  }
  
?>
        <TR>
          <TD>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                <TD background=images/graymidbottom.gif>&nbsp;</TD>
                <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>       
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>