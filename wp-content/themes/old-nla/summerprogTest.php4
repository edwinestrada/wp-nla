<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>

              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Morrisville Academy</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <p align="center"><STRONG> <font style="font-size: 20px; color: #ff0000;"><u>SUMMER "PLAY" 2013</u></font></strong></p>
                        <p>Next Level Academy is excited to announce our summer program emphasizing 
                        <strong>Playing and Tactical Awareness</strong>. Our Summer Play Program is a Futsal program which is played 
					indoors on a hard surface and the ball and rules create an emphasis on improvisation, creativity, and 
					technique as well as ball control and passing in small spaces.  </p>
                        <p>This program is open to rising U9-U13 boys and girls (current U8-U12 age groups) and will be offered 
					during the month of June and the exact playing days are listed below.  Girls enrolled in this program will 
					attend on Mondays from 5:00- 7:30pm and boys enrolled in this program will attend on Wednesdays from 
					5:00 � 7:30pm.  </p>

				<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
				<td width=5% rowspan=6>&nbsp;</td>
				<td width=30%>&nbsp;</td>
				<td width=35%><b><u>Summer Play Dates</u></b></td>
				<td width=30%>&nbsp;</td>
                    </TR>
                    <TR>
				<td><b><u>Girls</u></b> (all ages):</td>
				<td></td>
				<td><b><u>Boys</u></b> (all ages):</td>
                    </TR>
                    <TR>
				<td>Monday June 3</td>
				<td></td>
				<td>Wednesday June 5</td>
                    </TR>
				<td>Monday June 10</td>
				<td></td>
				<td>Wednesday June 12</td>
                    </TR>
				<td>Monday June 17</td>
				<td></td>
				<td>Wednesday June 19</td>
                    </TR>
				<td>Monday June 24</td>
				<td></td>
				<td>Wednesday June 26</td>
                    </TR>

                  </TBODY></TABLE>

                       <p> All ages will attend on the same night (girls on Mondays, boys on Wednesdays) but will be broken out 
				into appropriate groups.  Right now we see these groups being divided as U8/U9, U10, and U11/U12 for the 
				girls.  For the boys we anticipate these groups being divided as U8/U9, U9/U10, U10/U11, and U11/U12.  
				<b>Please note:</b> that these are only our estimates and are subject to modification based on enrollment 
				numbers, skill level of players, etc.  A child may also be asked to play in a different age group by the 
				coaches if they feel it�s necessary for that child�s development.  </p>

				<p>Due to how Futsal is played and the size of the space it�s played in, each age group will be capped at 
				30 players.  Once that number is reached for each group, players registering will be put on a wait list 
				and notified.</p>

				<p>The cost of our Summer Play program is $200.  Girls must attend on Monday nights and boys must attend 
				on Wednesday nights.  No exceptions will be made and if a player misses, they just miss � we do not offer 
				makeup days.  </p>

				<p>All trainings will be held indoors at NetSports (our Morrisville location) and the address is 3717 Davis Drive, 
				Morrisville, NC  27560.</p>
				
				<p>The first week of Summer Play will be done on the Soccer Fields at NetSports.  All weeks after the first one 
				will take place on the Sport Court surface at NetSports</p>
				<p>Players should wear indoor soccer shoes or turf shoes.   They should not wear outdoor cleats.  </p>

				<p>All players must 
                        complete a <a href = "docs/NextLevelAcademyMedicalWaiver.doc">
                        NLA waiver form</a> to participate in the Sumemr Play program. 
				</p>

				<p>Please mail or drop off your completed waiver form and payment to the Next Level office 
				located at:<br>&nbsp;  Next Level Academy
				<br>&nbsp; 3717 Davis Drive 
				<br>&nbsp; Morrisville, NC  27560.  
				<br>&nbsp; <br>If you have any questions you can 
				contact us at 919-467-2299 or email us at staff@nextlevelacademy.com.</p>


                        <p>Next Level Academy is very excited about our Summer Play program and the opportunity to work with players in a learning 
                        environment focusing on game situations. &nbsp;

<?
    $prgFile = "data/Playregactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<b>The deadline to register for Summer Play is May 20, 2013.</b>.  &nbsp;If you're interested in registering for this program, ");
        print("<a href= \"summerplayform.php4\">click here</a> ");
        print("to complete the online registration process. Once you've completed that, please mail payment and ");
        print("the <a href= \"docs/NextLevelAcademyMedicalWaiver.doc\">waiver form</a>  to the address below. </p>");
        print("<p>Next Level Academy <br>Attn: Summer Play <br>3717 Davis Drive<br> Morrisville, NC 27560</p>");
      }
      else 
        print("<br><b>Summer Play registration is currently not open.</b>");
    }
    else 
      print("<br><b>Summer Play registration is currently not open.</b>");                    
?>

 

                      </TD>
                    </TR>
                    <TR><TD height=10></TD></TR>
                  </TBODY></TABLE>
                </TD>
              </TR>


              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>