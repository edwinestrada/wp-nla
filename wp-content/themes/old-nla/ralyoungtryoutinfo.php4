<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>
              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Raleigh/WF Academy U7 thru U11 Tryout Information</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                   <TR>
                      <TD valign="top" align=center colspan=4>
                        <p> The tryout schedule for the U7, U8, U9, U10, and U11 age groups is as follows:
                      </TD>
                    </TR>

                    <tr>
                      <td colspan=4 align=center><strong>Monday Aprli 28, 2014</strong></td>
                    </tr>
                    <tr >
                      <td width="125" rowspan=17>&nbsp;</td>
                      <td align=right>Rising U9 girls</td>
                      <td >5:00 - 6:00pm</td>
                      <td width="125" rowspan=15>&nbsp;</td>
                    </tr>
                    <tr >
                      <td align=right>Rising U9 boys </td>
                      <td>6:00 - 7:00pm</td>
                    </tr>
			  <tr><td height=10>&nbsp; </td></tr>

                    <tr>
                      <td colspan=2 align=center><strong>Tuesday April 29, 2014</strong></td>
                    </tr>
                    <tr bgcolor=#ffffe0>
                      <td align=right>Rising U10 girls</td>
                      <td >5:00 - 6:00pm</td>
                    </tr>
                    <tr bgcolor=#ffffe0>
                      <td align=right>Rising U10 boys </td>
                      <td>6:00 - 7:00pm</td>
                    </tr>
			  <tr><td height=10>&nbsp; </td></tr>

                    <tr>
                      <td colspan=2 align=center><strong>Wednesday April 30, 2014</strong></td>
                    </tr>
                    <tr bgcolor=#ffffe0>
                      <td align=right>Rising U11 girls</td>
                      <td >5:00 - 6:00pm</td>
                    </tr>
                    <tr bgcolor=#ffffe0>
                      <td align=right>Rising U11 boys </td>
                      <td>6:00 - 7:00pm</td>
                    </tr>
			  <tr><td height=10>&nbsp; </td></tr>

                    <tr>
                      <td colspan=2 align=center><strong>Thursday May 1, 2014</strong></td>
                    </tr>
                    <tr >
                      <td align=right>Rising U7 girls</td>
                      <td >5:00 - 6:00pm</td>
                    </tr>
                    <tr >
                      <td align=right>Rising U7 boys </td>
                      <td>5:00 - 6:00pm</td>
                    </tr>
                    <tr bgcolor=#ffffe0>
                      <td align=right>Rising U8 girls</td>
                      <td >6:00 - 7:00pm</td>
                    </tr>
                    <tr bgcolor=#ffffe0>
                      <td align=right>Rising U8 boys </td>
                      <td>6:00 - 7:00pm</td>
                    </tr>

                   <tr><td height=10></td></tr>
                   <TR>
                      <TD valign="top" colspan=4 align=center>
                        <p> NLA Raleigh/WF Academy results will be posted 
			on the Next Level Academy website on Tuesday, May 8, 2014 by 8:00pm.</p>
<p><strong>The site you select to tryout at is the 
site you are selecting to participate at (i.e. if you tryout out at Raleigh, then your 
player will be evaluated for the program at Raleigh).</strong></p>
<?
    $prgFile = "data/Tryouts/RegactiveRal.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" )
        print("<p align=center>The deadline to register is Sunday, April 20 2014. &nbsp; <a href=tryoutformRal.php4><font style=\"color:#0000ff;\">Click here</font></a> to register on-line.</p>");
      else 
        print("<p align=center>Online registration for the Raleigh/WF Academy Tryouts is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the Raleigh/WF Academy Tryouts is not open. <br>");

    print("<p align=left> If you fail to register on-line you may still attend tryouts as a walk-up registrant.");  
    print(" The tryout fee for walk up players is $30. If you fall into this category, please ");
	print("arrive 20 minutes prior to the start time for your child's tryout so that you have time"); 
	print(" to complete the onsite registration paperwork.</p>");

                       
?>


                      </TD>
                    </TR>
                    <tr>
                     <td colspan=4>
                       If you are not able to attend the tryout dates and times listed above there 
                       will be an alternate tryout date and time which will require approval from the 
                       NLA staff to be able to attend. To be considered to attend the alternate tryout 
			you must register online by Sunday, April 20, 2014 and send an email to 
			staff@nextlevelacademy.com as described below.

                       <p align=center><strong>Alternate Tryouts</strong><br></p>
                     </td>
                   </tr>
                   <tr>
                     <td colspan=4>
                       Friday, May 2, 2014 at The Factory<br>
                       5:00 - 6:00pm: Rising U7 & U8 Boys and Girls<br>
                       6:00 - 7:00pm: Rising U9, U10 & U11 Boys and Girls<br>			    
                       
                       <p>The players should wear dark shorts, white 
                       non-soccer affiliated tshirt, indoor shoes, have a size 4 ball with their name on it and water with 
                       their name on it.</p>  
                       <p>You must still complete the online registration process to register your child for tryouts. Once you've done 
				that then send an e-mail to staff@nextlevelacademy.com with the following information and we will 
                       send you a follow-up email approving your attendance at the alternate tryout:<br>
                       &nbsp; &nbsp; Player's name<br>
                       &nbsp; &nbsp; Players date of birth<br>
                       &nbsp; &nbsp; Gender<br>
                       &nbsp; &nbsp; Age Group<br>
                       &nbsp; &nbsp; Parents Name<br>
                       &nbsp; &nbsp; Home Phone<br> 
                       &nbsp; &nbsp; Email address</p>
                       Please bring your acknowledgement email with you to the tryout.
				<P>Results for all tryouts will be posted on the NLA web site 
                        on Tuesday May 8, 2014 by 8:00 pm.
                     </td>
                   </tr>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>