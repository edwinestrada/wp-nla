<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>


  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Tryout Results</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top" colspan=4>
	                Thank you for trying out this weekend.  The talent was at a high level but unfortunately 
                        not all players can be selected for the Next Level Team.  However those players not selected 
                        for the team are selected to join the Next Level Centers.  Over the past year and a half 
                        many players began with the Academies/Centers and were promoted to the Next Level Teams. 
	                <p>
	                Congratulations to those players listed below on making the Next Level Academy Team! 
	                <b>Please either accept or decline your player's position on the Next Level Academy Team by 
	                clicking on the link in the appropriate age group box</b>. Provide the information requested 
	                when accepting or declining so we can ensure you are the correct player and we understand your 
	                intentions for the upcoming Next Level Academy year. If the information you provide does not 
	                match the information we received at tryouts we will contact you.  All players we do not hear 
	                from accepting or declining the position by <font color="#ff0000">Wednesday, June 9, 2004</font> 
	                will be contacted by the Next Level Academy Staff.</p>
	                <p>

	                If your number is not listed please check the <a href="academysupresults.php4">
	                <font color="#0000ff">Academies supplemental tryout results.</font></a></p>  
	                <p>
	                Thanks again for trying out and we wish each of you much success on and off the soccer field.
                      </td>
                    </TR>
                    <tr><td height=5>&nbsp;</td></tr>
<?
  $tryoutFile = "data/Tryouts/supresults.txt";
  
  if ( file_exists($tryoutFile) ) 
  { 
    $fh = fopen ($tryoutFile,"r");
    $active = fread($fh, 80);
    fclose($fh);
    if ($active == "active" )
    {
      $outputColumn = 0;
      $playerNumbers = array();
      $gender = "Male";
      $groupindex = 1;
      while (  $groupindex < 2 ) {
        if ( $gender == "Male" ) {
          $ageGroupFile = "data/Tryouts/Malesupteam.txt";
          $headerString = "Male";
        }
        else {
          $ageGroupFile =  "data/Tryouts/Femalesupteam.txt";
          $headerString = "Female";
        }

        if ( file_exists($ageGroupFile) ) {
          $fhTeam = fopen($ageGroupFile, "r");
          $index = 0;
          $totalPlayers = 0;
          while (  !feof($fhTeam) ) {
            $userInfo = fgets($fhTeam, 800);
            $parms = explode (",", $userInfo);
            $parmCount = count($parms);
            while ( $index < $parmCount ) {
              $playerNumbers[$index] = chop($parms[$index]); 
              $index++; 
            }
          }  // while userInfo = fgetcsv
          fclose($fhTeam);
          $totalPlayers = $index;

          if ( $outputColumn == 0 ) {   // need to start a new row
            print("<tr><td height=10>&nbsp;</td></tr>");
            print("<tr><td width=100>&nbsp;</td>");
            $needEndTR = 1;
          }

          print("<td width=325 valign=top><table width=325 height =200  border=1 cellpading=0 cellspacing=0 bgcolor=#ffff90><tbody><tr>");
          print("<td colspan=10 align=center valign=top height =30>");
          print("<font style = \" font-size: 14px; color: #0000ff;\"><strong>".$headerString."</strong></font></td></tr><tr>");
 
          $index = 0;
          $tdindex = 0;
          while ( $totalPlayers >  0 ) {
            print("<td width=40 align=center height =20>");
            print("<font style = \"font-size: 12px; color: #000000\">".$playerNumbers[$index]."</font></td>");
            $index++;
            $tdindex++;
            if ( $tdindex >= 10 ) {
              print("</tr><tr>");
              $tdindex = 0;
            }
            $totalPlayers--;
          }  // while totalPlayers > 0 

          if ($tdindex != 0) {
            while ( $tdindex < 10 ) {
              print("<td>&nbsp</td>");
              $tdindex++;
            }
          }

          print("</tr><tr><td colspan=10 align=center><font style = \"font-size: 12px;\">");
          print("<a href=\"acceptdecline.php4?&source=Team\">Click here to accept or decline position.</a>");  
          print("</tr><tr><td colspan=\"10\">&nbsp;</td></tr></tbody></table></td>");
          print("<td width=10>&nbsp;</td>");
        
          $outputColumn++;
          if ( $outputColumn >= 2 ) {
            print("</tr>");
            $outputColumn = 0;
            $needEndTR = 0;
          } // if outputColumn >= 2

        }   // file_exists(ageGroupFile)

        if ( $gender == "Male" ) {
          $gender = "Female";
        }
        else {
          $groupindex++;
          $gender = "Male";
        }
      }  // end while fgetcsv

      if ( $needEndTR == 1 )
        print("</tr>");
    }
    else {
      print("<tr><td align=center><strong>Tryout results have not been posted yet.</strong></td></tr>");
    }
  }
  else {
    print("<tr><td align=center><strong>Tryout results have not been posted yet.</strong></td></tr>");
  }


?>
                 

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>
