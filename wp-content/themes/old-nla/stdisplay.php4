<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                <TD align=middle background=images/darkgraybg.gif>
                   <STRONG><FONT color=#ffffff>Session Display</FONT></STRONG>
                </TD>
                <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
              </TR>
            </tbody></table>
          </td>
        </TR>


<?
  $numberofprograms = 4;
  $sday = chop($_POST['day']);
  $syear = chop($_POST['year']);
  switch ( chop($_POST['month']) ) {
    case 'January': 
     $cmonth = 1;
      break;
     case 'February': 
      $cmonth = 2;
      break;              
     case 'March':  
     $cmonth = 3;
      break;
     case 'April': 
     $cmonth = 4;
      break;
     case 'May': 
     $cmonth = 5;
      break;                 
     case 'June': 
     $cmonth = 6;
      break;                 
     case 'July': 
     $cmonth = 7;
      break;                 
     case 'August': 
     $cmonth = 8;
      break;                 
     case 'September': 
     $cmonth = 9;
      break;                 
     case 'October': 
     $cmonth = 10;
      break;                 
     case 'November': 
     $cmonth = 11;
      break;                 
     case 'December': 
      $cmonth = 12;
      break;                
  }


  $jdate = gregoriantojd($cmonth, $sday, $syear);
  $sfile[0] = "data/stdata/Rec".$jdate.".txt"; 
  $sheader[0] = "U7 - U10 Players";
  $sfile[1] = "data/stdata/Cla".$jdate.".txt"; 
  $sheader[1] = "U11 - U14 Players";
  $sfile[2] = "data/stdata/GK".$jdate.".txt"; 
  $sheader[2] = "Goalkeepers";
  $sfile[3] = "data/stdata/FB".$jdate.".txt"; 
  $sheader[3] = "Finishing / Ball Striking";



$prgday = $jdate;
$dayofWeek = jddayofweek($prgday, 1); 
// read in default coaches assignments.
// array is indexed by day and type ....  coach[Monday][U7-1] = name

  $fh = fopen("data/stdata/scoaches.txt", "r");
  while ( $coachInfo= fgets($fh, 512) ) {
    $parms = explode (",", $coachInfo, 2);
    switch ( chop($parms[0]) ) {
      case "Monday":
      case "Tuesday":
      case "Wednesday":
      case "Thursday":
      case "Friday":
      case "Saturday":
      case "Sunday":
        $dayindex = chop($parms[0]);
      break;

      case "end";
      break;

      default:
        $sessindex = chop($parms[0]);
        $cname = chop($parms[1]);
        $coach[$dayindex][$sessindex] = $cname;
      break;

    }
  }
  fclose($fh);

// check to see if default assignment is overridden
  $ncIndex = 0;
  $coachassignment = "data/stdata/c".$prgday.".txt";
  if ( file_exists($coachassignment) ) {
    $fh = fopen($coachassignment, "r");
    while ( $coachInfo= fgets($fh, 512) ) {
      $parms = explode (",", $coachInfo, 2);
      $cname = chop($parms[0]);
      $stype = chop ($parms[1]);
      if ( $stype == "None" )
        $noCoach[$ncIndex++] = $cname;
      else 
        $coach[$dayofWeek][$stype] = $cname;
    }
  }



  print("<tr><td align=center><table cellSpacing=0 cellPadding=0 width=\"75%\" border=0><TBODY>\n");
 
  $sindex = 0;
  while ( $sindex < $numberofprograms ) {
    if ( file_exists($sfile[$sindex]) ) {
//      print("<tr><td width=30%>".$sheader[$sindex]."</td><td> <select style= \"font-size: 12px;\"name=\"Player".$sindex."\">\n");
//      print("<option selected>Select One\n");


   switch ( $sindex ) {
     case 0:
       for ($cp = 0; $cp < 2; $cp++ ) {
         if ( $cp == 0 ) 
           $cpindex = "U7-1";
         else
           $cpindex = "U7-2";
         $printcoach[$cp] = $coach[$dayofWeek][$cpindex];
         if ( $printcoach[$cp]  == "None" )
           $printcoach[$cp]  = "TBD";
         else {
           for ( $i=0; $i < $ncIndex; $i++ ) {
             if ( $printcoach[$cp]  == $noCoach[$i] )
               $printcoach[$cp]  = "TBD";
           }
         }
       }
       $coachString = $printcoach[0]." / ".$printcoach[1];
       break;

     case 1:
       for ($cp = 0; $cp < 2; $cp++ ) {
         if ( $cp == 0 ) 
           $cpindex = "U11-1";
         else
           $cpindex = "U11-2";
         $printcoach[$cp] = $coach[$dayofWeek][$cpindex];
         if ( $printcoach[$cp]  == "None" )
           $printcoach[$cp]  = "TBD";
         else {
           for ( $i=0; $i < $ncIndex; $i++ ) {
             if ( $printcoach[$cp]  == $noCoach[$i] )
               $printcoach[$cp]  = "TBD";
           }
         }
       }
       $coachString = $printcoach[0]." / ".$printcoach[1];
       break;

     case 2:
       $printcoach = $coach[$dayofWeek]['GK'];
       if ( $printcoach == "None" )
         $printcoach = "TBD";
       else {
         for ( $i=0; $i < $ncIndex; $i++ ) {
           if ( $printcoach == $noCoach[$i] )
             $printcoach = "TBD";
         }
       }
       $coachString = $printcoach;
       break;

     case 3:
       $printcoach = $coach[$dayofWeek]['Finish'];
       if ( $printcoach == "None" )
         $printcoach = "TBD";
       else {
         for ( $i=0; $i < $ncIndex; $i++ ) {
           if ( $printcoach == $noCoach[$i] )
             $printcoach = "TBD";
         }
       }
       $coachString = $printcoach;
       break;

   }

print("<tr><td width=40% valign=top>".$sheader[$sindex]."<br>Coaches: ".$coachString."</td><td>\n");

      $fh = fopen($sfile[$sindex], "r");
      while ( $player = fgets($fh, 80) ) {
        switch ( $sindex ) {

          case 0:             // Rec/Challenge and clasic have form   name^type
          case 1:
            $parms = explode("^", $player, 2);
            $name = chop($parms[0]);
            $type = chop($parms[1]);

          break;

          case 2:             // Goalkeeper and finishing have form   name
            $type = " ";
            $name = chop($player);
          break;

          case 3:
            $parms = explode("^", $player, 2);
            $name = chop($parms[0]);
            $type = chop($parms[1]);
            if ( $type == "GK" )
              $type = " &nbsp; <b>GoalKeeper</b>";
            else
              $type = " &nbsp; Field Player";

            $name = $name.$type;
          break;
        }

        print(" ".$name."<br>");
      }
      print("</td></tr>\n");
      print("<tr><td height=5>&nbsp;</td></tr>\n");
    }

    $sindex++;
  }

  print("<tr><td height=10>&nbsp;</form></td></tr>\n");
  print("</tbody></table></td></tr>\n");
  
  
?>
        <TR>
          <TD>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                <TD background=images/graymidbottom.gif>&nbsp;</TD>
                <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>       
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>