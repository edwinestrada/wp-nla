Themes and Plugins
-----------------
This is a collection of Wordpress `Plugins` and `Themes` to allow for reuse via GIT without having
to worry about core files being updated and breaking stuff.
