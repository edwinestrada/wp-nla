<?php

/**
 * 
 * I control what template is used.
 * @version $Id$
 * @copyright 2003 
 **/

class rhc_template_frontend {
	var $is_taxonomy = false;
	var $theme_meta_fields=array();
	var $template_page = false;
	var $taxonomy_template_page_id = false;
	var $term_permalink = false;

	function rhc_template_frontend(){
		global $rhc_plugin;
		if( '1'!=$rhc_plugin->get_option('template_archive')){
			add_filter('archive_template', array(&$this,'archive_template'));	
		}
		if( '1'!=$rhc_plugin->get_option('template_single')){
			//add_filter('single_template', array(&$this,'single_template'),1,1);
			add_filter('page_template', array(&$this,'page_template'),1,1);
		}
		if( '1'!=$rhc_plugin->get_option('template_taxonomy')){
			add_filter('taxonomy_template', array(&$this,'taxonomy_template'));	
			//---
			//bug fix, taxonomies returning the permalink to the template and not the taxonomy due to how template is implemented.
			add_filter( 'page_link', array(&$this,'taxonomy_get_permalink'), 10, 3);
			add_filter( 'pre_get_shortlink', array(&$this,'taxonomy_get_shortlink'), 10, 4);
			add_filter( 'get_the_excerpt', array(&$this,'taxonomy_get_the_excerpt'), 10, 1);						
		}
		
		add_filter( 'query_vars', array(&$this,'query_vars') );

		//theme integration:
		$theme_meta_fields = array();
		//-- append fields set in options
		$tmp = $rhc_plugin->get_option('rhc_theme_meta_fields','',true);
		if(!empty($tmp)){
			$arr = explode(',',str_replace(' ','',$tmp));
			if(is_array($arr)&&count($arr)){
				foreach($arr as $meta){
					if(!in_array($meta,$theme_meta_fields)){
						$theme_meta_fields[]=$meta;
					}
				}
			}
		}
		//--
		$theme_meta_fields = apply_filters('rhc_theme_meta_fields', $theme_meta_fields );
		if(!empty($theme_meta_fields)){
			$this->theme_meta_fields = $theme_meta_fields;
			add_filter('get_post_metadata', array(&$this,'theme_meta_fields'),10,4);
		}	
		//---------------------	
		add_action('wp',array(&$this,'handle_main_query'),10,1);

		
	}

	function taxonomy_get_the_excerpt( $str ){
		if( $this->taxonomy_template_page_id > 0 ){
			$str = term_description( $this->term_id, $this->taxonomy );			
		}
		return $str;
	}
	
	function taxonomy_get_shortlink( $shortlink, $id, $context, $allow_slugs ){
		if ( false !== $shortlink )
			return $shortlink;
	
		global $wp_query;
		$post_id = 0;
		if ( 'query' == $context && is_singular() ) {
			$post_id = $wp_query->get_queried_object_id();
			$post = get_post( $post_id );
		} elseif ( 'post' == $context ) {
			$post = get_post( $id );
			if ( ! empty( $post->ID ) )
				$post_id = $post->ID;
		}
	
		// Return p= link for all public post types.
		if ( $post_id > 0 && $post_id == $this->taxonomy_template_page_id ) {
			$shortlink = home_url( sprintf( '?%s=%s', $this->taxonomy, $this->term_slug ) );
		}
		return $shortlink;
	}
	
	function taxonomy_get_permalink( $link, $post_ID, $sample ){
		if( $post_ID == $this->taxonomy_template_page_id ){
			//return the taxonomy permalink, not the page template one.
			return $this->get_term_permalink( $this->term_permalink );	
		}
		return $link;
	}

	function get_term_permalink( $url ){
		if( $this->taxonomy_template_page_id ){		
			return apply_filters('rhc_term_permalink', $this->term_permalink, $url, $this);		
		}
		return $url;
	}
	
	function handle_main_query( $wp ){
		global $rhc_plugin,$wp_query,$wp_the_query,$rhc_template_page_id;
		$o = get_queried_object();
		if(is_single() && $o->post_type==RHC_EVENTS){
			//NOTE: WordPress only allows page post type to use page templates.
			//So this lines are a hack to allow events to be treated as pages and use theme page templates.
			$template_page_id = intval($rhc_plugin->get_option('event_template_page_id',0,true));
			if($template_page_id>0){
				$rhc_template_page_id = $template_page_id;
				$copy_fields = array(
				'ID',
				'post_author','post_date','post_date_gmt'
				,'post_content','post_title','post_excerpt','post_status'
				,'comment_status','ping_status','post_password','post_name','to_ping','pinged','post_modified','post_modified_gmt','post_content_filtered'
				,'post_parent','guid','menu_order'
				,'post_type'
				,'post_mime_type','comment_count','ancestors','filter');
				$copy_fields = apply_filters('rhc_single_template_copy_fields',$copy_fields);
				$values = array();
				foreach( $copy_fields as $field){
					$values[$field] = $o->$field;
				}			
			
				global $wp_filter;	
				if(isset($wp_filter['pre_get_posts'])){
					$bak = $wp_filter['pre_get_posts'];
					unset($wp_filter['pre_get_posts']);				
				}else{
					$bak = false;
				}			
			
				$wp_query = new WP_Query('page_id='.$template_page_id);
			
				$o = $wp_query->get_queried_object();	
				$wrap = $o->post_content;			
				//----- without this, template selection does not gets correctly done, always default.
				global $post;
				$post = $o;
				$post = is_object($post)?$post:(object)array();
				$post->rhc_template_id = $template_page_id;
				$post->post_status = 'publish';//force it as publish 
				//------------			
				$this->template_page = get_page_template();//fetch template before overwritting post.

				if(false!==$bak){
					$wp_filter['pre_get_posts'] = $bak;
				}
				
				foreach( $copy_fields as $field){
					$post->$field = $values[$field];
				}
				$post->post_content = $this->single_template_content($o->post_content,$wrap);		
				$wp_query->post = $post;		
				$wp_the_query = $wp_query;	
			}else{
				$wp_query->is_single = false;
				$wp_query->is_page = true;
			
				$o->post_content = $this->single_template_content($post->post_content);	
				$this->template_page = get_page_template();			
			}			
			//infocus theme fix: autop not applied: if(class_exists('Mysitemyway'))$o->post_content = apply_filters('the_content',$o->post_content);
		}
		return $wp;
	}
	
	function theme_meta_fields($value, $object_id, $meta_key, $single){
		global $post, $rhc_template_page_id;

		$rhc_template_id = is_object($post) && property_exists($post,'rhc_template_id') ? $post->rhc_template_id : 0 ;
		$rhc_template_id = 	$rhc_template_id==0 && isset($rhc_template_page_id) && $rhc_template_page_id>0 ? $rhc_template_page_id : $rhc_template_id ;		

		if($single==='mix'){
			return $value;
		}else if( $rhc_template_id > 0  && $object_id!=$rhc_template_id ){
			if( empty($meta_key)){
				$default = get_post_meta($rhc_template_id,'',$single);
				$default = is_array($default) ? $default : array();
				$current = array();
				//this is need to put the event meta data on top of the default meta data from the template.
				$current = get_post_meta($post->ID,'','mix');//hack, i couldnt figure out how to skip this block to avoid an infinite loop. 
				$current = is_array($current) ? $current : array();
				return array_replace( $default, $current );
			}else if( in_array($meta_key,$this->theme_meta_fields) ){
				return get_post_meta($post->rhc_template_id,$meta_key,$single);	
			}		
		}

		return $value;
	}
	
	function get_template_path(){
		global $rhc_plugin;
		return $rhc_plugin->get_template_path();
	}
	
	function query_vars($vars){
		array_push($vars,RHC_DISPLAY);
		return $vars;
	}
	
	function is_calendar(){
		global $rhc_plugin;
		return (get_query_var( RHC_DISPLAY )==$rhc_plugin->get_option('rhc-visualcalendar-slug',RHC_VISUAL_CALENDAR,true));
	}
	
	function archive_template($template){	
		if( $this->is_calendar() ){
			global $rhc_plugin,$wp_query,$post; 
			$template_page_id = intval($rhc_plugin->get_option('calendar_template_page_id',0,true));
			if($template_page_id>0){
				$wp_query = new WP_Query('page_id='.$template_page_id);
				$o = $wp_query->get_queried_object();				
				//----- without this, template selection does not gets correctly done, always default.
				$post = $o;
				//------------			
				$template = get_page_template();//fetch template before overwritting post.
				$o->post_content = $this->archive_template_content($o->post_content);	
			}else{
				$template = $this->query_template( $this->get_template_path().'archive-'.get_query_var( 'post_type' ).'-calendar.php' );			
			}			
		}	
		return $template;
	}
	
	function archive_template_content($content){
		$filename = $this->get_template_path().'content-calendar.php';
		ob_start();
		if(file_exists($filename)){
			include($filename);
		}else{
			echo '[calendarizeit]';
		}	
		$output = ob_get_contents();
		ob_end_clean();
		return $this->inject_content($content,$output);
	}
	
	function page_template($template){
		if(false!==$this->template_page){
			return $this->template_page;
		}
		return $template;
	}
	
	function single_template($template){
		if(false!==$this->template_page){
			return $this->template_page;
		}
		return $template;
	}
	/* this didnt work, because themes may test for is_page to add classes to body by way of body_class() wich
	// tests using is_page and is_page_template wich have no hook to alter their response. so we are stuck
	// with the original hack
	function single_template($template){	
		global $wp_query,$wp_the_query;
		$o = get_queried_object();
	
		if($o->post_type==RHC_EVENTS){
			global $rhc_plugin; 
			$template_page_id = intval($rhc_plugin->get_option('event_template_page_id',0,true));
			if($template_page_id>0){
				global $rhc_template_page_id;
				$rhc_template_page_id = $template_page_id;//post is getting overwritten by themes plugins, so lets just use a global.
				
				$template = $this->get_page_template( $template_page_id );
				global $post;

				$template_post = get_post( $template_page_id );

				//Apply any template wrapper
				$post->post_content = $this->single_template_content($post->post_content, $template_post->post_content);
				//add the template id to the main post object
				$post->rhc_template_id = $template_page_id;

			}else{
				global $wp_query;
				//-- this is to much hacking.  TODO, find an alternative.
				$wp_query->is_single = false;
				$wp_query->is_page = true;
			
				$o->post_content = $this->single_template_content($post->post_content);	
				$template = get_page_template();			
			}			
			//infocus theme fix: autop not applied: if(class_exists('Mysitemyway'))$o->post_content = apply_filters('the_content',$o->post_content);
		}
		return $template;
	}
	*/
	
	/*
	function single_template($template){	
		global $wp_query,$wp_the_query;
		$o = get_queried_object();
	
		if($o->post_type==RHC_EVENTS){
			global $rhc_plugin; 
			$template_page_id = intval($rhc_plugin->get_option('event_template_page_id',0,true));
			if($template_page_id>0){
				$copy_fields = array(
				'ID',
				'post_author','post_date','post_date_gmt'
				,'post_content','post_title','post_excerpt','post_status'
				,'comment_status','ping_status','post_password','post_name','to_ping','pinged','post_modified','post_modified_gmt','post_content_filtered'
				,'post_parent','guid','menu_order'
				,'post_type'
				,'post_mime_type','comment_count','ancestors','filter');
				$copy_fields = apply_filters('rhc_single_template_copy_fields',$copy_fields);
				$values = array();
				foreach( $copy_fields as $field){
					$values[$field] = $o->$field;
				}			
			
				global $wp_filter;	
				if(isset($wp_filter['pre_get_posts'])){
					$bak = $wp_filter['pre_get_posts'];
					unset($wp_filter['pre_get_posts']);				
				}else{
					$bak = false;
				}			
			
				$wp_query = new WP_Query('page_id='.$template_page_id);
			
				$o = $wp_query->get_queried_object();	
				$wrap = $o->post_content;			
				//----- without this, template selection does not gets correctly done, always default.
				global $post;
				$post = $o;
				$post = is_object($post)?$post:(object)array();
				$post->rhc_template_id = $template_page_id;
				$post->post_status = 'publish';//force it as publish 
				//------------			
				$template = get_page_template();//fetch template before overwritting post.

				if(false!==$bak){
					$wp_filter['pre_get_posts'] = $bak;
				}
				
				foreach( $copy_fields as $field){
					$post->$field = $values[$field];
				}
				$post->post_content = $this->single_template_content($o->post_content,$wrap);		
				$wp_query->post = $post;		
				$wp_the_query = $wp_query;	
			}else{
				global $wp_query;
				//-- this is to much hacking.  TODO, find an alternative.
				$wp_query->is_single = false;
				$wp_query->is_page = true;
			
				$o->post_content = $this->single_template_content($post->post_content);	
				$template = get_page_template();			
			}			
			//infocus theme fix: autop not applied: if(class_exists('Mysitemyway'))$o->post_content = apply_filters('the_content',$o->post_content);
		}
		return $template;
	}
	*/
	function single_template_content($content,$wrap=''){
		$filename = $this->get_template_path().'content-single-event.php';
		ob_start();
		if(file_exists($filename)){
			include($filename);
		}else{
?>
[featuredimage meta_key='enable_featuredimage' meta_value='1' default='1' custom='rhc_top_image']<?php echo $content ?>
[postinfo meta_key='enable_postinfo' meta_value='1' default='1' class="se-dbox"]
[postinfo meta_key='enable_venuebox' meta_value='1' default='1' id="venuebox" class="se-vbox"]
<?php		
		}	
		$output = ob_get_contents();
		ob_end_clean();
		return apply_filters('the_events_content', $this->inject_content($wrap, $output));
	}
		
	function taxonomy_template($template){	
		global $wp_query,$wp_the_query;

		$cat = $wp_query->get_queried_object();
		//handle a situation where plugins or themes takeover the main query object and does not return it to its original state.
		//causing this script to load the version 1 template instead of the version 2.
		if(!is_object($cat)||!property_exists($cat,'taxonomy')){
			$taxonomies = apply_filters('rhc-taxonomies',array());
			if( is_array($taxonomies) && count($taxonomies)>0 && is_array($wp_query->query) && count($wp_query->query)>0 ){
				foreach( $wp_query->query as $q_taxonomy => $q_term ){
					if( array_key_exists($q_taxonomy, $taxonomies) ){
						$o_term = get_term_by('slug', $q_term, $q_taxonomy);
						$cat = $o_term;
						break;
					}
				}
			}						
		}
		//---			
		$this->is_taxonomy = true;
		$term_id 	= $this->term_id = $cat->term_id;
		$name 		= $cat->name;
		$taxonomy	= $this->taxonomy = $cat->taxonomy;
		$this->term_slug = $cat->slug;
		$term_permalink = $this->term_permalink = get_term_link($cat, $taxonomy);
		$this->term = $cat;
		
		$template_page_id = $this->get_taxonomy_template_page_id($term_id,$taxonomy);
		if($template_page_id){
			global $wp_filter;	
			//---wpml fix part 1	
			$backup_filters = array();
			foreach(array('posts_join','posts_where','pre_get_posts') as $hook_name){
				if(isset($wp_filter[$hook_name])){
					$backup_filters[$hook_name] = $wp_filter[$hook_name];
					unset($wp_filter[$hook_name]);				
				}
			}
			//---	
					
			$wp_query = new WP_Query('page_id='.$template_page_id);
			$o = $wp_query->get_queried_object();
			//----- without this, template selection does not gets correctly done, always default.
			global $post;
			$post = $o;
			//------------
			$template = get_page_template();
			//---wpml fix part 2
			if(!empty($backup_filters)){
				foreach($backup_filters as $hook_name => $backup_filter){
					$wp_filter[$hook_name] = $backup_filter;				
				}
				unset($backup_filters);
			}
			//---
			$post_content = $this->get_taxonomy_content($term_id,$taxonomy,$o->post_content);
/*
			$post_content = str_replace("\n","",$post_content);//autop adds p tags
			$post_content = str_replace("\r","",$post_content);
			$post_content = str_replace("\t","",$post_content);
*/
			$o->guid = $term_permalink;
			$o->post_title = $name;
			$o->post_content = do_shortcode($post_content);
			$wp_query->post = $o;	
			$wp_the_query = $wp_query;
			
			do_action('rhc_taxonomy_template_page_id_set', $template_page_id, $this->term );
			
			return $template;		
		}
		return $this->_taxonomy_template($template);
	}

	function _taxonomy_template($template){	
		if( $this->is_calendar() ){
			$template = $this->query_template( $this->get_template_path().'taxonomy-calendar.php' );				
		}else{
			$map_original_name = array(
				RHC_VENUE 		=> 'venue',
				RHC_ORGANIZER	=> 'organizer',
				RHC_CALENDAR	=> 'calendar'
			);
			$o = get_queried_object();
			//handle a situation where plugins or themes takeover the main query object and does not return it to its original state.
			if(!is_object($o)||!property_exists($o,'taxonomy')){
				global $wp_query;
				$taxonomies = apply_filters('rhc-taxonomies',array());
				if( is_array($taxonomies) && count($taxonomies)>0 && is_array($wp_query->query) && count($wp_query->query)>0 ){
					foreach( $wp_query->query as $q_taxonomy => $q_term ){
						if( array_key_exists($q_taxonomy, $taxonomies) ){
							$o = (object)array(
								'taxonomy' => $q_taxonomy
							);
							break;
						}
					}
				}						
			}
			//---			
			$filename = sprintf('%staxonomy-%s.php',
				$this->get_template_path(),
				isset($map_original_name[$o->taxonomy])?$map_original_name[$o->taxonomy]:$o->taxonomy
			);

			if(file_exists( $filename )){
				return $filename;
			}else{
				$taxonomies = apply_filters('rhc-taxonomies',array());
				if(is_array($taxonomies) && count($taxonomies)>0 && array_key_exists($o->taxonomy,$taxonomies)){
					return sprintf('%staxonomy-%s.php',
						$this->get_template_path(),
						'custom'
					);					
				}	
			}
		}		

		return $template;
	}
	
	function get_taxonomy_template_page_id($term_id,$taxonomy){
		global $wpdb;
		//-------------------------
		if(false!==$this->taxonomy_template_page_id && $this->taxonomy_template_page_id > 0){
			return $this->taxonomy_template_page_id;
		}		
		//-------------------------
		$default_taxonomies = array(
			RHC_CALENDAR 	=> __('Calendar','rhc'),
			RHC_ORGANIZER	=> __('Organizer','rhc'),
			RHC_VENUE		=> __('Venues','rhc')
		);
		
		$taxonomies = apply_filters('rhc-taxonomies',$default_taxonomies);
		//-------------------------	
		$page_id = intval(get_term_meta($term_id,'template_page_id',true));
		if($page_id==0 && in_array($taxonomy, array_keys($taxonomies) )){
			global $rhc_plugin; 
			$page_id = intval($rhc_plugin->get_option( $taxonomy.'_template_page_id',0,true));
			if($page_id==0){
				$page_id = intval($rhc_plugin->get_option('taxonomy_template_page_id',0,true));
			}
		}

		if($page_id>0){
			if('page'==$wpdb->get_var("SELECT post_type FROM {$wpdb->posts} WHERE ID={$page_id}",0,0)){
				$this->taxonomy_template_page_id = $page_id;
				return $page_id;		
			}
		}
		
		return 0;
	}
	
	function get_taxonomy_content($_term_id,$_taxonomy,$wrap=''){
		global $term_id,$taxonomy;
		$term_id = $_term_id;
		$taxonomy = $_taxonomy;
		$term = get_term($term_id,$taxonomy);
		$content = get_term_meta($term_id,'content',true);
		$content = trim($content)==''?$term->description:$content;
		
		$website = get_term_meta($term_id,'website',true);
		$href = false===strpos($website,'://')?'http://'.$website:$website;
		ob_start();
		
		$filename1 = $this->get_template_path().'content-taxonomy-'.$taxonomy.'.php';
		$filename2 = $this->get_template_path().'content-taxonomy.php';
		if(file_exists($filename1)){
			include($filename1);
		}else if(file_exists($filename2)){
			include($filename2);
		}else{	
	?>
	<div class="venue-container custom-content-area">
		<div class="venue-top-info">
			<div class="venue-small-map"><?php the_venue_map() ?></div>
	        <div class="venue-name"><?php the_venue_title()?></div>
			<div class="venue-details-holder">
				<div class="venue-image-holder"><?php the_venue_image()?></div>
				<div class="venue-defails">
	            	<?php the_venue_detail( array('label'=>__('Address','rhc'),'field'=>'gaddress'))?>
					<?php the_venue_detail( array('label'=>__('Telephone','rhc'),'field'=>'phone'))?>
					<?php the_venue_detail( array('label'=>__('Email','rhc'),'field'=>'email'))?>
					<?php the_venue_detail( array('label'=>__('Website','rhc'),'field'=>'website'))?>
					<div class="venue-description"><?php the_venue_content();?></div>
				</div>
	           
			</div>
			<div class="clear"></div>
		</div>
		[calendarizeit defaultview="rhc_event"]
	</div>
	<?php
		}
		$output = ob_get_contents();
		ob_end_clean();
		return $this->inject_content($wrap,$output);
	}
	
	function query_template($filename){
		if(file_exists($filename)){
			return $filename;
		}else{
			$filename = $this->get_template_path().'calendar.php';
			if(file_exists( $filename )){
				return $filename;
			}else{
				return RHC_PATH.'templates/default/calendar.php';
			}
		}
	}
	
	function inject_content($wrap,$content){
		if(false!==strpos($wrap,'[CONTENT]')){
			$content = str_replace('[CONTENT]',$content,$wrap);
		}else{
			$content = $wrap.$content;
		}
		return $content;
	}
	
	function get_page_template( $id ) {//based on wp template.php, modified so it can take an id instead of using the main query, as we want to fetch the template of anohter page.
		//$id = get_queried_object_id();
		$template = get_page_template_slug( $id );
		$pagename = get_query_var('pagename');
	
		if ( ! $pagename && $id ) {
			// If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
			$post = get_post( $id );
			if ( $post )
				$pagename = $post->post_name;
		}
	
		$templates = array();
		if ( $template && 0 === validate_file( $template ) )
			$templates[] = $template;
		if ( $pagename )
			$templates[] = "page-$pagename.php";
		if ( $id )
			$templates[] = "page-$id.php";
		$templates[] = 'page.php';
	
		return get_query_template( 'page', $templates );
	}	
}
?>