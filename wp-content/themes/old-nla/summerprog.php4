<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>

              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Morrisville Academy</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <p align="center"><STRONG> <font style="font-size: 20px; color: #ff0000;"><u>SUMMER "PLAY" 2014</u></font></strong></p>
                        <p>Next Level Academy is pleased to announce our summer program which will emphasize agility and 
					playing this year. Our summer play program is a soccer program and will be played indoors on turf 
					allowing an emphasis to be placed on improvisation, creativity, technique, and agility as well as ball 
					control and passing. The first part of each session will be agility and then the groups will play for the 
					remaining time.</p>
				<P>Summer Play is open to rising U9 � U14 boys and girls (current U8 � U13 age groups). Players do 
				not have to be currently enrolled in an NLA program in order to participate in this program as it is 
				open to everyone.</P>

                        <p>This program will be offered during the month of June and players will attend both Monday and
					Wednesday nights. The exact dates are listed below.</p>

				<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
				<td width=5% rowspan=6>&nbsp;</td>
				<td width=30%>&nbsp;</td>
				<td width=35%><b><u>Summer Play Dates</u></b></td>
				<td width=30%>&nbsp;</td>
                    </TR>
                    <TR>
				<td><b><u>Mondays</u></b>:</td>
				<td></td>
				<td><b><u>Wednesdays</u></b>:</td>
                    </TR>
                    <TR>
				<td>June 2</td>
				<td></td>
				<td>June 4</td>
                    </TR>
				<td>June 9</td>
				<td></td>
				<td>June 11</td>
                    </TR>
				<td>June 16</td>
				<td></td>
				<td>June 18</td>
                    </TR>
				<td>June 23</td>
				<td></td>
				<td>June 25</td>
                    </TR>

                  </TBODY></TABLE>

                       <p> Players in the rising U9 � U11 age groups will attend from 5:00 � 6:15 and players in the rising U12 
				� U14 age groups will attend from 6:15 � 7:30 each night. <b>Please note:</b> that these are only our 
				estimates and are subject to modification based on enrollment numbers, skill level of players, etc. A 
				child may also be asked to play in a different age group by the coaches if they feel it�s necessary for 
				that child�s development. </p>

				<p>The cost of Summer Play is $200 and players attend both nights each week. All sessions will be held 
				indoors at our Morrisville location (inside of NetSports) and the address is 3717 Davis Drive.</p>

				<p>Players should wear indoor soccer shoes or turf shoes. They should not wear outdoor cleats.</p>				
				
				<p>All players must 
                        complete a <a href = "docs/NextLevelAcademyMedicalWaiver.doc">
                        NLA waiver form</a> to participate in the Summer Play program. 
				</p>

				<p>Please mail or drop off your completed waiver form and payment to the Next Level office 
				located at:<br>&nbsp;  Next Level Academy
				<br>&nbsp; 3717 Davis Drive 
				<br>&nbsp; Morrisville, NC  27560.  
				<br>&nbsp; <br>If you have any questions you can 
				contact us at 919-467-2299 or email us at staff@nextlevelacademy.com.</p>


                        <p>Next Level Academy is very excited about our Summer Play program and the opportunity to work
				with players in a learning environment focusing on game situations. &nbsp;

<?
    $prgFile = "data/Playregactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<b>The deadline to register for Summer Play is May 28, 2013.</b>.  &nbsp;If you're interested in registering for this program, ");
        print("<a href= \"summerplayform.php4\">click here</a> ");
        print("to complete the online registration process. Once you've completed that, please mail payment and ");
        print("the <a href= \"docs/NextLevelAcademyMedicalWaiver.doc\">waiver form</a>  to the address below. </p>");
        print("<p>Next Level Academy <br>Attn: Summer Play <br>3717 Davis Drive<br> Morrisville, NC 27560</p>");
      }
      else 
        print("<br><b>Summer Play registration is currently not open.</b>");
    }
    else 
      print("<br><b>Summer Play registration is currently not open.</b>");                    
?>

 

                      </TD>
                    </TR>
                    <TR><TD height=10></TD></TR>
                  </TBODY></TABLE>
                </TD>
              </TR>


              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>