<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                <TD align=middle background=images/darkgraybg.gif>
                  <STRONG><FONT color=#ffffff>Edit Calendar</FONT></STRONG>
                </TD>
                <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
              </TR>
            </tbody></table>
          </td>
        </TR>
          <TR>
            <td>
              <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 background="images/graymidbottom.gif"><TBODY>
                <tr><td height=10></td></tr>
<?
  $iprog = chop( $_POST["iprog"]) ;
  $imonth = chop( $_POST["imonth"]) ;
  $iyear = chop( $_POST["iyear"]) ;
  $updateDate =  chop( $_POST["updateDate"]) ;

  $iday = array();
  $age = array();
  $gender = array();
  $place = array();
  $itime = array();
  $iwhat = array();

  $iday[0] = chop( $_POST["iday0"]) ;
  $iday[1] = chop( $_POST["iday1"]) ;
  $iday[2] = chop( $_POST["iday2"]) ;
  $iday[3] = chop( $_POST["iday3"]) ;
  $iday[4] = chop( $_POST["iday4"]) ;

  $age[0] = chop( $_POST["age0"]) ;
  $age[1] = chop( $_POST["age1"]) ;
  $age[2] = chop( $_POST["age2"]) ;
  $age[3] = chop( $_POST["age3"]) ;
  $age[4] = chop( $_POST["age4"]) ;

  $gender[0] = chop( $_POST["gender0"]) ;
  $gender[1] = chop( $_POST["gender1"]) ;
  $gender[2] = chop( $_POST["gender2"]) ;
  $gender[3] = chop( $_POST["gender3"]) ;
  $gender[4] = chop( $_POST["gender4"]) ;

  $place[0] = chop( $_POST["place0"]) ;
  $place[1] = chop( $_POST["place1"]) ;
  $place[2] = chop( $_POST["place2"]) ;
  $place[3] = chop( $_POST["place3"]) ;
  $place[4] = chop( $_POST["place4"]) ;

  $itime[0] =  $_POST["itime0"] ;
  $itime[1] =  $_POST["itime1"] ;
  $itime[2] =  $_POST["itime2"] ;
  $itime[3] =  $_POST["itime3"] ;
  $itime[4] =  $_POST["itime4"] ;

  $iwhat[0] = $_POST["iwhat0"] ;
  $iwhat[1] = $_POST["iwhat1"] ;
  $iwhat[2] = $_POST["iwhat2"] ;
  $iwhat[3] = $_POST["iwhat3"] ;
  $iwhat[4] = $_POST["iwhat4"] ;

  for ( $index = 0; $index < 5; $index++) {
    if ( $iwhat[$index] == "Centers")
      $iwhat[$index] = "Cen";
    if ( $iwhat[$index] == "Functionals")
      $iwhat[$index] = "Func";
    if ( $iwhat[$index] == "Event")
      $iwhat[$index] = "Evt";
    if ( $iwhat[$index] == "Training")
      $iwhat[$index] = "Tr";
    if ( $iwhat[$index] == "Academies")
      $iwhat[$index] = "Acad";
  }

  $freeform[0] = $_POST["freeformat0"] ;
  $freeform[1] = $_POST["freeformat1"] ;
  $freeform[2] = $_POST["freeformat2"] ;
  $freeform[3] = $_POST["freeformat3"] ;
  $freeform[4] = $_POST["freeformat4"] ;

  if ( $iprog == "NLA Morrisville" )
    $progext = "";
  else if ( $iprog == "NLA Raleigh" )
    $progext = "nlar";
    else
      $progext = "func";

  $error = "none";
  $index = 0;
  while ( $index < 5 ) {
    if ( $gender[$index] == "Male" )
      $gender[$index] = 'M';
    else
      if ( $gender[$index] == "Female" )
        $gender[$index] = 'F';
      else
        $gender[$index] = 'M/F';

    // time or freeformat key to determine if an entry made

    if ( ($itime[$index] != "") || ($freeform[$index] != "") ) { 
      if ($freeform[$index] != "")
        $entryString = "freeform^".$freeform[$index]."\n";
      else   
        $entryString = $age[$index]."^".$gender[$index]."^".$iwhat[$index]."^".$place[$index]."^".$itime[$index]."\n";


      $calFile = "data/calendars/".$iyear."/".$imonth."/".$iday[$index].$progext.".txt";
      $fh = fopen( $calFile, "a");
      if ( !fwrite($fh, $entryString) )
        $error = "Error writing to calendar file: ".$calFile.".  Data = ".$entryString;
      fclose($fh);
      $cindex = $iday[$index];
    }

    $index++;
  }


  $calFile = "data/calendars/".$iyear."/".$imonth."/lastupdate".$progext.".txt";
  $fh = fopen( $calFile, "w");
  if ( !fwrite($fh, $updateDate) )
        $error = "Error writing to last update file: ".$calFile.".  Data = ".$updateDate;
  fclose($fh);
 
  if ($error == "none") {
    print("<tr><td>Calendar information successfully updated.<br></td></tr>");
  }
  else {
    print("<tr><td>Error processing data:  ".$error."<br></td></tr>");
  }

?>

              </tbody></table>
            </td>
          </TR>    

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>
