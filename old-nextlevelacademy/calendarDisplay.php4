<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                <TD align=middle background=images/darkgraybg.gif>
                  <strong><FONT color=#ffffff>Next Level Academy 
<?
 $prgtype= $_GET['prog'];
 switch ( $prgtype ) 
 {
   case 1: $progext = "";
           print(" Morrisville Academy & Dev Centers Calendar ");
           break;

   case 2: $progext = "nlar";
           print(" Raleigh/WF Academy, Dev Centers & Graduate Program");
           break;

   case 3: $progext = "func";
           print(" Morrisville Functionals & Graduate Program");
           break;

   default: $progext = "";
           break;

 }
?>
                  </FONT></strong>
                </TD>
                <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
              </TR>
            </tbody></table>
          </td>
        </TR>

<?


 $fh = fopen("data/calendars/months.txt", "r");
 $monthCount =fread($fh,80);
 if ( $monthCount == null )
   $monthCount = 4;

  // Get current Month in text format for display.  Get current month in numerical format so that actual calendar
  // can be dynamically built.  Current year for display.

  $currMonth = date("F");
  $monthNumber = date("n");
  $currYear = date("Y");


  
  for ( $index = 0; $index < 33; $index++ ) {
    for ( $index2 = 0; $index2 < 10; $index2++ )
      $display[$index][$index2] = '.';
    $dayItems[$index] = 0;
  }
  
  $monthIndex = 0;
  while ( $monthIndex < $monthCount )  {
              // data is in file path:  data/calendars/2003/January/1.txt
              // Open each "day" file and read contents, if any.  save for future output
             // Display data array is indexed [day][item] as there can be multiple items per day.
    switch ( $currMonth ) {
      case 'January': 
                 $nextMonth = "February";
                 break;
     case 'February': 
                 $nextMonth = "March";
                 break;
     case 'March': 
                 $nextMonth = "April";
                 break;
     case 'April': 
                 $nextMonth = "May";
                 break;
     case 'May': 
                 $nextMonth = "June";
                 break;
     case 'June': 
                 $nextMonth = "July";
                 break;
     case 'July': 
                 $nextMonth = "August";
                 break;
     case 'August': 
                 $nextMonth = "September";
                 break;
     case 'September': 
                 $nextMonth = "October";
                 break;
     case 'October': 
                 $nextMonth = "November";
                 break;
     case 'November': 
                 $nextMonth = "December";
                 break;
     case 'December': 
                 $nextMonth = "January";
                 break;
    }


    $calFile = "data/calendars/".$currYear."/".$currMonth;
    $timeFile = "data/calendars/".$currYear."/".$currMonth."/lastupdate".$progext.".txt";

    if ( file_exists($timeFile) ) {
      $fh = fopen( $timeFile, "r");
      $updateDate = fread($fh,80);
      fclose($fh);  
    }
    else
      $updateDate = "";

    $index = 1;
    $dayIndex = 0;
    while ( $index <  32 ) {

      $coachItem[$index] = "";
      if ( file_exists($calFile."/".$index.$progext.".txt") ) {
        $fh = fopen($calFile."/".$index.$progext.".txt", "r");
        while (  $calInfo = fgets($fh, 80) ) {

          $parms = explode ("^", $calInfo, 6);
          if ( $parms[0] != "freeform" )  {
   //         $display[$index][$dayIndex++] = $parms[0].' '.$parms[1].' '.$parms[2].' '.$parms[3].' '.$parms[4];
            $display[$index][$dayIndex++] = str_replace("^", " ", $calInfo);
          }
          else {
            $display[$index][$dayIndex++] = $parms[1];
          }

          $dayItems[$index]++;
        }
        fclose($fh);
        $dayIndex = 0;
      }

      $index++;
    }  // while $index

    print("<tr>");
    print("<td align=\"center\">");
    print("<table border=\"1\" cellpading=\"1\" cellspacing=\"1\" width=\"1050\" bgcolor=\"#f0f0f0\"><tbody>");
    print("<tr><td height=\"30\" colspan=\"7\" align=\"center\">");
    print("<font style=\"font-size: 20px; color: #000000;\">");
    if ( $updateDate != "" ) {
      print("&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp ");
      print("&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp ");
      print("&nbsp ".$currMonth."  ".$currYear."</font><font style=\"font-size: 12px; color: #000000;\">");
      print("&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Last Updated: ".$updateDate);
    } 
    else 
      print("&nbsp ".$currMonth."  ".$currYear."</font>");

    print("</td></tr>");
    print("<tr>");
    print("<td width=\"150\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Sunday</td>");
    print("<td width=\"150\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Monday</td>");
    print("<td width=\"150\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Tuesday</td>");
    print("<td width=\"150\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Wednesday</td>");
    print("<td width=\"150\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Thursday</td>");
    print("<td width=\"150\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Friday</td>");
    print("<td width=\"150\" align=\"center\"><font style=\"font-size: 10pt; color: #000000;\">Saturday</td>");
    print("</tr>");

   // Find out which day of the week is the first of the month so the calendar can be properly displayed.

    $jdDate = gregoriantojd($monthNumber, 1, $currYear);
    $dayofWeek = jddayofweek($jdDate, 0); 

  // create first row of calendar, print blanks until we get to the day that is the first of the month
    $dayIndex = 1;
    $colIndex = 0;

    print("<tr height=\"100\">");
    while ( $colIndex < 7 ) {
      if ( $colIndex >= $dayofWeek ) {
        print("<td  width=\"150\" align=\"right\" valign=\"bottom\">");
        print("<font style=\"font-size: 12px; color: #000000;\">"); 
        $dayItemIndex = 0;

        while ( $dayItems[$dayIndex] > 0 ) {
          print($display[$dayIndex][$dayItemIndex++]."<br>");
          $dayItems[$dayIndex]--;
        }
        print("</font><font style=\"font-size: 12px; color: #ff0000;\"><strong>".$dayIndex++."</strong></td>");
      }
      else
        print("<td align=\"right\" valign=\"bottom\"><font style=\"font-size: 12px; color: #000000;\">&nbsp;</td>");
      $colIndex++;
    }
    print("</tr>");

  // now loop through and finish the rest of the rows of the calendar.  May need to pad end of calendar
  // to keep a nice square look.   Need to determine the number of days in the given month - could be 28,
  // 29, 30, or 31.

    $index = 29;
    while ( $index < 33 ) {
      if ( !checkdate($monthNumber, $index, $currYear) ) {
        $loopMax = $index;
        $index = 32;
      }
      $index++;
    }

    while ( $dayIndex < $loopMax ) {
        $colIndex = 0; 
        print("<tr height=\"100\">");
        while ( ($colIndex < 7 ) && ($dayIndex < $loopMax) )  {
          print("<td  width=\"150\" align=\"right\" valign=\"bottom\">");
          print("<font style=\"font-size: 12px; color: #000000;\">"); 
          $dayItemIndex = 0;

          while ( $dayItems[$dayIndex] > 0 ) {
            print($display[$dayIndex][$dayItemIndex++]."<br>");
            $dayItems[$dayIndex]--;
          }
          print("</font><font style=\"font-size: 12px; color: #ff0000;\"><strong>".$dayIndex++."</strong></td>");
          $colIndex++;
        }
        if ( ($dayIndex == $loopMax) && ($colIndex < 7) ) {
           while ( $colIndex++ < 7 )
             print("<td align=\"right\" valign=\"bottom\"><font style=\"font-size: 12px;\">&nbsp;</td>");
        }
    }
    print("</tr></tbody></table></td></tr>");
    print("<tr><td height=\"25\"><font style=\"font-size: 12px;\">&nbsp;</td></tr>");
      
    if ( $monthNumber == 12 ) {
      $monthNumber = 1;
      $currYear++;
    }
    else
      $monthNumber++;

    $currMonth = $nextMonth;
    $monthIndex++;
  }
      
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>
