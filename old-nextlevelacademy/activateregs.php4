<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>

  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <td>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                <TD align=middle background=images/darkgraybg.gif>

<?
  $prgtype =chop( $_POST["prgtype"]) ;
  $prgaction =chop( $_POST["prgaction"]) ;
  $season =chop( $_POST["season"]) ;
  $programs = array ('Academy', 'Centers', 'Tryouts', 'TryoutsRal', 'RalWFAcademy', 'RalWFCenters', 'Camps', 'Play', 'YGK', 'OGK' );

  $programIndex = -1;

  for ( $i=0; $i < count($programs); $i++ )
  {
    if ( $prgtype == $programs[$i] )
    {
      $programIndex = $i;
      $i = count($programs);
    }
  }

  if ( $prgaction == "on" )
    $actionString = "Activate ";
  else
    $actionString = "Deactivate ";

  switch ( $programIndex )
  {
    case 0:
      print("<STRONG><FONT color=#ffffff>".$actionString."Academies Registration</FONT></STRONG>");
      break;

    case 1:
      print("<STRONG><FONT color=#ffffff>".$actionString."Centers Registration</FONT></STRONG>");
      break;

    case 2:
      print("<STRONG><FONT color=#ffffff>".$actionString."Morrisville Tryout Registration</FONT></STRONG>");
      break;

    case 3:
      print("<STRONG><FONT color=#ffffff>".$actionString."Raleigh Tryout Registration</FONT></STRONG>");
      break;

    case 4:
      print("<STRONG><FONT color=#ffffff>".$actionString."Ral/WF Academies Registration</FONT></STRONG>");
      break;

    case 5:
      print("<STRONG><FONT color=#ffffff>".$actionString."Ral/WF Centers Registration</FONT></STRONG>");
      break;

    case 6:
      print("<STRONG><FONT color=#ffffff>".$actionString."Camps Registration</FONT></STRONG>");
      break;

    case 7:
      print("<STRONG><FONT color=#ffffff>".$actionString."Summer Play Registration</FONT></STRONG>");
      break;

    case 8:
      print("<STRONG><FONT color=#ffffff>".$actionString."Young Goalkeeper Registration</FONT></STRONG>");
      break;

    case 9:
      print("<STRONG><FONT color=#ffffff>".$actionString."Older Goalkeeper Registration</FONT></STRONG>");
      break;

    default:
      print("<STRONG><FONT color=#ffffff>Uh-Oh !!!   BUG - call JACK ASAP !!!</FONT></STRONG>");
      break;

  }

?>
                </TD>
                <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
              </TR>
            </tbody></table>
          </td>
        </TR>
        <TR>
          <TD>
<?

  if ( $prgaction == "on" ) {
    $actionString = "open";
    $textString = " registrations have been activated.<br>&nbsp;</p>";
  }
  else {
    $actionString = "closed";
    $textString = " registrations have been deactivated.<br>&nbsp;</p>";
  }

  $err = 0;
  $seasonFile = "";
  $maleFile = "";
  $femaleFile = "";

   switch ( $programIndex )
  {
    case 0:
      $prgFile = "data/Academy/Regactive.txt";
      $seasonFile = "data/Academy/Regseason.txt";
      $maleFile = "data/Academy/Maleregistration.txt";
      $femaleFile = "data/Academy/Femaleregistration.txt";
      break;

    case 1:
      $prgFile = "data/Centers/Regactive.txt";
      $seasonFile = "data/Centers/Regseason.txt";
      $maleFile = "data/Centers/Maleregistration.txt";
      $femaleFile = "data/Centers/Femaleregistration.txt";
      break;

    case 2:
      $prgFile = "data/Tryouts/Regactive.txt";
      $maleFile = "data/Tryouts/Male.txt";
      $femaleFile = "data/Tryouts/Female.txt";
      break;

    case 3:
      $prgFile = "data/Tryouts/RegactiveRal.txt";
      $maleFile = "data/Tryouts/MaleRal.txt";
      $femaleFile = "data/Tryouts/FemaleRal.txt";
      break;

    case 4:
      $prgFile = "data/Academy/RalWFRegactive.txt";
      $seasonFile = "data/Academy/RalWFRegseason.txt";
      $maleFile = "data/RalWFAcademy/Maleregistration.txt";
      $femaleFile = "data/RalWFAcademy/Femaleregistration.txt";
      break;

    case 5:
      $prgFile = "data/Centers/RalWFRegactive.txt";
      $seasonFile = "data/Centers/RalWFRegseason.txt";
      $maleFile = "data/RalWFCenters/Maleregistration.txt";
      $femaleFile = "data/RalWFCenters/Femaleregistration.txt";
      break;

    case 6:
      $prgFile = "data/Campregactive.txt";
      break;

    case 7:
      $prgFile = "data/Playregactive.txt";
      break;

    case 8:
      $prgFile = "data/Goalies/YoungRegactive.txt";
      $seasonFile = "data/Goalies/YoungRegseason.txt";
      break;

    case 9:
      $prgFile = "data/Goalies/OldRegactive.txt";
      $seasonFile = "data/Goalies/OldRegseason.txt";
      break;

    default:
      print("<p><STRONG><FONT color=#ffffff>Uh-Oh !!!   BUG - call JACK</FONT></STRONG>");
      $err = 1;
      break;

  }

  if ( $err == 0 )
  {
    $fh = fopen($prgFile, "w");
    fputs($fh,$actionString);
    fclose($fh);
    print ("<p><br>".$programs[$programIndex].$textString);

    if ( $prgaction == "on" )
    {
      if ( file_exists($maleFile) ) {
        $fh = fopen($maleFile, "w");
        fclose($fh);
      }
      if ( file_exists($femaleFile) ) {
        $fh = fopen($femaleFile, "w");
        fclose($fh);
      }
      if ( $seasonFile != "" )
      {
        $fh = fopen($seasonFile, "w");
        fwrite($fh, $season);
        fclose($fh);
      }
    }

  }

?>
          </TD>
        </TR>

        <TR>
          <TD>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD VAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                <TD background=images/graymidbottom.gif>&nbsp;</TD>
                <TD VAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>       
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>