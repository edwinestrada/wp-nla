<?php
/**
 * ACF Section - Text Blocks Section
 *
 */
?>
<?php
  $texts = array();
  $type = get_sub_field('type');

  while(have_rows('text_blocks')): the_row();
    array_push($texts, get_sub_field('text_block'));
  endwhile;
?>

<?php

switch ($type) {
  case 'text-excerpt-horizontal':
    /////////////////////////////////////////
    // Image Left, Title & Caption Right   //
    ///////////////////////////////////////// ?>
    <section class="text-block dark-texture-4">
      <div class="container page-contents">
        <?php if(count($texts) > 0): ?>
          <?php while(have_rows('text_blocks')): the_row(); ?>
          <div class='text-block-section row'>
            <?php $text_block = get_sub_field('text_block'); ?>
            <div class="span5">
              <div class="picture">
                <img src="<?php echo $text_block['url']; ?>">
              </div>
            </div>
            <div class="span7">
              <h1><?php the_sub_field('title'); ?></h1>
              <ul class="nav allcaps">
              <?php
                $buttons = array();
                $buttons['titles'] = array();
                $buttons['links'] = array();
                while(have_rows('buttons')): the_row();
                  switch (get_sub_field('link_type')) {
                    case 'external':
                      $link = get_sub_field('link');
                      break;
                    
                    case 'internal':
                      $link = get_sub_field('page');
                      break;
                    
                    default:
                      $link = get_sub_field('link');
                      break;
                  }
                  array_push($buttons['titles'], get_sub_field('title'));
                  array_push($buttons['links'], $link);
                endwhile;
              ?>
              <?php if(count($buttons) > 0): ?>
                <?php $button_count = 0; ?>
                <?php while(have_rows('buttons')): the_row(); ?>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page">
                    <a href="<?php echo $buttons['links'][$button_count]; ?>"><?php echo $buttons['titles'][$button_count]; ?></a>
                  </li>
                <?php $button_count++; ?>
                <?php endwhile; ?>
              <?php endif; ?>
              </ul>
              <p><?php the_sub_field('caption'); ?></p>
            </div>
          </div><!-- .text-block-section .row-->
          <?php endwhile; ?>
        <?php endif; ?>
      </div><!-- .container .page-contents -->
    </section><!-- .dark-texture-4 -->

    <?php
    break;
  
  case 'text-wedge':
    /////////////////////////////////////////////////////
    // Image Top, Title & Subtitle Bottom inside Wedge //
    ///////////////////////////////////////////////////// ?>
    <section class="text-block dark-texture-4">
      <div class="container page-contents">
        <div class="row">
        <?php if(count($texts) > 0): ?>
          <?php while(have_rows('text_blocks')): the_row(); ?>
            <?php $text_block = get_sub_field('text_block'); ?>
            <div class="span6">
              <div class="picture">
                <img src="<?php echo $text_block['url']; ?>">
              </div>
              <div class="title">
                <div class="bg-blue2">
                  <div class="name">
                    <p><?php the_sub_field('title'); ?></p>
                    <span><?php the_sub_field('subtitle'); ?></span>
                  </div>
                </div>
                <div class="from_left_bot bot_box_left"></div>
              </div>
              <div class="team-social-icons pull-right">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/facebook_active.png">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/in_active.png">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/skype_active.png">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/image/social-icons/twitter02_active.png">
                </div>
              <div class="description"><p><?php echo get_the_excerpt(); ?></p></div>
            </div>
          <?php endwhile; ?>
        <?php endif; ?>
        </div><!-- .row  -->
      </div><!-- .container .page-contents -->
    </section><!-- .dark-texture-4 -->

    <?php
    break;
  
  case 'text-excerpt-vertical':
    ////////////////////////////////////////////////
    // Top Image, Centered Title & Caption Bottom //
    //////////////////////////////////////////////// ?>
    <section class="text-block black-bg">
      <div class="container page-contents">
        <div class="row">
        <?php if(count($texts) > 0): ?>
          <?php while(have_rows('text_blocks')): the_row(); ?>
            <?php $text_block = get_sub_field('text_block'); ?>
            <div class="span4 pull-center">
              <div class="round-icons">
                <img src="<?php echo $text_block['url']; ?>">
              </div>
              <h2><?php the_sub_field('title'); ?></h2>
              <p><?php the_sub_field('caption'); ?></p>
            </div><!-- .span4.pull-center -->
          <?php endwhile; ?>
        <?php endif; ?>
        </div><!-- .row  -->
      </div><!-- .container .page-contents -->
    </section><!-- .black-bg -->

    <?php
    break;

  case 'text-wedge-excerpt-below':
    ////////////////////////////////////////////////////
    // Title & Image Top inside Wedge, Caption Bottom //
    ////////////////////////////////////////////////////
    ?>
    <section id="services" class="black-bg text-wedge-excerpt-below">
      <div class="container page-contents">
        <div class="row">
        <?php if(count($texts) > 0): ?>
          <?php while(have_rows('text_blocks')): the_row(); ?>
            <?php $text_block = get_sub_field('text_block'); ?>
            <div class="span4">
              <div class="bg-blue2">
                <div class="titlebar clearfix">
                  <div class="pull-left title"><h2><?php the_sub_field('title'); ?></h2></div>
                  <div class="pull-right">
                    <img src="<?php echo $text_block['url']; ?>">
                  </div>
                </div>
              </div>
              <div class="from_right_bot bot_box_right"></div>
              <div class="description"><p><?php the_sub_field('caption'); ?></p></div>
            </div>
          <?php endwhile; ?>
        <?php endif; ?>
        </div><!-- .row  -->
      </div><!-- .container .page-contents -->
    </section><!-- .black-bg -->

    <?php
    break;
  
  default:?>
    <section class="text-block dark-texture-4">
      <div class="container page-contents">
        <?php if(count($texts) > 0): ?>
          <?php while(have_rows('text_blocks')): the_row(); ?>
          <div class='text-block-section row'>
            <div class="span12">
              <h1><?php the_sub_field('title'); ?></h1>
              <ul class="nav allcaps">
              <?php
                $buttons = array();
                $buttons['titles'] = array();
                $buttons['links'] = array();
                while(have_rows('buttons')): the_row();
                  switch (get_sub_field('link_type')) {
                    case 'external':
                      $link = get_sub_field('link');
                      break;
                    
                    case 'internal':
                      $link = get_sub_field('page');
                      break;
                    
                    default:
                      $link = get_sub_field('link');
                      break;
                  }
                  array_push($buttons['titles'], get_sub_field('title'));
                  array_push($buttons['links'], $link);
                endwhile;
              ?>
              <?php if(count($buttons) > 0): ?>
                <?php $button_count = 0; ?>
                <?php while(have_rows('buttons')): the_row(); ?>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page">
                    <a href="<?php echo $buttons['links'][$button_count]; ?>"><?php echo $buttons['titles'][$button_count]; ?></a>
                  </li>
                <?php $button_count++; ?>
                <?php endwhile; ?>
              <?php endif; ?>
              </ul>
              <p><?php the_sub_field('caption'); ?></p>
            </div>
          </div><!-- .text-block-section .row-->
          <?php endwhile; ?>
        <?php endif; ?>
      </div><!-- .container .page-contents -->
    </section><!-- .dark-texture-4 -->
    <?php break;
}
?>