<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>
              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Individual/Team Training</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <tr><td colspan=2 align=center><b>Individual Training</b></td></tr>
                    <TR>
                      <TD valign="top" colspan=2>
                        <P>The NLA Staff is please to announce <b>NLA Individual Training</b>. The individual 
                        training provides high quality, player-specific training focused on improving 
                        technique and understanding of the game in order to take the player's mental, 
                        physical, and tactical performance to the "Next Level".  Session are fun, 
                        intense, and challenging!  Every touch on the ball is calculated and 
                        deliberate.  Training is tailored specifically to each player's individual 
                        areas of difficulty. Each session will focus on improving the player's ball 
                        skill which will carry over in overall player confidence and performance.
                        </p>
                        <p>To arrange for individual training please contact NLA at 
                        <font color=#0000ff>staff@nextlevelacademy.com</font> or call at 467-2299.
                        </p>
                      </TD>
                    </TR>
                    <tr>
                      <td align=center><strong>Member/Non-Member Rates</strong></td>
                      <td align=center><strong>1 thru 4 Player Package Rates</strong></td>
                    </tr>
                    <tr>
                      <td colspan=2>
                        <em>* To qualify as a Next Level Academy Member you must be presently enrolled in our
                        NLA Graduate, Academy, Centers, or Functionals program.</em> 

                      </td>
                    </tr>
                    <tr><td height=5></td></tr>
                    <tr>
                      <td colspan=2>
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                          <tr>
                            <td align=left colspan=4><strong>One Player Individual Session </strong></td>
                          </tr>
                          <TR>
                            <td align=right>1 session</td>
                            <td align=center width=175>Non-member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$60</strong></td>
                          </tr>
                          <TR>
                            <td align=right>&nbsp;</td>
                            <td align=center width=175>Member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$55</strong></td>
                          </tr>
                          <tr><td height=5></td></tr>
                          <tr>
                            <td align=left colspan=4><strong>One Player Package </strong></td>
                          </tr>
                          <TR>
                            <td align=right>4 sessions</td>
                            <td align=center width=175>Non-member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$50/player per session</strong></td>
                          </tr>
                          <TR>
                            <td align=right>&nbsp;</td>
                            <td align=center width=175>Member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$45/player per session</strong></td>
                          </tr>
                          <tr><td height=5>&nbsp;</td></tr>
                          <tr><td height=5 align=center colspan=4><img src=images/line.jpg width=400></td></tr>
                          <tr><td height=5>&nbsp;</td></tr>
                          <tr>
                            <td align=left colspan=4><strong>Two Player Individual Session </strong></td>
                          </tr>
                          <TR>
                            <td align=right>1 session</td>
                            <td align=center width=175>Non-member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$55/player</strong></td>

                          </tr>
                          <TR>
                            <td align=right>&nbsp;</td>
                            <td align=center width=175>Member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$50/player</strong></td>

                          </tr>
                          <tr><td height=5></td></tr>
                          <tr>
                            <td align=left colspan=4><strong>Two Player Package </strong></td>
                          </tr>
                          <TR>
                            <td align=right>4 sessions</td>
                            <td align=center width=175>Non-member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$45/player per session</strong></td>

                          </tr>
                          <TR>
                            <td align=right>&nbsp;</td>
                            <td align=center width=175>Member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$40/player per session</strong></td>

                          </tr>
                          <tr><td height=5>&nbsp;</td></tr>
                          <tr><td height=5 align=center colspan=4><img src=images/line.jpg width=400></td></tr>
                          <tr><td height=5>&nbsp;</td></tr>
                          <tr>
                            <td align=left colspan=4><strong>Three Player Individual Session </strong></td>
                          </tr>
                          <TR>
                            <td align=right>1 session</td>
                            <td align=center width=175>Non-member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$50/player</strong></td>

                          </tr>
                          <TR>
                            <td align=right>&nbsp;</td>
                            <td align=center width=175>Member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$45/player</strong></td>

                          </tr>
                          <tr><td height=5></td></tr>
                          <tr>
                            <td align=left colspan=4><strong>Three Player Package </strong></td>
                          </tr>
                          <TR>
                            <td align=right>4 sessions</td>
                            <td align=center width=175>Non-member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$40/player per session</strong></td>

                          </tr>
                          <TR>
                            <td align=right>&nbsp;</td>
                            <td align=center width=175>Member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$35/player per session</strong></td>

                          </tr>
                          <tr><td height=5>&nbsp;</td></tr>
                          <tr><td height=5 align=center colspan=4><img src=images/line.jpg width=400></td></tr>
                          <tr><td height=5>&nbsp;</td></tr>
                          <tr>
                            <td align=left colspan=4><strong>Four Player Individual Session </strong></td>
                          </tr>
                          <TR>
                            <td align=right>1 session</td>
                            <td align=center width=175>Non-member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$45/player</strong></td>

                          </tr>
                          <TR>
                            <td align=right>&nbsp;</td>
                            <td align=center width=175>Member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$40/player</strong></td>

                          </tr>
                          <tr><td height=5></td></tr>
                          <tr>
                            <td align=left colspan=4><strong>Four Player Package </strong></td>
                          </tr>
                          <TR>
                            <td align=right>4 sessions</td>
                            <td align=center width=175>Non-member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$35/player per session</strong></td>

                          </tr>
                          <TR>
                            <td align=right>&nbsp;</td>
                            <td align=center width=175>Member</td>
                            <td align=center width=25>--</td>
                            <td align=center><strong>$30/player per session</strong></td>

                          </tr>
                          <tr><td height=5>&nbsp;</td></tr>

                        </tbody></table>
                      </td>
                    </tr>
		        
	              <tr><td colspan=2 align=center><hr><b>NLA Team Training</b></td></tr>
			<TR>
                      <TD valign="top" colspan=2>
                        <P><b>Next Level Team Training</b> is a great opportunity for your team and coach to be trained by one of our NLA Director Coaches. 
				Learn the Next Level philosophy while your NLA Director/Trainer takes your team through specifically devised training regimens to work on your team's focus areas. 
				There is a minimum registration of 4 sessions per team. Each session will include agility, technical, and tactical portions. This will coincide with the overall concept discussed 
				and agreed upon between Coach and the NLA Director/Trainer after the first session.

                        </p>
                        <p>Contact NLA at 
                        <font color=#0000ff>staff@nextlevelacademy.com</font> or call at 467-2299 for pricing, registration, and scheduling information.
                        </p>
                      </TD>
                    </TR>


                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>