///////////////
// Hide Logo //
///////////////

$(window).scroll(function() {
    if ($(this).scrollTop() > 10) {
        $('#logo').hide(1000);

        $('.navbar').css({
            'background': 'rgba(0, 0, 0, 0.6)'
        });

        $('#top-nav').css({
            'margin-top': '0px'
        });
        /*$('#top-nav').css({'margin-top':'20px','background-color':'#06F'});*/
    } else {
        $('#logo').show(1000);;
        $('.navbar').css({
            'background': 'rgba(0, 0, 0, 0.0)'
        });
        $('#top-nav').css({
            'margin-top': '0px'
        });

        /*$('#top-nav').css({'margin-top':'0px','background-color':'none'});*/
    }
});

/*$(function () {

    var lastScrollTop = 0;
    $(window).scroll(function(event){
       var st = $(this).scrollTop();
       if (st > lastScrollTop){
           $(".navbar").hide(200);
       } else {
           $(".navbar").show(200);
       }
       lastScrollTop = st;
    });


}); */
