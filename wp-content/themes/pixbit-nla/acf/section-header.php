<?php
/**
 * ACF Section - Header Section
 *
 */
?>
<section id="header-section-<?php echo $section_counter; ?>" class="header-section dark-texture-4">
  <div class="container">
    <div class="row">
      <div class="span12 center">
        <h3 class="subheading"><?php echo get_sub_field('title'); ?></h3>
        <h4><?php echo get_sub_field('caption'); ?></h4>
      </div>
    </div>
  </div>
</section>
