<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>
<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>

              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Raleigh/Wake Forest Development Centers</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>Raleigh/Wake Forest Development Centers</FONT></strong></P>

                        <P>Next Level Academy is offering the full year of Development Centers as individual 
			season sessions or full year participation.  Each session will provide the instruction 
			and repetition to improving your player�s technical ability. 
                  All sessions for Raleigh/WF Development Centers are held at the Factory facility.</p>
			<p>For each session you register your player for, you will have to provide payment 
			within 2 weeks of registering or loose your spot for that session.  If you register 
			for the entire year (5 sessions) you will be required to pay 50% upfront and then the 
			remaining balance by the beginning of the third session.</p>
			<p>Each season session will have focused areas of development.  The techniques will be 
			taught in various drills at training but it is up to the individual player to practice 
			away from training to get the most success.</p>
                     </TD>
                    </TR>
                    <TR>
                      <TD>
                        <font style="color: #ff0000;"><strong>For each session your player will need to:</strong></font> 
                              <br>&nbsp; &nbsp; &nbsp; 1. Wear NLA Development Center training t-shirt 
                              <br>&nbsp; &nbsp; &nbsp; 2. Have indoor or flat soled shoes 
                              <br>&nbsp; &nbsp; &nbsp; 3. Wear shin guards
                              <br>&nbsp; &nbsp; &nbsp; 4. Bring a size 4 soccer ball with their name on it 
                              <br>&nbsp; &nbsp; &nbsp; 5. Bring a water bottle with their name on it 
                      </TD>
                    </TR>
                    <TR>
                      <TD>
                        <font style="color: #ff0000;"><strong>Focused Areas:</strong></font> 
                              <br>&nbsp; &nbsp; &nbsp; Agility and Coordination 
                              <br>&nbsp; &nbsp; &nbsp; Dribbling and Ball Control 
                              <br>&nbsp; &nbsp; &nbsp; Passing and Receiving 
                      </TD>
                    </TR>
                    <tr><td height=10></td></tr>
                    <TR>
                      <TD colspan=2> <font style="color: #ff0000;"><strong>Registration and Payment:</strong></font>
                        &nbsp All <strong>registration</strong> is done online at our 
                        website - <a href=www.nextlevelacademy.com> www.nextlevelacademy.com</a> under
			      the Development Centers link.
                        Our waiver form has changed so all players registering for a Development Centers session will need to 
                        complete a <a href = "docs/NextLevelAcademyMedicalWaiver.doc">
                        new waiver form</a> for the program year. Once a new waiver 
                        form has been completed, it's good through Spring.
                      </td>
                    </tr>
                    <tr><td height=10></td></tr>
                    <TR>
                      <TD colspan=2>
                        <p>The <strong>cost</strong> is $150 for 8 trainings (1 session).</p>

			<p>Please drop off your completed medical waiver and payments to the Next Level office 
			located at Netsports 3717 Davis Drive Morrisville, NC  27560.  You can 
			also mail in your completed medical waiver and payment to Next Level 
			Academy 3717 Davis Drive Morrisville, NC  27560.  If you have any questions you can 
			contact us at 467-2299 or email us at staff@nextlevelacademy.com.</p> 
                      </td>
                    </tr>
                    <tr><td height=10></td></tr>
                    <TR>
                      <TD colspan=2><strong>With your $150 registration you will receive:</strong>
                      	<br>&nbsp; &nbsp; 8- One hour training sessions
			<br>&nbsp; &nbsp; 1- training t-shirt
                      </td>
                    </tr>
                    <tr><td height=10></td></tr>
                    <TR>
                      <TD colspan=2> <strong>If you register for more than 1 session the cost is:</strong>
                      	<br> &nbsp; &nbsp; Register for 2 sessions: $290
			<br> &nbsp; &nbsp; Register for 3 sessions: $430
			<br> &nbsp; &nbsp; Register for 4 sessions: $570
			<br> &nbsp; &nbsp; Register for 5 sessions: $700
			<p> If you register for 1, 2, 3, or 4 sessions full payment must be received within 2 weeks of 
			registration.
			<p> If you register for 5 sessions at one time 50% ($350) of total must be received within 2 weeks of 
			registration and remainder ($350) by the beginning of the third session. 
                      </td>
                    </tr>
                    <tr><td height=5 align=center><img src=images/line.jpg width=400></td></tr>
                    <TR>
                      <TD align= center><strong>Dates and Registration</strong> </td>
		    </tr>
		    <tr>
		      <td>
				   <p align=center>
                        &nbsp; &nbsp;<a href=RalWFSpring2014.php4>Spring 2014</a>
                        &nbsp; &nbsp;<a href=RalWFsummer.php4>Summer 2014</a>
                        &nbsp; &nbsp;<a href=RalWFfall.php4>Fall 2014</a>
                        &nbsp; &nbsp;<a href=RalWFwinter1.php4>Winter I 2014</a>
                        &nbsp; &nbsp;<a href=RalWFwinter2.php4>Winter II 2015</a> 
                        &nbsp; &nbsp;<a href=RalWFSpring2015.php4>Spring 2015</a> 
				   </p>
                      </TD>
                    </TR>
                    <tr><td height=10></td></tr>
		    <tr>
		      <td>
<?
    $prgFile = "data/Centers/RalWFRegactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        $fh = fopen("data/Centers/RalWFRegseason.txt", "r");
        $season = fgets($fh,25);
        fclose($fh);
        print("<p><strong>Registration for the younger Development Centers (U7, U8, and U9) is available</strong>. &nbsp; &nbsp;");
        print("Space is limited so please register online to reserve your place in the younger NLA Development Centers. ");
        print("To register <a href=regshtml.php4?&prgtype=RalWFCenters><font style=\"color: #0000ff;\">click here.</font></a> </p>");
      }
      else 
        print("<p align=center>Online registration for the younger Development Centers (U7, U8, and U9) is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the younger Development Centers (U7, U8, and U9) is not open.</p>");
                       
?>
		  </td>
		</tr>
		    <tr>
		      <td>

<?
    $prgFile = "data/Academy/RalWFRegactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        $fh = fopen("data/Academy/RalWFRegseason.txt", "r");
        $season = fgets($fh,25);
        fclose($fh);
        print("<p><strong>Registration for the older Development Centers (U10, U11, and U12) is available</strong>. &nbsp; &nbsp;");
        print("Space is limited so please register online to reserve your place in the older Development Centers . ");
        print("To register <a href=regshtml.php4?&prgtype=RalWFAcademy><font style=\"color: #0000ff;\">click here</font></a> </p>");

      }
      else 
        print("<p align=center>Online registration for the older Development Centers is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the older Development Centers  is not open.</p>");
                       
?>
		  </td>
		</tr>

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>