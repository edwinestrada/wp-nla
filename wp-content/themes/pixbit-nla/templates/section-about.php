<section id="about" class="dark-texture-4">
  <div class="container page-contents">
    <!-- Two Columns Starts -->
    <div class="row">
      <div class="span6">
        <h1>WHY CHOOSE US?</h1>
        <div class="accordion" id="accordion2">
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
              OUR EXPERIENCE
              </a>
            </div>
            <div id="collapseOne" class="accordion-body collapse in">
              <div class="accordion-inner">
                Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
              </div>
            </div>
          </div>
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
              OUR PROCESS
              </a>
            </div>
            <div id="collapseTwo" class="accordion-body collapse">
              <div class="accordion-inner">
                Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra.
              </div>
            </div>
          </div>
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
              OUR FACILITIES
              </a>
            </div>
            <div id="collapseThree" class="accordion-body collapse">
              <div class="accordion-inner">
                Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="span6">
        <h1>FUNCTIONAL FOCUS</h1>
        <div class="progress progress-striped active">
          <div class="bar" style="width: 95%;"><span>DRIBBLING</span></div>
        </div>
        <div class="progress progress-striped">
          <div class="bar" style="width: 85%;"><span>TRAPPING</span></div>
        </div>
        <div class="progress progress-striped">
          <div class="bar" style="width: 75%;"><span>VISION</span></div>
        </div>
        <div class="progress progress-striped">
          <div class="bar" style="width: 65%;"><span>PASSING</span></div>
        </div>
        <div class="progress progress-striped">
          <div class="bar" style="width: 55%;"><span>SHOOTING</span></div>
        </div>
      </div>
    </div>
    <!-- Two Columns Ends -->
  </div><!-- .container .page-contents -->
  <!-- /container -->
</section>
