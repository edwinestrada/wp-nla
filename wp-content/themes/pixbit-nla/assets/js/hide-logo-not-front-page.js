///////////////
// Hide Logo //
///////////////

$(window).scroll(function() {
    if ($(this).scrollTop() > 10) {
      $('.navbar').css({
          'background-color': 'rgba(0, 0, 0, 0.7)'
      });
      // $('.navbar .current-menu-item').css({
      //     'background-color': 'rgba(255, 107, 18, 1)'
      // });
    } else {
      $('.navbar').css({
          'background-color': 'rgba(0, 0, 0, 1)'
      });
      // $('.navbar .current-menu-item').css({
      //     'background-color': 'rgba(255, 107, 18, 0.7)'
      // });
    }
});