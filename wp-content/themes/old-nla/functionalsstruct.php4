<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>
              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Functionals Training</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD align=center colspan=2>
                        <strong>Finishing / Goalkeeping</strong>
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Ball Striking - learn the proper fundamentals and technique of becoming 
                        proficient and confident using all parts of both feet.
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Expand your Imagination and creativity around the goal
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Acquire the tools needed to be dangerous in the final 3rd
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Recognize when to be selfish/unselfish around the goal 
                      </TD>
                    </TR>
                    <tr><td height=10>&nbsp</td></tr>
                    <TR>
                      <TD align=center colspan=2>
                        <strong>Passing / Receiving</strong>
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Learn the proper technique of playing different types of passes � lofted, driven, bent 
                        and different spins etc.
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Develop the knowledge to recognize when to use what type of pass
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Acquire the art of a good 1st touch � develop the ability and understanding to be 
                        able to control the ball where needed using all parts of the body (feet, thighs, 
                        chest, head)
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Ball Striking � develop the proper technique playing the ball with all parts of the foot 
                        (inside/outside, sole, heel) as well as chest and head.
                      </TD>
                    </TR>
                    <tr><td height=10>&nbsp</td></tr>
                    <TR>
                      <TD align=center colspan=2>
                        <strong>Dribbling / Coerver</strong>
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Learn and polish overall ball control using all parts of both feet 
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Become skilled at deception, body language and how to sell your fake
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Improve your overall ball skills, shielding, juggling, change of direction etc. 
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Build your confidence and comfort on the ball 
                      </TD>
                    </TR>
                    <tr><td height=10>&nbsp</td></tr>
                    <TR>
                      <TD align=center colspan=2>
                        <strong>Transition Play / Defending</strong>
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Gain knowledge in the proper fundamentals of the counter attack
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Recognize the correct time to execute
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Be fundamentally trained on one on one and tactical defending 
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Learn tactical defending  � Man marking, zone defending and match-up zone
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Learn the flat back and sweeper formations 
                      </TD>
                    </TR>
                    <tr><td height=10>&nbsp</td></tr>
                    <TR>
                      <TD align=center colspan=2>
                        <strong>Combination Play</strong>
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Learn the art in bringing the ball out of the back   
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Proper positioning and situations to make good defensive attacking runs
                      </TD>
                    </TR>
                    <TR>
                      <TD VAlign=top><IMG height=12 src="images/ball.jpg" width=10></TD>
                      <TD>
                        Be taught combination standards � 1-2�s, lay-offs, dummies, patterns, 1-3�s 
                      </TD>
                    </TR>
                    <tr><td height=10>&nbsp</td></tr>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>