<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>


  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Academy Results</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top" colspan=4>
       I want to thank each of you for bringing your son or daughter to the Next Level Academy Tryouts. 
       It is a very stressful process and unfortunately we can not place all players on a team. We have 
       exciting Academies or Centers programs, based on age, planned for the upcoming year with 3 sessions (Summer, Fall and Spring) 
       with 8 trainings per session. The Academies and Centers programs are an important part of why Next Level Academy 
       has been successful. It gives players an opportunity to go through the same training that the 
       players on the teams go through. We have experienced many tryouts, whether it be trying out ourselves 
       or conducting the tryout, and the one thing that is clear is that all players handle tryouts differently. 
       Most of the time players go through tryouts, they either make it and become part of a team, or do not 
       make it and have to wait until the next year to tryout again. This is unfortunate because I have seen 
       too many times where a player had a bad tryout but was as good as many of the players that made the team. 
       We understand that there will always be error in any tryout, so that is why we created the Academies and Centers
       programs. We want to give the players that want to make a commitment to getting better the opportunity 
       to do so. My staff and I know that each player develops differently and we want to help with that process.
       We applaud a young player determined to prove to the coaches and other players that they will put the 
       hard work in to become a better soccer player.</p>
       <p align="center">Thank you - &nbsp; 
         <font style ="font-family: "Script MT Bold,courier" color="#ff6000">Damon</font>
       </p>
                      </td>
                    </TR>
                    <tr><td height=5>&nbsp;</td></tr>
<?
  $agegroup= $_GET['agegroup'];  
  $tryoutFile = "data/Tryouts/".$agegroup."results.txt";
  
  if ( file_exists($tryoutFile) ) 
  { 
    $fh = fopen ($tryoutFile,"r");
    $active = fread($fh, 80);
    fclose($fh);
    if ($active == "active" )
    {
      $outputColumn = 0;
      $playerNumbers = array();
      if ( $agegroup == "u9" ) 
      {
        $groupindex = 7;   
        $maxgroup = 10;  
      }
      else {
        $groupindex = 10;   
        $maxgroup = 12;
      }


      $gender = "Male";
      while (  $groupindex < $maxgroup ) {
        if ( $gender == "Male" ) {
          $ageGroupFile = "data/Tryouts/U".$groupindex."Maleacademy.txt";
          $headerString = "U".$groupindex." Males";
        }
        else {
          $ageGroupFile =  "data/Tryouts/U".$groupindex."Femaleacademy.txt";
          $headerString = "U".$groupindex." Females";
        }

        if ( file_exists($ageGroupFile) ) {
          $fhTeam = fopen($ageGroupFile, "r");
          $index = 0;
          $totalPlayers = 0;
          while (  !feof($fhTeam) ) {
            $userInfo = fgets($fhTeam, 800);
            $parms = explode (",", $userInfo);
            $parmCount = count($parms);
            while ( $index < $parmCount ) {
              $playerNumbers[$index] = chop($parms[$index]); 
              $index++; 
            }
          }  // while userInfo = fgetcsv
          fclose($fhTeam);
          $totalPlayers = $index;

          if ( $outputColumn == 0 ) {   // need to start a new row
            print("<tr><td height=10>&nbsp;</td></tr>");
            print("<tr><td width=100>&nbsp;</td>");
            $needEndTR = 1;
          }

          print("<td width=325 valign=top><table width=325 height =200  border=1 cellpading=0 cellspacing=0 bgcolor=#ffff90><tbody><tr>");
          print("<td colspan=10 align=center valign=top height =30>");
          print("<font style = \" font-size: 14px; color: #0000ff;\"><strong>".$headerString."</strong></font></td></tr><tr>");
 
          $index = 0;
          $tdindex = 0;
          while ( $totalPlayers >  0 ) {
            print("<td width=40 align=center height =20>");
            print("<font style = \"font-size: 12px; color: #000000\">".$playerNumbers[$index]."</font></td>");
            $index++;
            $tdindex++;
            if ( $tdindex >= 10 ) {
              print("</tr><tr>");
              $tdindex = 0;
            }
            $totalPlayers--;
          }  // while totalPlayers > 0 

          if ($tdindex != 0) {
            while ( $tdindex < 10 ) {
              print("<td>&nbsp</td>");
              $tdindex++;
            }
          }

          print("</tr><tr><td colspan=10 align=center><font style = \"font-size: 12px;\">");
          print("<a href=\"acceptdecline.php4?&source=Academies\">Click here to accept or decline position.</a>");  
          print("</tr><tr><td colspan=\"10\">&nbsp;</td></tr></tbody></table></td>");
          print("<td width=10>&nbsp;</td>");
        
          $outputColumn++;
          if ( $outputColumn >= 2 ) {
            print("</tr>");
            $outputColumn = 0;
            $needEndTR = 0;
          } // if outputColumn >= 2

        }   // file_exists(ageGroupFile)

        if ( $gender == "Male" ) {
          $gender = "Female";
        }
        else {
          $groupindex++;
          $gender = "Male";
        }
      }  // end while fgetcsv

      if ( $needEndTR == 1 )
        print("</tr>");
    }
    else {
      print("<tr><td align=center><strong>Tryout results have not been posted yet.</strong></td></tr>");
    }
  }
  else {
    print("<tr><td align=center><strong>Tryout results have not been posted yet.</strong></td></tr>");
  }


?>
                 

                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>