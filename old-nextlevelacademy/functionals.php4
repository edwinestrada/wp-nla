<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>

<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>
              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Functionals</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><u>NLA Functional Training</u></strong></P>
                        <p>The NLA Functionals program offers players the opportunity to fine-tune their 
			techniques, tactics and overall knowledge of the game in a team environment.  Each 
			Functionals session focuses on a specialized training area which Next Level Academy 
			believes is necessary to master in order for a player to maximize his or her potential.
			</p>
                        <p>All sessions will be held indoors at the Netsports facility in Morrisville. </p>
			<p>In order to provide a more family and team environment we will ask you to look ahead 
			at your calendar for the next year.  We�ve come up with a year round schedule
                        that will provide a consistent opportunity for our students to 
			maximize their overall skill level.</p>
			<p>Each Functional focus area will consist of 8 sessions lasting 1 hour and 15 minutes 
			each.  All players will enroll via the website (www.nextlevelacademy.com) online 
			registration.  At this time NLA Functionals is offered only for U11 to U15 boys and girls.

                        <p>Some of the topics that will be touched on each session include: finishing, goalkeeping,
                          passing, receiving, dribbling, Coerver (fast footwork), and combination/transitional play/defending. </p>

			<p>You may choose to participate in all 3 session or you may choose 1 session at a time.  
			Each session you register your player for, you will have to provide payment within 2 
			weeks of registering or loose your spot for that session.  If you register for the entire 
			year (all 3 sessions) you will be required to pay 50% upfront and then the remaining 
			balance by the beginning of the third session.</p>

			<p>After looking at the needs of the players, we have devised a new system that will 
			be both manageable and extremely beneficial, which will compliment your player�s club 
			and/or school team experience.</p>

			<p>For more information on the NLA Functional structure, 
                        <a href=functionalsstruct.php4>click here</a>.</p>
                        <p>For information on dates, cost, and on-line registration 
                        <a href=functionalsdates.php4>click here</a>.</p>

                        <p> Our waiver form has changed so all players registering for a Functionals session will need to 
                        complete a <a href = "docs/NextLevelAcademyMedicalWaiver.doc">
                        new waiver form</a> for the program year. Once a new waiver 
                        form has been completed, it's good through Spring.</p>

                      </TD>
                    </TR>



                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>


<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>