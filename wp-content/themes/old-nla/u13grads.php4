<?    
  $bd = fopen("header.txt","r");
  fpassthru($bd);
?>
  <TR>
    <TD>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
        <TR>
          <TD VAlign=top width=145>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=""><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>Links</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

             <TR><TD height=5>&nbsp</TD></TR>
<?    
  $bd = fopen("links.txt","r");
  fpassthru($bd);
?>

              <TR><TD>&nbsp;</TD></TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterleft.gif" width=19>
                      </TD>
                      <TD align=middle background=images/topbluebg.gif>
                        <STRONG><FONT color=#ffffff>News</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/topbluebg.gif>
                        <IMG src="images/bluefooterright.gif" width=19>
                      </TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD bgcolor=#e7e7e7>
                  <br><SCRIPT language=JavaScript1.2 src="script/NLAscroller.js"></SCRIPT>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif height=21>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
          <TD>&nbsp;</TD>
          <TD width=593 valign="top"> 
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD VAlign=top width=21 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayleft.gif" width=21></TD>
                      <TD align=middle background=images/darkgraybg.gif>
                        <STRONG><FONT color=#ffffff>NLA U13 Graduate Functionals Program</FONT></STRONG>
                      </TD>
                      <TD VAlign=top width=19 background=images/darkgraybg.gif><IMG height=21 src="images/darkgrayright.gif" width=19></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR><TD height=2></TD></TR>

              <TR>
                <TD valign="top" background="images/graymidbottom.gif"> 
                  <TABLE cellSpacing=5 cellPadding=5 width="100%" border=0><TBODY>
                    <TR>
                      <TD valign="top">
                        <P align=center><strong><FONT COLOR=#ff0000>NLA U13 Graduate Functionals Program</FONT></strong></P>

                        <P>  Next Level Academy's U13 Graduate Program continues to expand on what players learned in our U12 Graduate 
					Program.  In this program, players continue developing & finessing their technique, skills, and game tactics 
					while learning new skills & game situations to help them achieve the highest level possible.
 				  </p><p>
					In this program, players train one night per week for one hour for 10 months (July - the end of the following 
					April).  A typical training session will have 30 minutes of technical training followed by 30 minutes of game 
					play to incorporate what they just learned.  
 				  </p><p>
					This program is only open to players that have graduated from our U11 Academy Program.  It is not open to 
					the general public.  
 				  </p><p>			
				Registration information is sent directly from Next Level Academy to eligible graduating Academy players.
				</p>
                     </TD>
                    </TR>
<!--
				<TR>
                      <TD>
                        <TABLE cellSpacing=3 cellPadding=3 width="100%" border=0><TBODY>
                          
                          <TR>
                            <TD width="25%"><u>Summer</u></TD>
                            <TD width="40%"><u>Winter 1</u></TD>
                            <TD><u>Winter 2</u></TD>
                          </TR>
                          <TR>
                            <TD>July 12</TD>
                            <TD>November 1</TD>
                            <TD>January 24</TD>
                          </TR>
                          <TR>
                            <TD>July 19</TD>
                            <TD>November 8</TD>
                            <TD>January 31</TD>
                          </TR>
                          <TR>
                            <TD>July 26</TD>
                            <TD>November 15</TD>
                            <TD>February 7</TD>
                          </TR>
                          <TR>
                            <TD>August 2</TD>
                            <TD>November 22</TD>
                            <TD>February 14</TD>
                          </TR>
                          <TR>
                            <TD>August 9</TD>
                            <TD>November 29 - Closed, Holiday</TD>
                            <TD>February 21</TD>
                          </TR>
                          <TR>
                            <TD>August 16</TD>
                            <TD>December 6</TD>
                            <TD>February 28</TD>
                          </TR>
                          <TR>
                            <TD>August 23</TD>
                            <TD>December 13</TD>
				       <TD>March 7</TD>
                          </TR>
                          <TR>
                            <TD>August 30</TD>
                            <TD>December 20</TD>
				       <TD>March 14</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>December 27 - Closed, Holiday</TD>
				      <TD>March 21 (MAKE UP if needed)</TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>Jan 3, 2014 - Closed, Holiday</TD>
				      <TD></TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>Jan 10, 2014</TD>
				      <TD></TD>
                          </TR>
                          <TR>
                            <TD></TD>
                            <TD>Jan 17 (MAKE UP if needed)</TD>
				      <TD></TD>
                          </TR>
                          <tr><td height=4>&nbsp; </td></tr>
                        </TBODY></TABLE>
                      </TD>
                    </TR>
                    <tr><td height=10></td></tr>

                    <TR>
                      <TD> All players registering for a Graduate Program session will need to 
                        complete a <a href = "docs/NextLevelAcademyMedicalWaiver.doc">
                        waiver form</a> for the program year. Once a new waiver 
                        form has been completed, it's good through Spring. 
                      </td>
                    </tr>
                    <tr><td height=10></td></tr>
 -->
		    <tr>
		      <td>
<?
/*
    $prgFile = "data/Grads/Regactive.txt";
    if ( file_exists($prgFile) )
    {
      $fh = fopen($prgFile, "r");
      $active = fgets($fh,25);
      fclose($fh);
      if ( $active == "open" ) {
        print("<p><strong>Registration for the U13-U15 Graduate Program is available</strong>. &nbsp; &nbsp;");
        print("Space is limited so please register online to reserve your place in the U13-U15 Graduate Program. ");
        print("To register <a href=gradsreg.php4><font style=\"color: #0000ff;\">click here.</font></a> </p>");
      }
      else 
        print("<p align=center>Online registration for the U13-U15 Graduate Program is not open.</p>");
    }
    else 
      print("<p align=center>Online registration for the U13-U15 Graduate Program is not open.</p>");
*/                      
?>
		  </td>
		</tr>


                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0><TBODY>
                    <TR>
                      <TD vAlign=bottom width=21><IMG height=21 src="images/grayleftbottom.gif" width=21></TD>
                      <TD background=images/graymidbottom.gif>&nbsp;</TD>
                      <TD vAlign=bottom width=19><IMG height=21 src="images/grayrightbottom.gif" width=20></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>

              <TR><TD height=15></TD></TR>

              
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>

<?    
  $bd = fopen("footer.txt","r");
  fpassthru($bd);
?>